<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>HipHelper</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="http://localhost/hiphelper-new/hiphelper/public/css/materialize.min.css">
    <link rel="stylesheet" href="http://localhost/hiphelper-new/hiphelper/public/css/master-css.css" media="screen,projection">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="http://localhost/hiphelper-new/hiphelper/public/favicon.ico">
    <!-- implement csrf token for AJAX calling -->
    <!-- <meta name="_token" content="WAlcBwICNQ0SVa632GMJKPbQhKhqVU4xGb5kgtgT"/> -->
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="http://localhost/hiphelper-new/hiphelper/public/js/jquery.min.js"></script>
    <script type="text/javascript" src="http://localhost/hiphelper-new/hiphelper/public/js/materialize.min.js"></script>
    <script type="text/javascript" src="http://localhost/hiphelper-new/hiphelper/public/js/master-js.js"></script>
</head>

<body>
		<div class="navbar-fixed">
	 <nav>
        <div class="nav-wrapper">
            <div class="container"> <a href="#" id="logo" class="brand-logo">HipHelper</a>
                <ul id="menu-navbar" class="right hide-on-med-and-down">
                    <li>
                        <!-- Login call modal --><a class="modal-trigger" href="#login">Masuk</a> </li>
                    <li> <a id="btn-daftar" class="waves-effect waves-light btn" href="http://localhost/hiphelper-new/hiphelper/public/register">Daftar</a> </li>
                </ul>
            </div>
        </div>
    </nav>
		</div>
            <!-- Modal login -->
            <div id="login" class="modal">
        <div class="modal-content" style="color: rgb(128, 128, 128);">
            <form>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="" id="email-login" type="text" class="validate">
                        <label class="" for="email-login">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="" id="password-login" type="password" class="validate">
                        <label class="" for="password-login">Password</label>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer"> <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a> </div>
    </div>
		<div class="slider" style="height: 565px;touch-action: pan-y;-webkit-user-drag: none;-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
			<ul class="slides" style="height: 525px;">
				<li class="active" style="opacity: 1; transform: translateX(0px) translateY(0px);">
					<img src="data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" style="background-image: url(&quot;http://localhost/hiphelper-new/hiphelper/public/images/slider1.jpg&quot;);">
					
					<div class="caption center-align" style="opacity: 1; transform: translateY(0px) translateX(0px);">
						<div class="big-rectangle">
							<h3>Halo, Selamat Datang di HipHelper
							</h3>
							<h5 class="light grey-text text-lighten-3">
								syalalalalallala~
							</h5>
						</div>
					</div>
				</li>
				<li class="" style="opacity: 0; transform: translateX(0px) translateY(0px);">
					<img src="data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" style="background-image: url(&quot;http://localhost/hiphelper-new/hiphelper/public/images/slider2.jpg&quot;);">
					<div class="caption center-align" style="opacity: 0; transform: translateY(-100px) translateX(0px);">
						<div class="row">
		
							<div class="col s4">
								<div class="slider-card">
									<h5 class="light grey-text text-lighten-3">
										Halo, setelah saya menggunakan aplikasi ini. Saya merasa pekerjaan saya terasa lebih tenang. Anak saya bisa dirawat dengan baik.
									</h5>
									<img src="data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" id="user-icon" class="circle responsive-img" style="background-image: url(&quot;http://localhost/hiphelper-new/hiphelper/public/images/user.png&quot;);">
									<h5 style="font-weight: bold;">
										Jayden Smith
									</h5>
									<h5 style="font-style: italic;">
										22 tahun - PNS
									</h5>
								</div>
							</div>
							<div class="col s4">
								<div class="slider-card">
									<h5 class="light grey-text text-lighten-3">
										Halo, setelah saya menggunakan aplikasi ini. Saya merasa pekerjaan saya terasa lebih tenang. Anak saya bisa dirawat dengan baik.
									</h5>
									<img src="data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" id="user-icon" class="circle responsive-img" style="background-image: url(&quot;http://localhost/hiphelper-new/hiphelper/public/images/user.png&quot;);">
									<h5 style="font-weight: bold;">
										Jayden Smith
									</h5>
									<h5 style="font-style: italic;">
										22 tahun - PNS
									</h5>
								</div>
							</div>
							<div class="col s4">
								<div class="slider-card">
									<h5 class="light grey-text text-lighten-3">
										Halo, setelah saya menggunakan aplikasi ini. Saya merasa pekerjaan saya terasa lebih tenang. Anak saya bisa dirawat dengan baik.
									</h5>
									<img src="data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" id="user-icon" class="circle responsive-img" style="background-image: url(&quot;http://localhost/hiphelper-new/hiphelper/public/images/user.png&quot;);">
									<h5 style="font-weight: bold;">
										Jayden Smith
									</h5>
									<h5 style="font-style: italic;">
										22 tahun - PNS
									</h5>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
		<ul class="indicators"><li class="indicator-item active"></li><li class="indicator-item"></li></ul></div>

		

		<div class="container">
			
		</div>
	
		<div id="card-babysitter">
			<div class="container">
				<div id="search-bar" class="z-depth-2" style="
    background-color: white;
">
				<ul class="collapsible" data-collapsible="accordion">
					<li>
						<div class="collapsible-header">
							<form>
								<div class="input-field">
									<i class="material-icons prefix">search</i>
									<input placeholder="Cari Babysitter atau Agen Penyalur" id="icon_prefix" type="text">
								</div>
							</form>						
						</div>
					</li>
					<li class="">
						<div style="padding-top: 7px;background-color: #a6263e;" class="collapsible-header maroon">
							<h6 class="center-align">Pencarian Lebih Lanjut</h6>
						</div>
						<div class="collapsible-body" style="display: none;">
							<form>
								<div class="row">
									<div class="input-field col s4">
										 <!-- <i id="search-form-icon"class="material-icons prefix">location_on</i> -->
										<!--  <input placeholder="Placeholder" id="first_name" type="text" class="validate">
          								<label for="first_name">First Name</label> -->

										<input placeholder="Lokasi tempat tinggal Anda" type="text" id="autocomplete-input" class="autocomplete">
										<label for="autocomplete-input" class="active">Lokasi</label>
									</div>

									<div class="col s12 m4">
										<label>Pendidikan Terakhir</label>
										<select class="browser-default">
											<option value="" disabled="" selected="">Pendidikan Terakhir</option>
											<option value="SMP/MTS">SMP/MTS</option>
											<option value="SMA/SMK/MA">SMA/SMK/MA</option>
											<option value="D3">D3</option>
											<option value="S1">S1</option>
											<option value="S2">S2</option>
											<option value="S3">S3</option>	
										</select>
									</div>
									<div class="col s12 m4">
										<label>Status</label>
										<select class="browser-default">
											<option value="" disabled="" selected="">Status</option>
											<option value="semua">Semua</option>
											<option value="lajang">Lajang</option>
											<option value="menikah">Menikah</option>
											<option value="janda">Janda</option>	
										</select>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s6">
										<input placeholder="Minimal Gaji" id="min-gaji" type="text" class="validate">
										<label for="min-gaji" class="active">Minimal Gaji per bulan</label>
									</div>
									<div class="input-field col s6">
										<input placeholder="Maksimal Gaji" id="max-gaji" type="number" class="validate">
										<label for="max-gaji" class="active">Maksimal Gaji per bulan</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s3">
										<input placeholder="Minimal Umur" id="min-umur" type="number" class="validate">
										<label for="min-umur" class="active">Minimal Umur</label>
									</div>
									<div class="input-field col s3">
										<input placeholder="Maksimal Umur" id="max-umur" type="number" class="validate">
										<label for="max-umur" class="active">Maksimal Umur</label>
									</div>
									<div class="col s3">
										<label>Agama</label>
										<select class="browser-default">
											<option value="" disabled="" selected="">Agama</option>
											<option value="semua">Semua</option>
											<option value="Islam">Islam</option>
											<option value="Kristen">Kristen</option>
											<option value="Katolik">Katolik</option>
											<option value="Hindu">Hindu</option>
											<option value="Buddha">Buddha</option>
											<option value="lainnya">lainnya</option>	
										</select>
									</div>
									<div class="col s3">
										<label>Suku</label>
										<select class="browser-default">
											<option value="" disabled="" selected="">Suku</option>
											<option value="semua">Sunda</option>
											<option value="Islam">Jawa</option>
											<option value="Kristen">Minang</option>
											<option value="Katolik">Batak</option>
											<option value="Hindu">Bugis</option>
											<option value="Buddha">Lampung</option>
											<option value="lainnya">Palembang</option>
											<option value="lainnya">lainnya</option>	
										</select>
									</div>
								</div>
								<div class="center-align">
								<a class="waves-effect waves-light btn dark-maroon">Cari</a>
								</div>
							</form>
						</div>
					</li>

				</ul>
				  
   
			</div><div class="row">
					<div class="col s12">
						<ul class="tabs" style="width: 100%;">
							<li class="tab col s3"><a class="active" href="#pendatang-baru">Pendatang Baru</a></li>
							<li class="tab col s3"><a href="#tersedia" class="">Tersedia</a></li>
							<li class="tab col s3"><a href="#segera-tersedia" class="">Segera Tersedia</a></li>
						<div class="indicator" style="right: 612px; left: 0px;"></div></ul>
					</div>
				</div>
				
				<div id="pendatang-baru" style="display: block;">
		      		<div class="row">
		      						        			<div class="col s4">
		          				<div class="card maroon">
		            				<div style="padding-bottom: 0px;" class="card-content">
		            					<div class="row">
		            						<div style="height: 25px;" class="col s12">
					              				<span class="card-title">
					              					Niea Kurnia Fajar
					              					<span class="location">
					              							- Depok
					              					</span>
					              				</span>
				              
				              				</div>
		              					</div>
		              					<div class="row">
		              						<div class="col s6">
		              							<img class="avatar" src="http://localhost/hiphelper-new/hiphelper/public/images/avatar.jpg">
		              							<div class="center-align">
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
		              							 </div>
		              							
		              							 <div class="col s12">
		              							 	<span><i class="tiny material-icons prefix">query_builder</i></span>
          											<span>6 tahun</span>
       
        										</div>
        										<div class="col s12">
        											<span><i class="tiny material-icons prefix">perm_identity</i></span>
          											<span>29 tahun</span>
        										</div>

		              						</div>
		              						<div class="col s6">
		              						<p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
		              						</div>
		              						<div style="height: 10px;" class="col s12 center-align">
		              							<a href="#">6 Review</a>
		              						</div>
		              						
		              					</div>
		            				</div>
		            				<div class="card-action">
		            					<a style="width: 100%;" class="waves-effect waves-light btn dark-maroon" href="http://localhost/hiphelper-new/hiphelper/public/profile">Detil</a>
		            				</div>
		          				</div>
		        			</div>
		        					        			<div class="col s4">
		          				<div class="card maroon">
		            				<div style="padding-bottom: 0px;" class="card-content">
		            					<div class="row">
		            						<div style="height: 25px;" class="col s12">
					              				<span class="card-title">
					              					Niea Kurnia Fajar
					              					<span class="location">
					              							- Depok
					              					</span>
					              				</span>
				              
				              				</div>
		              					</div>
		              					<div class="row">
		              						<div class="col s6">
		              							<img class="avatar" src="http://localhost/hiphelper-new/hiphelper/public/images/avatar.jpg">
		              							<div class="center-align">
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
		              							 </div>
		              							
		              							 <div class="col s12">
		              							 	<span><i class="tiny material-icons prefix">query_builder</i></span>
          											<span>6 tahun</span>
       
        										</div>
        										<div class="col s12">
        											<span><i class="tiny material-icons prefix">perm_identity</i></span>
          											<span>29 tahun</span>
        										</div>

		              						</div>
		              						<div class="col s6">
		              						<p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
		              						</div>
		              						<div style="height: 10px;" class="col s12 center-align">
		              							<a href="#">6 Review</a>
		              						</div>
		              						
		              					</div>
		            				</div>
		            				<div class="card-action">
		            					<a style="width: 100%;" class="waves-effect waves-light btn dark-maroon" href="http://localhost/hiphelper-new/hiphelper/public/profile">Detil</a>
		            				</div>
		          				</div>
		        			</div>
		        					        			<div class="col s4">
		          				<div class="card maroon">
		            				<div style="padding-bottom: 0px;" class="card-content">
		            					<div class="row">
		            						<div style="height: 25px;" class="col s12">
					              				<span class="card-title" style="
    font-weight: 500;
">
					              					Niea Kurnia Fajar
					              					<span class="location">
					              							- Depok
					              					</span>
					              				</span>
				              
				              				</div>
		              					</div>
		              					<div class="row">
		              						<div class="col s6">
		              							<img class="avatar" src="http://localhost/hiphelper-new/hiphelper/public/images/avatar.jpg">
		              							<div class="center-align">
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
		              							 </div>
		              							
		              							 <div class="col s12">
		              							 	<span><i class="tiny material-icons prefix">query_builder</i></span>
          											<span>6 tahun</span>
       
        										</div>
        										<div class="col s12">
        											<span><i class="tiny material-icons prefix">perm_identity</i></span>
          											<span>29 tahun</span>
        										</div>

		              						</div>
		              						<div class="col s6">
		              						<p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
		              						</div>
		              						<div style="height: 10px;" class="col s12 center-align">
		              							<a href="#">6 Review</a>
		              						</div>
		              						
		              					</div>
		            				</div>
		            				<div class="card-action">
		            					<a style="width: 100%;" class="waves-effect waves-light btn dark-maroon" href="http://localhost/hiphelper-new/hiphelper/public/profile">Detil</a>
		            				</div>
		          				</div>
		        			</div>
		        					      		</div>
		      		<div class="row">
		      						        			<div class="col s4">
		          				<div class="card maroon">
		            				<div style="padding-bottom: 0px;" class="card-content">
		            					<div class="row">
		            						<div style="height: 25px;" class="col s12">
					              				<span class="card-title">
					              					Niea Kurnia Fajar
					              					<span class="location">
					              							- Depok
					              					</span>
					              				</span>
				              
				              				</div>
		              					</div>
		              					<div class="row">
		              						<div class="col s6">
		              							<img class="avatar" src="http://localhost/hiphelper-new/hiphelper/public/images/avatar.jpg">
		              							<div class="center-align">
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
		              							 </div>
		              							
		              							 <div class="col s12">
		              							 	<span><i class="tiny material-icons prefix">query_builder</i></span>
          											<span>6 tahun</span>
       
        										</div>
        										<div class="col s12">
        											<span><i class="tiny material-icons prefix">perm_identity</i></span>
          											<span>29 tahun</span>
        										</div>

		              						</div>
		              						<div class="col s6">
		              						<p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
		              						</div>
		              						<div style="height: 10px;" class="col s12 center-align">
		              							<a href="#">6 Review</a>
		              						</div>
		              						
		              					</div>
		            				</div>
		            				<div class="card-action">
		            					<a style="width: 100%;" class="waves-effect waves-light btn dark-maroon">Detil</a>
		            				</div>
		          				</div>
		        			</div>
		        					        			<div class="col s4">
		          				<div class="card maroon">
		            				<div style="padding-bottom: 0px;" class="card-content">
		            					<div class="row">
		            						<div style="height: 25px;" class="col s12">
					              				<span class="card-title">
					              					Niea Kurnia Fajar
					              					<span class="location">
					              							- Depok
					              					</span>
					              				</span>
				              
				              				</div>
		              					</div>
		              					<div class="row">
		              						<div class="col s6">
		              							<img class="avatar" src="http://localhost/hiphelper-new/hiphelper/public/images/avatar.jpg">
		              							<div class="center-align">
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
		              							 </div>
		              							
		              							 <div class="col s12">
		              							 	<span><i class="tiny material-icons prefix">query_builder</i></span>
          											<span>6 tahun</span>
       
        										</div>
        										<div class="col s12">
        											<span><i class="tiny material-icons prefix">perm_identity</i></span>
          											<span>29 tahun</span>
        										</div>

		              						</div>
		              						<div class="col s6">
		              						<p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
		              						</div>
		              						<div style="height: 10px;" class="col s12 center-align">
		              							<a href="#">6 Review</a>
		              						</div>
		              						
		              					</div>
		            				</div>
		            				<div class="card-action">
		            					<a style="width: 100%;" class="waves-effect waves-light btn dark-maroon">Detil</a>
		            				</div>
		          				</div>
		        			</div>
		        					        			<div class="col s4">
		          				<div class="card maroon">
		            				<div style="padding-bottom: 0px;" class="card-content">
		            					<div class="row">
		            						<div style="height: 25px;" class="col s12">
					              				<span class="card-title">
					              					Niea Kurnia Fajar
					              					<span class="location">
					              							- Depok
					              					</span>
					              				</span>
				              
				              				</div>
		              					</div>
		              					<div class="row">
		              						<div class="col s6">
		              							<img class="avatar" src="http://localhost/hiphelper-new/hiphelper/public/images/avatar.jpg">
		              							<div class="center-align">
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
			              							 <i class="tiny material-icons bintang">star</i>
		              							 </div>
		              							
		              							 <div class="col s12">
		              							 	<span><i class="tiny material-icons prefix">query_builder</i></span>
          											<span>6 tahun</span>
       
        										</div>
        										<div class="col s12">
        											<span><i class="tiny material-icons prefix">perm_identity</i></span>
          											<span>29 tahun</span>
        										</div>

		              						</div>
		              						<div class="col s6">
		              						<p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
		              						</div>
		              						<div style="height: 10px;" class="col s12 center-align">
		              							<a href="#">6 Review</a>
		              						</div>
		              						
		              					</div>
		            				</div>
		            				<div class="card-action">
		            					<a style="width: 100%;" class="waves-effect waves-light btn dark-maroon">Detil</a>
		            				</div>
		          				</div>
		        			</div>
		        					      		</div>

		      	

		      		<div class="right-align">
			      		<ul class="pagination" style="
    margin-bottom: 0px;
    padding-bottom: 20px;
">
		      				<li class="disabled"><i class="material-icons">chevron_left</i></li>
		      				<li class="active">1</li>
		      				<li class="waves-effect">2</li>
		      				<li class="waves-effect">3</li>
		      				<li class="waves-effect">4</li>
		      				<li class="waves-effect">5</li>
		      				<li class="waves-effect"><i class="material-icons">chevron_right</i></li>
		      			</ul>
	      			</div>
	      		</div>
      		</div>
      
		</div>
	
		<footer class="page-footer">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Footer Content</h5>
                <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2014 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </footer>
            		
	    <script type="text/javascript">
        $(document).ready(function () {
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal-trigger').leanModal();
        });
    </script>


	

<div class="hiddendiv common"></div></body>

</html>