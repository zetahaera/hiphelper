@extends('master')

@section('content')
<div class="container">
	<div class="row">

		<div class="col s3">
			<div class="card">
				<div class="card-content">
			<img class="avatar" src="{{ url('images/avatar.jpg') }}">
			<br>
			<br>
			<a class="waves-effect waves-light btn" style="width:100%;"><i class="material-icons left">publish</i>Upload Foto</a>		
			</div></div>
		</div>

		<div class="col s9">
			<div class="card">
				<div class="card-title black-text" style="padding-left: 20px; padding-top: 20px;">
					Edit Profil

				</div>
				
				<div class="card-content black-text">
					<div class="row">
						<div class="input-field col s8">
							<input value="Niea Kurnia Fajar" id="namaLengkap" type="text" class="validate">
      						<label class="active" for="namaLengkap">Nama Lengkap</label>
						</div>
					</div>
					<ul class="collapsible" data-collapsible="expandable">
							<li>
								<div class="collapsible-header">
									<i class="material-icons">perm_identity</i>
									Akun 							
								</div>
								<div class="collapsible-body">
									<div class="row">
										<div class="input-field col s8">
											
											<input value="nieakurnia@gmail.com" id="email" type="text" class="validate">
											<label class="active" for="email">Email</label>
											
								
										</div>
										
									</div>
									<div class="row">
										<div class="input-field col s4">
											<input value="••••••••••••••••" id="password" type="text" class="validate">
											<label class="active" for="password">Password</label>
											
											
										</div>
										<div class="input-field col s4">
											<input value="••••••••••••••••" id="passwordConfirm" type="text" class="validate">
											<label class="active" for="passwordConfirm">Konfirmasi Password</label>
										</div>
									</div>
									
								</div>
							</li>
							<li>
								<div class="collapsible-header">
									<i class="material-icons">view_list</i>
									Detil 
								</div>
								<div class="collapsible-body">
									<div class="row">
										<div class="input-field col s12">
											
											<textarea id="deskripsiDiri" class="materialize-textarea">Sebagai seorang babysitter yang memiliki pengalaman babysitter selama hampir 5 tahun. Saya menyadari bahwa childcare adalah passion dari saya. Saya senang dengan anak kecil, dan saya selalu bersemangat untuk bermain dengan mereka.</textarea>
          									<label class="active" for="deskripsiDiri">Deskripsi Diri</label>

										</div>
									</div>
									<div class="row">
										<div class="input-field col s8">

											<input value="Memiliki keahlian memasak, mencuci, mengasuh bayi" id="keahlian" type="text" class="validate">
											<label class="active" for="keahlian">Keahlian</label>
												 
										</div>
									</div>
									<div class="row">
										<div class="input-field col s8">
											<input value="https://www.youtube.com/watch?v=zEjSb_TCsIE&t=0s" id="linkVideo" type="text" class="validate">
											<label class="active" for="linkVideo">Link Video Profil</label>

										</div>
									</div>
									<div class="row">
										<div class="input-field col s4">
											
											<input value="Depok" id="tempatLahir" type="text" class="validate">
											<label class="active" for="tempatLahir"> Tempat Lahir</label>

											
										</div>
										<div class="input-field col s4">
											
											 <input id="tanggalLahir" type="date" class="datepicker">
											 <label class="active" for="tanggalLahir">Tanggal Lahir</label>

										
										</div>
									</div>
									<div class="row">
										<div class="input-field col s8">
											
											<input value="Jl. Margonda Raya No.2, Depok Jawa Barat" id="tempatTinggal" type="text" class="validate">
											<label class="active" for="tempatTinggal">Tempat Tinggal</label>
											
										</div>
										
									</div>
									<div class="row">
										<div class="input-field col s4">
											<input value="S1" id="pendTer" type="text" class="validate">
											<label class="active" for="pendTer">Pendidikan Terakhir</label>
										

										</div>
										<div class="input-field col s4">
											<input value="Lajang" id="status" type="text" class="validate">
											<label class="active" for="status">Status</label>

											
										</div>
									</div>
									<div class="row">
										<div class="input-field col s4">
											<input value="Sunda" id="suku" type="text" class="validate">
											<label class="active" for="suku">Suku</label>
										</div>
										<div class="input-field col s4">
											<input value="Islam" id="agama" type="text" class="validate">
											<label class="active" for="agama">Agama</label>

									
										</div>
										
									</div>
									<div class="row">
										<div class="input-field col s4">
											<input value="08132525234" id="noHP" type="text" class="validate">
											<label class="active" for="noHP">Nomor Handphone</label>

										</div>
									</div>

								</div>
							</li>
						</ul>
						<div class="row">
										<div class="col s4 offset-s8">
											<a class="waves-effect waves-light btn maroon" href="{{url('home/babysitter/')}}"><i class="material-icons right">save</i>Simpan</a>
										</div>
									</div>
				</div>
			</div>
		</div>

	</div>
</div>
@stop