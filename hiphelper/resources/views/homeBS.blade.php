@extends('master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col s3">
				<img class="avatar" src="{{ url('images/avatar.jpg') }}">	
			</div>
			<div class="col s9">
				<div class="row">
					<div class="col s8" >
						<div class="row">
							<span class="wrap-text">
								<h4 style="margin-top: 0; margin-bottom: 0;">Niea Kurnia Fajar</h4>
							</span>
						</div>
						
						<div class="row">
								
									<b>Rating : </b>
						         	<i class="tiny material-icons bintang">star</i>
						            <i class="tiny material-icons bintang">star</i>
						            <i class="tiny material-icons bintang">star</i>
						            <i class="tiny material-icons bintang">star</i>
						           	<i class="tiny material-icons bintang">star</i>
					              							 
								
						</div>
						<!-- misalnya di minggu itu ada 2 task babysitting -->
					</div>
					<div class="col s4">
						<div class="row" style="margin-bottom: 0;">
							<span class="wrap-text">
								<a class="waves-effect waves-light red lighten-1 btn"  style="width: 100%;" 
								href="#waiting-confirmation" onclick="Materialize.showStaggeredList('#waiting-confirmation')">2 Waiting Confirmation</a>
							</span>
						</div>
						<div class="row">
							<span class="wrap-text">
								<a class="waves-effect waves-light green accent-4  btn" style="width: 100%;" href="#upcoming-babysitting-task" onclick="Materialize.showStaggeredList('#upcoming-babysitting-task')">2 Upcoming Babysitting Task</a>
							</span>
						</div>
					</div>
				<!-- 	<div class="col s4">
						<span class="wrap-text">
							<a class="waves-effect waves-light red lighten-1 black-text btn" href="#waiting-confirmation" onclick="Materialize.showStaggeredList('#waiting-confirmation')">2 Waiting Confirmation</a>
						</span>
					</div>
					<div class="col s4">
						<span class="wrap-text">
							<a class="waves-effect waves-light green accent-4 black-text btn-large" href="#upcoming-babysitting-task" onclick="Materialize.showStaggeredList('#upcoming-babysitting-task')">2 Upcoming Babysitting Task</a>
						</span>
						
					</div> -->
					
					
				</div>
			<!-- 	<div class="row">
					<div class="col s8">
						<b>Rating : </b>
			         	<i class="tiny material-icons bintang">star</i>
			            <i class="tiny material-icons bintang">star</i>
			            <i class="tiny material-icons bintang">star</i>
			            <i class="tiny material-icons bintang">star</i>
			           	<i class="tiny material-icons bintang">star</i>
		              							 
					</div>
				</div> -->
				<hr>
				<div class="row">
					<div class="col s12">
						<ul class="collapsible" data-collapsible="accordion">
							<li>
								<div class="collapsible-header">
									<i class="material-icons">perm_identity</i>
									Akun 							
								</div>
								<div class="collapsible-body">
									<div class="row">
										<div class="col s12 m12 l4">
											<p>
											<b>Email :</b>
											<br>
											<span class="wrap-text">
												nieakurnia@gmail.com
											</span>
											</p>
										</div>
										<div class="col s12 m12 l4">
											<p>
											<b>Password : </b>
											<br>
											<span class="wrap-text">
												••••••••••••••••
											</span>
											</p>
											
										</div>
									</div>
									<!-- <div class="row">
											<div class="right-align">
												<button class="btn-flat waves-effect waves-light" type="submit" name="action">
													Edit
													<i class="material-icons right">mode_edit</i>
	  											</button>
											</div>
									</div> -->
								</div>
							</li>
							<li>
								<div class="collapsible-header">
									<i class="material-icons">view_list</i>
									Detil 
								</div>
								<div class="collapsible-body">
									<div class="row">
										<div class="col s12">
											
											<b>Deskripsi Diri :</b>
											<br>
											<span class="wrap-text">
												Sebagai seorang babysitter yang memiliki pengalaman babysitter selama hampir 5 tahun. Saya menyadari bahwa childcare adalah passion dari saya. Saya senang dengan anak kecil, dan saya selalu bersemangat untuk bermain dengan mereka.
											</span>
										</div>
									</div>
									<div class="row">
										<div class="col s8">
											<b>Keahlian :</b>
											<br>
											<span class="wrap-text">
												 Memiliki keahlian memasak, mencuci, mengasuh bayi
											</span>
										</div>
									</div>
									<div class="row">
										<div class="col s8">
											<b>Link video Profil :</b>
											<br>
											<span class="wrap-text">
												https://www.youtube.com/watch?v=zEjSb_TCsIE&t=0s
											</span>
										</div>
									</div>
									<div class="row">
										<div class="col s4">
										
											<b>Tempat Lahir :</b>
											<br>
											<span class="wrap-text">
											Depok,
											</span>
										</div>
										<div class="col s4">
											
											<b>Tanggal Lahir :</b>
											<br>
											<span class="wrap-text">
											15 Mei 1995
											</span>
										</div>
									</div>
									<div class="row">
										<div class="col s8">
											<b>Tempat Tinggal : </b>
											<br>
											<span class="wrap-text">
												Jl. Margonda Raya No.2, Depok Jawa Barat
											</span>
										</div>
										
									</div>
									<div class="row">
										<div class="col s4">
											<b>Pendidikan Terakhir :</b>
											<br>
											<span>
												S1
											</span>
										</div>
										<div class="col s4">
											<b>Status :</b>
											<br>
											<span class="wrap-text">
												Lajang
											</span>
										</div>
									</div>
									<div class="row">
										<div class="col s4">
											<b>Suku :</b>
											<br>
											<span class="wrap-text">
												Sunda
											</span>
										</div>
										<div class="col s4">
											<b>Agama :</b>
											<br>
											<span class="wrap-text">
												Islam
											</span>
										</div>
										
									</div>
									<div class="row">
										<div class="col s4">
											<b>Nomor Handphone :</b>
											<br>
											<span class="wrap-text">
												08132525234
											</span>
										</div>
									</div>
								</div>
							</li>
						</ul>
							
					</div>

				</div>
				<!-- <div class="row">
					<div class="col s4 push-s8">
						<a class="waves-effect waves-light btn maroon" href="{{url('home/babysitter/edit') }}"><i class="material-icons right">mode_edit</i>Edit Profil</a>
					</div>
				</div> -->
				
			

			</div>
		</div>
		<div class="row">

			<div class="col s12">
				<div class="card">
					<ul class="tabs">
						<li class="tab col s4">
							<a class="active" href="#booking">Booking</a>
						</li>
						<li class="tab col s4">
							<a 
							href ="#jadwal">Jadwal</a>
						</li>
						<li class="tab col s4">	
							<a href="#review">Review</a>

						</li>
					</ul>

					<div class="card-content black-text">
						<div id="booking">
							<!-- <br>
							<br>
							<br> -->
							<div class="row">
								<div class="col s4 m8 l8">
									<div class="card-title black-text">
										Booking
									</div>
								</div>
								<div class="col s8 m4 l4">
									<a class="waves-effect maroon btn" href="{{url('/babysitter/transaksi')}}"><i class="material-icons left">child_care</i>List Transaksi</a>
									<!-- history transaksi -->
								</div>
							</div>
							<div id="waiting-confirmation"class="card grey lighten-5">
								<div class="card-content black-text">
									<div class="row">
										<div class="col s12">
											<span class="card-title black-text">Menunggu Konfirmasi
												<span class="new badge red lighten-1 black-text">2
												</span>
											</span>
										</div>
										
									</div>
									
									<div id="row" style="padding-left: 14px; padding-right: 14px;">
					        			<div class="col s2 hide-on-med-and-down"><b>Id Pemesanan</b></div>
					        			<div class="col s4 hide-on-med-and-down"><b>Waktu</b></div>
					        			<div class="col s2 hide-on-med-and-down"><b>Lokasi</b></div>
					        			<div class="col s4 hide-on-med-and-down"><span class="wrap-text"><b>Status Pemesanan</b></span></div> 
					        			<div class="col s5 hide-on-large-only"><b>Id Pemesanan</b></div>
					        			<div class="col s7 hide-on-large-only"><b>Status Pemesanan</b></div>      
					    			</div>
			    					<br>
			    					<br>
									<ul class="collapsible" data-collapsible="accordion">
								<li>
									<div class="collapsible-header red lighten-4" style="display: block;overflow: auto;" >
										<div class="col s2 hide-on-med-and-down"><span class="wrap-text">NFK0001</span></div>
			        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Jumat, 18 Nov 2016 ; 06:00 - 18:00</span></div>
			        					<div class="col s2 hide-on-med-and-down"><span class="wrap-text">Depok</span></div>
			        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Menunggu Konfirmasi</span></div>
			        					<div class="col s5 hide-on-large-only"><span class="wrap-text">NFK0001</span></div>
			        					<div class="col s7 hide-on-large-only"><span class="wrap-text">Menunggu Konfirmasi</span></div>		
									</div>
									<div class="collapsible-body">
										
										<div class="row">
											<div class="col s12">
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Booking
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Nama Parent :</b>
																<br>
																<span class="wrap-text">
																	Zahra Zuluthfa
																</span>
															</div>
															<div class="col s5">
																<b>Jumlah Anak :</b>
																<br>
																<span class="wrap-text">
																	2 Anak
																</span>
															</div>
														</div>
														<div class="row black-text">
															<div class="col s10 ">
																<b>Waktu :</b>
																<br>
																<span class="wrap-text">
																	Jumat, 18 Nov 2016 ; 06:00 - 18:00
																</span>
															</div>


														</div>
														<div class="row black-text">
															<div class="col s5">
																<b>Lokasi : </b>
																<br>
																<span class="wrap-text">
																	Depok
																</span>
															</div>
															<div class="col s5">
																<b>Tarif :</b>
																<br>
																<span class="wrap-text">
																	Rp. 250.000
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>		
										<div class="row">
											<div class="col s12">
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Anak
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Jenis Kelamin :</b>
																<br>
																<span class="wrap-text">
																	Perempuan
																</span>
															</div>
															<div class="col s5">
																<b>Umur :</b>
																<br>
																<span class="wrap-text">
																	3 tahun
																</span>
															</div>
														</div>
														<div class="row black-text">
															<div class="col s10">
																<b>Catatan :</b>
																<br>
																<span class="wrap-text">
																	Alergi udang, obt terkahir yang dikomsunsi antibiotik
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
										<div class="row hide-on-med-and-down">
											<div class=" col s6">
												<a class=" modal-trigger waves-effect waves-light btn green" href="#konfirmasiTerima" style="width: 100%;"><i class="material-icons right">done</i>Terima Booking</a>
												
											</div>
											<div class="col s6">
												<a class="modal-trigger waves-effect waves-light btn red" href="#konfirmasiTolak" style="width: 100%;"><i class="material-icons right" >clear</i>Tolak Pesanan</a>
											</div>
										</div>
										<div class="row hide-on-large-only">
											<div class=" col s6">
												<a class="waves-effect waves-light btn green" href="#konfirmasiTerima" style="width: 100%;">Terima</a>
												
											</div>
											<div class="col s6">
												<a class="waves-effect waves-light btn red" href="#konfirmasiTolak" style="width: 100%;">Tolak</a>
											</div>
										</div>

									
									</div>
								</li>
								<li>
									<div class="collapsible-header red lighten-4" style="display: block;overflow: auto;" >
										<div class="col s2 hide-on-med-and-down"><span class="wrap-text">NFK0001</span></div>
			        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Jumat, 18 Nov 2016 ; 06:00 - 18:00</span></div>
			        					<div class="col s2 hide-on-med-and-down"><span class="wrap-text">Depok</span></div>
			        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Menunggu Konfirmasi</span></div>
			        					<div class="col s5 hide-on-large-only"><span class="wrap-text">NFK0001</span></div>
			        					<div class="col s7 hide-on-large-only"><span class="wrap-text">Menunggu Konfirmasi</span></div>		
									</div>
									<div class="collapsible-body">
										
										<div class="row">
											<div class="col s12">
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Booking
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Nama Parent :</b>
																<br>
																<span class="wrap-text">
																	Zahra Zuluthfa
																</span>
															</div>
															<div class="col s5">
																<b>Jumlah Anak :</b>
																<br>
																<span class="wrap-text">
																	2 Anak
																</span>
															</div>
														</div>
														<div class="row black-text">
															<div class="col s10 ">
																<b>Waktu :</b>
																<br>
																<span class="wrap-text">
																	Jumat, 18 Nov 2016 ; 06:00 - 18:00
																</span>
															</div>


														</div>
														<div class="row black-text">
															<div class="col s5">
																<b>Lokasi : </b>
																<br>
																<span class="wrap-text">
																	Depok
																</span>
															</div>
															<div class="col s5">
																<b>Tarif :</b>
																<br>
																<span class="wrap-text">
																	Rp. 250.000
																</span>
															</div>
														</div>	
																													
														
													</div>
												</div>
											</div>
										</div>		
										<div class="row">
											<div class="col s12">
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Anak
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Jenis Kelamin :</b>
																<br>
																<span class="wrap-text">
																	Perempuan
																</span>
															</div>
															<div class="col s5">
																<b>Umur :</b>
																<br>
																<span class="wrap-text">
																	3 tahun
																</span>
															</div>
														</div>
														<div class="row black-text">
															<div class="col s10">
																<b>Catatan :</b>
																<br>
																<span class="wrap-text">
																	Alergi udang, obt terkahir yang dikomsunsi antibiotik
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
										<div class="row hide-on-med-and-down">
											<div class=" col s6">
												<a class=" modal-trigger waves-effect waves-light btn green" href="#konfirmasiTerima" style="width: 100%;"><i class="material-icons right">done</i>Terima Booking</a>
												
											</div>
											<div class="col s6">
												<a class="modal-trigger waves-effect waves-light btn red" href="#konfirmasiTolak" style="width: 100%;"><i class="material-icons right" >clear</i>Tolak Pesanan</a>
											</div>
										</div>
										<div class="row hide-on-large-only">
											<div class=" col s6">
												<a class="waves-effect waves-light btn green" href="#konfirmasiTerima" style="width: 100%;">Terima</a>
												
											</div>
											<div class="col s6">
												<a class="waves-effect waves-light btn red" href="#konfirmasiTolak" style="width: 100%;">Tolak</a>
											</div>
										</div>

									
									</div>
								</li>

									</ul>
								</div>
							</div>
							
							<div id="waiting-payment"class="card grey lighten-5">
								<div class="card-content black-text">
									<div class="row">
										<div class="col s8">
											<span class="card-title black-text">Menunggu Pembayaran
												<span class="new badge yellow black-text">2
												</span>
											</span>
										</div>
										
									</div>
									
									<div id="row" style="padding-left: 14px; padding-right: 14px;">
					        			<div class="col s2 hide-on-med-and-down"><b>Id Pemesanan</b></div>
					        			<div class="col s4 hide-on-med-and-down"><b>Waktu</b></div>
					        			<div class="col s2 hide-on-med-and-down"><b>Lokasi</b></div>
					        			<div class="col s4 hide-on-med-and-down"><span class="wrap-text"><b>Status Pemesanan</b></span></div> 
					        			<div class="col s5 hide-on-large-only"><b>Id Pemesanan</b></div>
					        			<div class="col s7 hide-on-large-only"><b>Status Pemesanan</b></div>      
					    			</div>
			    					<br>
			    					<br>

									<ul class="collapsible" data-collapsible="accordion">
										<li>
											
											<div class="collapsible-header yellow lighten-2" style="display: block; overflow: auto;">
												<div class="col s2 hide-on-med-and-down"><span class="wrap-text"> NFK0001</span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text"> Jumat, 18 Nov 2016 ; 07:00 - 17:00</span></div>
					        					<div class="col s2 hide-on-med-and-down"><span class="wrap-text"> Depok </span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text"> Menunggu Pembayaran</span></div>
					        					<div class="col s5 hide-on-large-only"><span class="wrap-text"> NFK0001</span></div>
					        					<div class="col s7 hide-on-large-only"><span class="wrap-text">Menunggu Pembayaran</span></div>
											</div>
											<div class="collapsible-body">
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Waktu :</b>
																		<br>
																		<span class="wrap-text">
																			Jumat, 18 Nov 2016 ; 07:00 - 17:00
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Lokasi :</b>
																		<br>
																		<span class="wrap-text">
																			Depok
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Tarif :</b>
																		<br>
																		<span class="wrap-text">
																			Tarif : 250.000
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>		
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- <div class="row">
													<div class=" col s7 push-s5">
														<a class="waves-effect waves-light btn green" href="#konfirmasiTerima"><i class="material-icons right">done</i>Terima Booking</a>
														<a class="waves-effect waves-light btn red" href="#konfirmasiTolak"><i class="material-icons right">clear</i>Tolak Pesanan</a>
													</div>
												</div> -->	
												<!-- <div id="konfirmasiTerima" class="modal modal-fixed-footer">
													<div class="modal-content">
														<h5>Apakah Anda Yakin Menerima Pesanan?</h5>
														<div class="row">
															<div class="col s10">
																<b>Id Pemesanan : </b>
																<br>
																<span class="wrap-text">
																	NFK0001
																</span>
															</div>
														</div>
														<div class="row">
															<div class="col s5">
																<b>Waktu Pemesanan :</b>
																<br>
																<span class="wrap-text">
																	Jumat, 18 Nov 2016 ; 07:00 - 17:00
																</span>
															</div>
															<div class="col s5">
																<b>Lokasi :</b>
																<br>
																<span class="wrap-text">
																	Depok
																</span>
															</div>
															
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<div class="col s7-push-s5">
															<a class="waves-effect waves-teal btn-flat"><i class="material icons-right">cancel</i>Cancel</a>
															<a class="waves-effect waves-light btn green" href="#"><i class="material-icons right">done</i>Terima Booking</a>
														</div>
													</div>
												</div> -->
											</div>
										</li>
										<li>
											
											<div class="collapsible-header yellow lighten-2" style="display: block; overflow: auto;">
												<div class="col s2 hide-on-med-and-down"><span class="wrap-text"> NFK0001</span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text"> Jumat, 18 Nov 2016 ; 07:00 - 17:00</span></div>
					        					<div class="col s2 hide-on-med-and-down"><span class="wrap-text"> Depok </span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text"> Menunggu Pembayaran</span></div>
					        					<div class="col s5 hide-on-large-only"><span class="wrap-text"> NFK0001</span></div>
					        					<div class="col s7 hide-on-large-only"><span class="wrap-text">Menunggu Pembayaran</span></div>
											</div>
											<div class="collapsible-body">
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Waktu :</b>
																		<br>
																		<span class="wrap-text">
																			Jumat, 18 Nov 2016 ; 07:00 - 17:00
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Lokasi :</b>
																		<br>
																		<span class="wrap-text">
																			Depok
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Tarif :</b>
																		<br>
																		<span class="wrap-text">
																			Tarif : 250.000
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>		
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- <div class="row">
													<div class=" col s7 push-s5">
														<a class="waves-effect waves-light btn green" href="#konfirmasiTerima"><i class="material-icons right">done</i>Terima Booking</a>
														<a class="waves-effect waves-light btn red" href="#konfirmasiTolak"><i class="material-icons right">clear</i>Tolak Pesanan</a>
													</div>
												</div> -->	
												<!-- <div id="konfirmasiTerima" class="modal modal-fixed-footer">
													<div class="modal-content">
														<h5>Apakah Anda Yakin Menerima Pesanan?</h5>
														<div class="row">
															<div class="col s10">
																<b>Id Pemesanan : </b>
																<br>
																<span class="wrap-text">
																	NFK0001
																</span>
															</div>
														</div>
														<div class="row">
															<div class="col s5">
																<b>Waktu Pemesanan :</b>
																<br>
																<span class="wrap-text">
																	Jumat, 18 Nov 2016 ; 07:00 - 17:00
																</span>
															</div>
															<div class="col s5">
																<b>Lokasi :</b>
																<br>
																<span class="wrap-text">
																	Depok
																</span>
															</div>
															
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<div class="col s7-push-s5">
															<a class="waves-effect waves-teal btn-flat"><i class="material icons-right">cancel</i>Cancel</a>
															<a class="waves-effect waves-light btn green" href="#"><i class="material-icons right">done</i>Terima Booking</a>
														</div>
													</div>
												</div> -->
											</div>
										</li>

									</ul>
								</div>
							</div>
			    			
							<div id="upcoming-babysitting-task"class="card grey lighten-5">
								<div class="card-content black-text">
									<div class="row">
										<div class="col s8">
											<span class="card-title black-text">Upcoming Babysitting Task
												<span class="new badge  green accent-4 black-text">2
												</span>
												<!-- kalo udah dibayar dan nunggu hari H-->
											</span>
										</div>
										
									</div>
									
									<div id="row" style="padding-left: 14px; padding-right: 14px;">
					        			<div class="col s2 hide-on-med-and-down"><b>Id Pemesanan</b></div>
					        			<div class="col s4 hide-on-med-and-down"><b>Waktu</b></div>
					        			<div class="col s2 hide-on-med-and-down"><b>Lokasi</b></div>
					        			<div class="col s4 hide-on-med-and-down"><span class="wrap-text"><b>Status Pemesanan</b></span></div> 
					        			<div class="col s5 hide-on-large-only"><b>Id Pemesanan</b></div>
					        			<div class="col s7 hide-on-large-only"><b>Status Pemesanan</b></div>      
					    			</div>
					    			<br>
					    			<br>

									<ul class="collapsible" data-collapsible="accordion">
										<li>
											<div class="collapsible-header  green accent-2" style="display: block; overflow: auto;">
												<div class="col s2 hide-on-med-and-down"><span class="wrap-text">NFK0002</span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Jumat, 2 Des 2016 ; 07:00 - 17:00</span></div>
					        					<div class="col s2 hide-on-med-and-down"><span class="wrap-text"> Kelapa Dua</span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Menunggu Waktu Babysitting<span></div>		
					        					<div class="col s5 hide-on-large-only"><span class="wrap-text">NFK0002</span></div>
					        					<div class="col s7 hide-on-large-only"><span class="wrap-text">Menunggu Waktu Babysitting</span></div>
											</div>
											<div class="collapsible-body">
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Alamat :</b>
																		<br>
																		<span class="wrap-text">
																			Jl. Kedondong No. 1 Perumahan Depok Indah, Depok
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b> Waktu :</b>
																		<br>
																		<span class="wrap-text">
																			Jumat, 2 Des 2016 ; 07:00 - 17:00
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nomor HP Parent :</b>
																		<br>
																		<span class="wrap-text">
																			08782346823
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Tarif :</b>
																		<br>
																		<span class="wrap-text">
																			Rp. 250.000
																		</span>
																	</div>
																</div>
															
															</div>
														</div>
													</div>
												</div>		
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col s12">
														<ul class="collection with-header">
															<li class="collection-header"><h4>SoP for Babysitter</h4></li>
															<li class="collection-item">
																Menjalin komunikasi yang lancar dan nyaman dengan orang tua, termasuk berupa menggunakan etika dan bahasa yang sopan, jujur, dan terbuka.</li>
															</li>
															<li class="collection-item">
																Menggunakan kemeja/kaos berlengan dengan outer dan celana atau rok bahan ketika bekerja. Dilarang menggunakan hanya kaos oblong dan celana jeans.
															</li>
															<li class="collection-item">
																Memberikan form parental consent pada orang tua saat bertemu sebelum menjalankan tugas. Pastikan isi profil dan semua kebutuhan anak benar dan lengkap tertulis.
															</li>
															<li class="collection-item"> 
																Wajib menyiapkan SAK (Satuan Aktivitas Kegiatan) minimal 1 kegiatan dan jadwal harian untuk anak dan diperlihatkan kepada orang tua saat bertemu di awal.
															</li>
															<li class="collection-item">
																Wajib memberi kabar kepada orang tua minimal 2 jam sekali berupa telpon, video call, pengiriman foto anak melalui chat, dll. sesuai kesepakatan dengan orang tua.
															</li>
															<li class="collection-item">
																Tidak mengajak anak pergi ke lokasi lain, memberi asupan, dll tanpa seizin orang tua telah melakukan kontak terlebih dahulu untuk meminta izin/dalam kasus darurat.
															</li>
															<li class="collection-item">
																Setelah bertugas, Babysitter wajib memberikan laporan harian berisi nama anak, usia anak, kegiatan yang dilakukan anak hari itu beserta jamnya, makanan & minuman yang dikonsumsi anak hari itu, serta pencapaian & hambatan yang terjadi pada anak hari itu.
															</li>
															
														</ul> 
													</div>
													
												</div>
												<!-- <div class="row">
													<div class=" col s7 push-s5">
														<a class="waves-effect waves-light btn green" href="#konfirmasiTerima"><i class="material-icons right">done</i>Terima Booking</a>
														<a class="waves-effect waves-light btn red" href="#konfirmasiTolak"><i class="material-icons right">clear</i>Tolak Pesanan</a>
													</div>
												</div>	 -->
												<!-- <div id="konfirmasiTerima" class="modal modal-fixed-footer">
													<div class="modal-content">
														<h5>Apakah Anda Yakin Menerima Pesanan?</h5>
														<div class="row">
															<div class="col s10">
																<b>Id Pemesanan : </b>
																<br>
																<span class="wrap-text">
																	NFK0001
																</span>
															</div>
														</div>
														<div class="row">
															<div class="col s5">
																<b>Waktu Pemesanan :</b>
																<br>
																<span class="wrap-text">
																	Jumat, 18 Nov 2016 ; 07:00 - 17:00
																</span>
															</div>
															<div class="col s5">
																<b>Lokasi :</b>
																<br>
																<span class="wrap-text">
																	Depok
																</span>
															</div>
															
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<div class="col s7-push-s5">
															<a class="waves-effect waves-teal btn-flat"><i class="material icons-right">cancel</i>Cancel</a>
															<a class="waves-effect waves-light btn green" href="#"><i class="material-icons right">done</i>Terima Booking</a>
														</div>
													</div>
												</div> -->
											</div>
										</li>
										<li>

										
											<div class="collapsible-header  green accent-2" style="display: block; overflow: auto;">
												<div class="col s2 hide-on-med-and-down"><span class="wrap-text">NFK0002</span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Jumat, 2 Des 2016 ; 07:00 - 17:00</span></div>
					        					<div class="col s2 hide-on-med-and-down"><span class="wrap-text"> Kelapa Dua</span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Menunggu Waktu Babysitting<span></div>		
					        					<div class="col s5 hide-on-large-only"><span class="wrap-text">NFK0002</span></div>
					        					<div class="col s7 hide-on-large-only"><span class="wrap-text">Menunggu Waktu Babysitting</span></div>
											</div>
											<div class="collapsible-body">
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Alamat :</b>
																		<br>
																		<span class="wrap-text">
																			Jl. Kedondong No. 1 Perumahan Depok Indah, Depok
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b> Waktu :</b>
																		<br>
																		<span class="wrap-text">
																			Jumat, 2 Des 2016 ; 07:00 - 17:00
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nomor HP Parent :</b>
																		<br>
																		<span class="wrap-text">
																			08782346823
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Tarif :</b>
																		<br>
																		<span class="wrap-text">
																			Rp. 250.000
																		</span>
																	</div>
																</div>
															
															</div>
														</div>
													</div>
												</div>		
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col s12">
														<ul class="collection with-header">
															<li class="collection-header"><h4>SoP for Babysitter</h4></li>
															<li class="collection-item">
																Menjalin komunikasi yang lancar dan nyaman dengan orang tua, termasuk berupa menggunakan etika dan bahasa yang sopan, jujur, dan terbuka.</li>
															</li>
															<li class="collection-item">
																Menggunakan kemeja/kaos berlengan dengan outer dan celana atau rok bahan ketika bekerja. Dilarang menggunakan hanya kaos oblong dan celana jeans.
															</li>
															<li class="collection-item">
																Memberikan form parental consent pada orang tua saat bertemu sebelum menjalankan tugas. Pastikan isi profil dan semua kebutuhan anak benar dan lengkap tertulis.
															</li>
															<li class="collection-item"> 
																Wajib menyiapkan SAK (Satuan Aktivitas Kegiatan) minimal 1 kegiatan dan jadwal harian untuk anak dan diperlihatkan kepada orang tua saat bertemu di awal.
															</li>
															<li class="collection-item">
																Wajib memberi kabar kepada orang tua minimal 2 jam sekali berupa telpon, video call, pengiriman foto anak melalui chat, dll. sesuai kesepakatan dengan orang tua.
															</li>
															<li class="collection-item">
																Tidak mengajak anak pergi ke lokasi lain, memberi asupan, dll tanpa seizin orang tua telah melakukan kontak terlebih dahulu untuk meminta izin/dalam kasus darurat.
															</li>
															<li class="collection-item">
																Setelah bertugas, Babysitter wajib memberikan laporan harian berisi nama anak, usia anak, kegiatan yang dilakukan anak hari itu beserta jamnya, makanan & minuman yang dikonsumsi anak hari itu, serta pencapaian & hambatan yang terjadi pada anak hari itu.
															</li>
															
														</ul> 
													</div>
													
												</div>
												<!-- <div class="row">
													<div class=" col s7 push-s5">
														<a class="waves-effect waves-light btn green" href="#konfirmasiTerima"><i class="material-icons right">done</i>Terima Booking</a>
														<a class="waves-effect waves-light btn red" href="#konfirmasiTolak"><i class="material-icons right">clear</i>Tolak Pesanan</a>
													</div>
												</div>	 -->
												<!-- <div id="konfirmasiTerima" class="modal modal-fixed-footer">
													<div class="modal-content">
														<h5>Apakah Anda Yakin Menerima Pesanan?</h5>
														<div class="row">
															<div class="col s10">
																<b>Id Pemesanan : </b>
																<br>
																<span class="wrap-text">
																	NFK0001
																</span>
															</div>
														</div>
														<div class="row">
															<div class="col s5">
																<b>Waktu Pemesanan :</b>
																<br>
																<span class="wrap-text">
																	Jumat, 18 Nov 2016 ; 07:00 - 17:00
																</span>
															</div>
															<div class="col s5">
																<b>Lokasi :</b>
																<br>
																<span class="wrap-text">
																	Depok
																</span>
															</div>
															
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<div class="col s7-push-s5">
															<a class="waves-effect waves-teal btn-flat"><i class="material icons-right">cancel</i>Cancel</a>
															<a class="waves-effect waves-light btn green" href="#"><i class="material-icons right">done</i>Terima Booking</a>
														</div>
													</div>
												</div> -->
											</div>
										</li>

									</ul>
								</div>
							</div>
						</div>
						<div id="jadwal">
							<div class="card-title black-text" style="padding-left: 20px; padding-top: 20px">
								Jadwal
							</div>
						</div>
						<div id="review">
							<span class="card-title black-text" style="padding-left: 20px; padding-top: 20px">
								Review
							</span>
							<div class="card-panel grey lighten-5">
								<div class="row valign-wrapper">
									<div class="col s1">
										<img class="circle" style="width:50px;" src ="{{ url('images/avatar.jpg') }}">	

									</div>
									<div class="col s11">
										<h5>Zahra Zuluthfa</h5>
										<i class="tiny material-icons bintang">star</i>
							            <i class="tiny material-icons bintang">star</i>
							            <i class="tiny material-icons bintang">star</i>
							            <i class="tiny material-icons bintang">star</i>
							           	<i class="tiny material-icons bintang">star</i>
										<span class="black-text"><br>
                							This is a square image. Add the "circle" class to it to make it appear circular.
              							</span>
									</div>
								</div>

							</div>
							<div class="card-panel grey lighten-5">
								<div class="row valign-wrapper">
									<div class="col s1">
										<img class="circle" style="width:50px;" src ="{{ url('images/avatar.jpg') }}">	

									</div>
									<div class="col s11">
										<h5>Zahra Zuluthfa</h5>
										<i class="tiny material-icons bintang">star</i>
							            <i class="tiny material-icons bintang">star</i>
							            <i class="tiny material-icons bintang">star</i>
							            <i class="tiny material-icons bintang">star</i>
							           	<i class="tiny material-icons bintang">star</i>
										<span class="black-text"><br>
                							This is a square image. Add the "circle" class to it to make it appear circular.
              							</span>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div id="konfirmasiTerima" class="modal modal-fixed-footer">
											<div class="modal-content">
												<h5>Apakah Anda Yakin Menerima Pesanan?</h5>
												<div class="row">
													<div class="col s10">
														<b>Id Pemesanan : </b>
														<br>
														<span class="wrap-text">
															NFK0001
														</span>
													</div>
												</div>
												<div class="row">
													<div class="col s5">
														<b>Waktu Pemesanan :</b>
														<br>
														<span class="wrap-text">
															Jumat, 18 Nov 2016 ; 07:00 - 17:00
														</span>
													</div>
													<div class="col s5">
														<b>Lokasi :</b>
														<br>
														<span class="wrap-text">
															Depok
														</span>
													</div>
													
												</div>
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Booking
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Nama Parent :</b>
																<br>
																<span class="wrap-text">
																	Zahra Zuluthfa
																</span>
															</div>
															<div class="col s5">
																<b>Jumlah Anak :</b>
																<br>
																<span class="wrap-text">
																	2 Anak
																</span>
															</div>
														</div>
													</div>
												</div>
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Anak
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Jenis Kelamin :</b>
																<br>
																<span class="wrap-text">
																	Perempuan
																</span>
															</div>
															<div class="col s5">
																<b>Umur :</b>
																<br>
																<span class="wrap-text">
																	3 tahun
																</span>
															</div>
														</div>
														<div class="row black-text">
															<div class="col s10">
																<b>Catatan :</b>
																<br>
																<span class="wrap-text">
																	Alergi udang, obt terkahir yang dikomsunsi antibiotik
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<div class="col s7-push-s5">
													<a class="waves-effect waves-teal btn-flat"><i class="material-icons right">close</i>Cancel</a>
													<a class="waves-effect waves-light btn green" href="#"><i class="material-icons right">done</i>Terima Booking</a>
												</div>
											</div>
					</div>
					<div id="konfirmasiTolak" class="modal modal-fixed-footer">
						<div class="modal-content"> 
							<h5>Apakah Anda Yakin Menolak Pesanan?</h5>
												<div class="row">
													<div class="col s10">
														<b>Id Pemesanan : </b>
														<br>
														<span class="wrap-text">
															NFK0001
														</span>
													</div>
												</div>
												<div class="row">
													<div class="col s5">
														<b>Waktu Pemesanan :</b>
														<br>
														<span class="wrap-text">
															Jumat, 18 Nov 2016 ; 07:00 - 17:00
														</span>
													</div>
													<div class="col s5">
														<b>Lokasi :</b>
														<br>
														<span class="wrap-text">
															Depok
														</span>
													</div>
													
												</div>
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Booking
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Nama Parent :</b>
																<br>
																<span class="wrap-text">
																	Zahra Zuluthfa
																</span>
															</div>
															<div class="col s5">
																<b>Jumlah Anak :</b>
																<br>
																<span class="wrap-text">
																	2 Anak
																</span>
															</div>
														</div>
													</div>
												</div>
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Anak
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Jenis Kelamin :</b>
																<br>
																<span class="wrap-text">
																	Perempuan
																</span>
															</div>
															<div class="col s5">
																<b>Umur :</b>
																<br>
																<span class="wrap-text">
																	3 tahun
																</span>
															</div>
														</div>
														<div class="row black-text">
															<div class="col s10">
																<b>Catatan :</b>
																<br>
																<span class="wrap-text">
																	Alergi udang, obt terkahir yang dikomsunsi antibiotik
																</span>
															</div>
														</div>
													</div>
												</div>
						</div>
						<div class="modal-footer">
												<div class="col s7-push-s5">
													<a class="waves-effect waves-teal btn-flat"><i class="material-icons right">close</i>Cancel</a>
													<a class="waves-effect waves-light btn red" href="#"><i class="material-icons right">close</i>Tolak Booking</a>
												</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop