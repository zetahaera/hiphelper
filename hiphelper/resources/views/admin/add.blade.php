@extends('layouts.dashboard')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content ">
    
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li> <a href="#">UI Elements</a> </li>
            <li><a href="#" class="active">Form layouts</a> </li>
        </ul>
        <!-- END BREADCRUMB -->
        <!-- START PANEL -->
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="panel-title">Separated form layouts </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-10">
                        <h3>Simple but not simpler, Seperate your forms and create diversified info graphic</h3>
                        <p>Want it to be more Descriptive and User Friendly, We Made it possible, Use Seperated Form Layouts Structure to Presentate your Form Fields. </p>
                        <br>
                        <p class="small hint-text">To Add A full Width Portlet - Class - portlet-full This can be used in any
                            <br> widget or situation, Highly Recomended on Forms and tables</p>
                        <form id="form-work" class="form-horizontal" role="form" autocomplete="off">
                            <div class="form-group">
                                <label for="fname" class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="fname" placeholder="Full name" name="name" required> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Your gender</label>
                                <div class="col-sm-9">
                                    <div class="radio radio-success">
                                        <input type="radio" value="male" name="optionyes" id="male">
                                        <label for="male">Male</label>
                                        <input type="radio" checked="checked" value="female" name="optionyes" id="female">
                                        <label for="female">Female</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Work</label>
                                <div class="col-sm-9">
                                    <p>Have you Worked at page Inc. before, Or joined the Pages Supirior Club?</p>
                                    <p class="hint-text small">If yes State which Place, if yes note date and Job CODE / Membership Number</p>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" required> </div>
                                        <div class="col-sm-5 sm-m-t-10">
                                            <input type="text" placeholder="Code/Number" class="form-control"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="position" class="col-sm-3 control-label">Position applying for</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="position" placeholder="Designation" required> </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="name" placeholder="Briefly Describe your Abilities"></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-3">
                                    <p>I hereby certify that the information above is true and accurate. </p>
                                </div>
                                <div class="col-sm-9">
                                    <button class="btn btn-success" type="submit">Submit</button>
                                    <button class="btn btn-default"><i class="pg-close"></i> Clear</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PANEL -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>

@stop