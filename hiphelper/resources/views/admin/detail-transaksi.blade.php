@extends('layouts.dashboard')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content ">
    
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li> <a href="#">Detail Transkasi</a> </li>
            <li><a href="#" class="active">detail Transaksi</a> </li>
        </ul>
        <!-- END BREADCRUMB -->
        <!-- START PANEL -->
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="panel-title">Detail Transaksi </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-10">
                        <form id="form-work" class="form-horizontal" role="form" autocomplete="off">
                            <div class="form-group">
                                <label for="booking-date" class="col-sm-3 control-label">Bukti Transfer</label>
                                <div class="col-sm-9">
                                    <!--<img id="image" src="{{ url('images/struk-atm.jpg') }}" style="width: 200px;margin-top: 25px;"  />-->
                                    <img id="zoomImg" src="{{ url('images/struk-atm.jpg') }}" alt="Bukti Transfer" width="300">
                                    <!-- The Modal -->
                                    <div id="imgModal" class="modal">
                                        <span class="close">&times;</span>
                                        <img class="modal-content" id="img01">
                                        <div id="caption"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="id-pemesanan" class="col-sm-3 control-label">Id Pemesanan</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="id-pemesanan" placeholder="xxx-xxx" name="id-pemesanan" value="NKF001" disabled> </div>
                            </div>
                            <div class="form-group">
                                <label for="name-parent" class="col-sm-3 control-label">Nama Orang Tua</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name-parent" placeholder="Nama Orang Tua" name="name-parent" value="Zahra Zuluthfa" disabled> </div>
                            </div>
                            <div class="form-group">
                                <label for="text-of-child" class="col-sm-3 control-label">Jumlah Anak</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="text-of-child" placeholder="Jumlah Anak" name="text-of-child" value="2" disabled> </div>
                            </div>
                            <div class="form-group">
                                <label for="gender-of-child" class="col-sm-3 control-label">Jenis Kelamin Anak 1</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="gender-of-child" placeholder="perempuan" name="gender-of-child" value="Perempuan" disabled> </div>
                            </div>
                            <div class="form-group">
                                <label for="gender-of-child2" class="col-sm-3 control-label">Jenis Kelamin Anak 2</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="gender-of-child2" placeholder="laki-laki" name="gender-of-child2" value="Laki-laki" disabled> </div>
                            </div>
                            <div class="form-group">
                                <label for="rek-no" class="col-sm-3 control-label">Nomor Rekening</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="rek-no" placeholder="3083-01-000-982-50-9" name="rek-no" value="3083-01-000-982-50-9" disabled> </div>
                            </div>
                            <div class="form-group">
                                <label for="transfer-date" class="col-sm-3 control-label">Tanggal Transfer</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="transfer-date" placeholder="2 Des 2016 ; 10:00" name="transfer-date" value="2 Des 2016 ; 10:00" disabled> </div>
                            </div>
                            <div class="form-group">
                                <label for="transefer-amount" class="col-sm-3 control-label">Jumlah Transfer</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="transefer-amount" placeholder="600000" name="transfer-amount" value="600000" disabled> </div>
                            </div>
                            <div class="form-group">
                                <label for="confirmation" class="col-sm-3 control-label">Status Konfirmasi</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="confirmation" placeholder="Konfirmasi" name="confirmation" value="Menunggu Konfirmasi" disabled> </div>
                            </div>
                             <div class="form-group">
                                <label for="name-babysitter" class="col-sm-3 control-label">Nama Babysitter</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name-babysitter" placeholder="Nama Babysitter" name="name-parent" value="Dyah K." disabled> </div>
                            </div>
                            <div class="form-group">
                                <label for="booking-date" class="col-sm-3 control-label">Jadwal Pemesanan</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="booking-date" placeholder="18 Nov 2016 ; 07:00 - 17:00" name="booking-date" value="18 Nov 2016 ; 07:00 - 17:00" disabled> </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-3">
                                    <p>Konfirmasi status pembayaran </p>
                                </div>
                                <div class="col-sm-9">
                                    <button class="btn btn-success" type="submit">Terima</button>
                                    <button class="btn btn-danger" type="submit">Tolak</button>
                                    <div class="pull-right">
                                        <a class="btn btn-default" href="{{ url('/dashboard/kelola-transaksi/') }}"><i class="pg-close"></i> Batal</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PANEL -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>

<script>
    document.getElementById("foto").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };
</script>

@stop