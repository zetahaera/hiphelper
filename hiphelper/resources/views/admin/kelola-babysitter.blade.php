@extends('layouts.dashboard')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content ">
    
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li> <a href="#">Dashboard</a> </li>
            <li><a href="#" class="active">Babysitter</a> </li>
        </ul>
        <!-- END BREADCRUMB -->
        <!-- START PANEL -->
        <div class="panel panel-transparent">
            <div class="panel-heading">
            <div class="panel-title">List Babysitter
            </div>
            <div class="pull-right">
                <div class="col-xs-12">
                <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                </div>
            </div>
            <div class="pull-right">
                <div class="col-xs-12">
                <a class="btn btn-danger" href="{{ url('/dashboard/kelola-babysitter/tambah') }}">Tambah Babysitter</a>
                </div>
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="panel-body">
            <table class="table table-hover demo-table-search" id="tableWithSearch">
                <thead>
                <tr>
                    <th>Id Babysitter</th>
                    <th>Nama Lengkap</th>
                    <th>Tanggal Pendafataran </th>
                    <th>Detail</th>
                    <th>Status Babysitter</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="v-align-middle semi-bold">
                    <p>001</p>
                    </td>
                    <td class="v-align-middle"> <p>Dyah Nabila</p>
                    </td>
                    <td class="v-align-middle">
                     <p>April 13,2014 10:13</p>
                    </td>
                    <td class="v-align-middle">
                    <a class="btn" href="{{ url('/dashboard/kelola-babysitter/detail') }}">Detail</a>
                    </td>
                    <td class="v-align-middle" style="text-align:center;">
                    non active <input type="checkbox" data-init-plugin="switchery" data-size="small" data-color="success" checked="checked" data-switchery="true" style="display: none;"> active
                    </td>
                </tr>
                 <tr>
                    <td class="v-align-middle semi-bold">
                    <p>003</p>
                    </td>
                    <td class="v-align-middle"> <p>Kamila Nabila</p>
                    </td>
                    <td class="v-align-middle">
                     <p>April 13,2014 10:13</p>
                    </td>
                    <td class="v-align-middle">
                      <a class="btn" href="{{ url('/dashboard/kelola-babysitter/detail') }}">Detail</a>
                    </td>
                    <td class="v-align-middle" style="text-align:center;">
                    non active <input type="checkbox" data-init-plugin="switchery" data-size="small" data-color="success" checked="checked" data-switchery="true" style="display: none;"> active
                    </td>
                </tr>
                 <tr>
                    <td class="v-align-middle semi-bold">
                    <p>003</p>
                    </td>
                    <td class="v-align-middle"> <p>Aminah Nabila</p>
                    </td>
                    <td class="v-align-middle">
                     <p>April 13,2014 10:13</p>
                    </td>
                    <td class="v-align-middle">
                      <a class="btn" href="{{ url('/dashboard/kelola-babysitter/detail') }}">Detail</a>
                    </td>
                    <td class="v-align-middle" style="text-align:center;">
                    non active <input type="checkbox" data-init-plugin="switchery" data-size="small" data-color="success" checked="checked" data-switchery="true" style="display: none;"> active
                    </td>
                </tr>
                  <tr>
                    <td class="v-align-middle semi-bold">
                    <p>004</p>
                    </td>
                    <td class="v-align-middle"> <p>Zahra Nabila</p>
                    </td>
                    <td class="v-align-middle">
                     <p>April 13,2014 10:13</p>
                    </td>
                    <td class="v-align-middle">
                     <a class="btn" href="{{ url('/dashboard/kelola-babysitter/detail') }}">Detail</a>
                    </td>
                    <td class="v-align-middle" style="text-align:center;">
                    non active <input type="checkbox" data-init-plugin="switchery" data-size="small" data-color="success" checked="checked" data-switchery="true" style="display: none;"> active
                    </td>
                </tr>
                    <tr>
                    <td class="v-align-middle semi-bold">
                    <p>005</p>
                    </td>
                    <td class="v-align-middle"> <p>Deni Kurniawan</p>
                    </td>
                    <td class="v-align-middle">
                     <p>April 13,2014 10:13</p>
                    </td>
                    <td class="v-align-middle">
                      <a class="btn" href="{{ url('/dashboard/kelola-babysitter/detail') }}">Detail</a>
                    </td>
                    <td class="v-align-middle" style="text-align:center;">
                    non active <input type="checkbox" data-init-plugin="switchery" data-size="small" data-color="success" checked="checked" data-switchery="true" style="display: none;"> active
                    </td>
                </tr>
                 
                    <tr>
                    <td class="v-align-middle semi-bold">
                    <p>006</p>
                    </td>
                    <td class="v-align-middle"> <p>Budi Raharjo</p>
                    </td>
                    <td class="v-align-middle">
                     <p>April 13,2014 10:13</p>
                    </td>
                    <td class="v-align-middle">
                      <a class="btn" href="{{ url('/dashboard/kelola-babysitter/detail') }}">Detail</a>
                    </td>
                    <td class="v-align-middle" style="text-align:center;">
                    non active <input type="checkbox" data-init-plugin="switchery" data-size="small" data-color="success" checked="checked" data-switchery="true" style="display: none;"> active
                    </td>
                </tr>
                 <tr>
                    <td class="v-align-middle semi-bold">
                    <p>007</p>
                    </td>
                    <td class="v-align-middle"> <p>Tantio</p>
                    </td>
                    <td class="v-align-middle">
                     <p>April 13,2014 10:13</p>
                    </td>
                    <td class="v-align-middle">
                      <a class="btn" href="{{ url('/dashboard/kelola-babysitter/detail') }}">Detail</a>
                    </td>
                    <td class="v-align-middle" style="text-align:center;">
                    non active <input type="checkbox" data-init-plugin="switchery" data-size="small" data-color="success" checked="checked" data-switchery="true" style="display: none;"> active
                    </td>
                </tr>
                 <tr>
                    <td class="v-align-middle semi-bold">
                    <p>008</p>
                    </td>
                    <td class="v-align-middle"> <p>Bob Wibisona</p>
                    </td>
                    <td class="v-align-middle">
                     <p>April 13,2014 10:13</p>
                    </td>
                    <td class="v-align-middle">
                     <a class="btn" href="{{ url('/dashboard/kelola-babysitter/detail') }}">Detail</a>
                    </td>
                    <td class="v-align-middle" style="text-align:center;">
                    non active <input type="checkbox" data-init-plugin="switchery" data-size="small" data-color="success" checked="checked" data-switchery="true" style="display: none;"> active
                    </td>
                </tr>



                </tbody>
            </table>
            </div>
        </div>
        <!-- END PANEL -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>

@stop