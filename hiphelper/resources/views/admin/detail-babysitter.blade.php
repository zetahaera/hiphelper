@extends('layouts.dashboard')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content ">
    
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li> <a href="#">Babysitter</a> </li>
            <li><a href="#" class="active">Edit Profile</a> </li>
        </ul>
        <!-- END BREADCRUMB -->
        <!-- START PANEL -->
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="panel-title">Detail Profile Babysitter </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-10">
                        <form id="form-work" class="form-horizontal" role="form" autocomplete="off">
                            <div class="form-group">
                                <label for="fname" class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="fname" placeholder="Nama" name="name" value="Dyah Nabila" required> </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="email" placeholder="babysitter@gmail.com" name="email" value="dyah@gmail.com" required> </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" id="password" placeholder="" name="password" value="" required> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Jenis Kelamin</label>
                                <div class="col-sm-9">
                                    <div class="radio radio-success">
                                        <input type="radio" value="male" name="optionyes" id="male">
                                        <label for="male">Laki-laki</label>
                                        <input type="radio" checked="checked" value="female" name="optionyes" id="female">
                                        <label for="female">Perempuan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ethnic" class="col-sm-3 control-label">Suku</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="ethnic" placeholder="suku" name="ethnic" value="Jawa" required> </div>
                            </div>
                            <div class="form-group">
                                <label for="education" class="col-sm-3 control-label">Pendidikan terakhir</label>
                                <div class="col-sm-9">
                                    <!--<input type="text" class="form-control" id="education" placeholder="Designation" required> </div>-->
                                    <select id="education" name="education" data-init-plugin="select2">
                                        <option value="SD">SD</option>
                                        <option value="SMP">SMP</option>
                                        <option value="SMA" selected>SMA</option>
                                        <option value="D1">D1</option>
                                        <option value="D2">D2</option>
                                        <option value="D3">D3</option>
                                        <option value="D4">D4</option>
                                        <option value="S1">S1</option>
                                        <option value="S2">S2</option>
                                        <option value="S3">S3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="religion" class="col-sm-3 control-label">Agama</label>
                                <div class="col-sm-9">
                                    <select id="religion" name="religion" data-init-plugin="select2">
                                        <option value="Islam" selected>Islam</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                    </select>
                                </div>
                            </div>
                              <div class="form-group">
                                <label for="marital" class="col-sm-3 control-label">Status Pernikahan</label>
                                <div class="col-sm-9">
                                    <select id="martial" name="martial" data-init-plugin="select2">
                                        <option value selected="Single">Belum Menikah</option>
                                        <option value="Merried">sudah Menikah</option>
                                    </select>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="birth" class="col-sm-3 control-label">Tanggal Lahir</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control"  data-provide="datepicker" id="birth" name="birth" required> </div>
                            </div>
                             <div class="form-group">
                                <label for="link" class="col-sm-3 control-label">Link video</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="link" name="link" value="www.youtube.com" placeholder="Link Video" required> </div>
                            </div>
                            <div class="form-group">
                                <label for="short-description" class="col-sm-3 control-label">Deskripsi singkat</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="short-description" name="short-description"  placeholder="Briefly Describe your Abilities">Suka memasak, mencuci</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="briefly-description" class="col-sm-3 control-label">Deskripsi diri</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="8" id="briefly-description" name="briefly-description" placeholder="Deskripsi diri">suka memasak dan menyukai anak kecil. Hobi saya suka menggambar sehingga saya sering membuka les privat di rumah bersama saudara-saudara saya. Saya berpenampilan menarik dengan tinggi badan 160 cm dan berat badan 50 kg.</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="briefly-description" class="col-sm-3 control-label">Deskripsi diri</label>
                                <div class="col-sm-9">
                                    <input type="file" id="foto" />
                                    <img id="image" src="{{ url('images/girl.png') }}" style="width: 200px;margin-top: 25px;"  />
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-3">
                                    <p>Jika melakukan perubahan klik update. Jika tidak klik batal. </p>
                                </div>
                                <div class="col-sm-9">
                                    <button class="btn btn-success" type="submit">Update</button>
                                    <a class="btn btn-default" href="{{ url('/dashboard/kelola-babysitter/') }}"><i class="pg-close"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PANEL -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>

<script>
    document.getElementById("foto").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };
</script>

@stop