@extends('layouts.dashboard')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content ">
    
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li> <a href="#">Dashboard</a> </li>
            <li><a href="#" class="active">Transaksi</a> </li>
        </ul>
        <!-- END BREADCRUMB -->
        <!-- START PANEL -->
        <div class="panel panel-transparent">
            <div class="panel-heading">
            <div class="panel-title">List Transaksi
            </div>
            <div class="pull-right">
                <div class="col-xs-12">
                <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                </div>
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="panel-body">
            <table class="table table-hover demo-table-search" id="tableWithSearch">
                <thead>
                <tr>
                    <th>Photo</th>
                    <th>Id Pemesanan</th>
                    <th>Nama Orang Tua</th>
                    <th>Nomor Rekening</th>
                    <th>Tanggal Transfer</th>
                    <th>Jumlah Transfer</th>
                    <th>Status Konfirmasi</th>
                    <th>Detail</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="v-align-middle semi-bold">
                        <img src="{{ url('images/struk-atm.jpg') }}" alt="Bukti Transfer" width="100">
                    </td>
                    <td class="v-align-middle semi-bold">
                        <p>NKF001</p>
                    </td>
                    <td class="v-align-middle"> 
                        <p>Zahra Zuluthfa</p>
                    </td>
                    <td class="v-align-middle">
                        <p>3083-01-000-982-50-9</p>
                    </td>
                    <td class="v-align-middle">
                        <p>2 Des 2016 ; 10:00</p>
                    </td>
                    <td class="v-align-middle">
                        <p>6000000</p>
                    </td>
                    <td class="v-align-middle">
                        <span class="label label-default">Menunggu konfirmasi</span>
                    </td>
                    <td class="v-align-middle">
                        <a class="btn" href="{{ url('/dashboard/kelola-transaksi/detail') }}">Detail</a>
                    </td>
                </tr>
                <tr>
                    <td class="v-align-middle semi-bold">
                        <img src="{{ url('images/struk-atm.jpg') }}" alt="Bukti Transfer" width="100">
                    </td>
                    <td class="v-align-middle semi-bold">
                        <p>NKF001</p>
                    </td>
                    <td class="v-align-middle"> 
                        <p>Zahra Zuluthfa</p>
                    </td>
                    <td class="v-align-middle">
                        <p>3083-01-000-982-50-9</p>
                    </td>
                    <td class="v-align-middle">
                        <p>2 Des 2016 ; 10:00</p>
                    </td>
                    <td class="v-align-middle">
                        <p>6000000</p>
                    </td>
                    <td class="v-align-middle">
                        <span class="label label-success">Terima</span>
                    </td>
                    <td class="v-align-middle">
                        <a class="btn" href="{{ url('/dashboard/kelola-transaksi/detail') }}">Detail</a>
                    </td>
                </tr>
                <tr>
                    <td class="v-align-middle semi-bold">
                        <img src="{{ url('images/struk-atm.jpg') }}" alt="Bukti Transfer" width="100">
                    </td>
                    <td class="v-align-middle semi-bold">
                        <p>NKF001</p>
                    </td>
                    <td class="v-align-middle"> 
                        <p>Zahra Zuluthfa</p>
                    </td>
                    <td class="v-align-middle">
                        <p>3083-01-000-982-50-9</p>
                    </td>
                    <td class="v-align-middle">
                        <p>2 Des 2016 ; 10:00</p>
                    </td>
                    <td class="v-align-middle">
                        <p>6000000</p>
                    </td>
                    <td class="v-align-middle">
                        <span class="label label-danger">Ditolak</span>
                    </td>
                    <td class="v-align-middle">
                        <a class="btn" href="{{ url('/dashboard/kelola-transaksi/detail') }}">Detail</a>
                    </td>
                </tr>
                </tbody>
            </table>
            </div>
        </div>
        <!-- END PANEL -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>

@stop