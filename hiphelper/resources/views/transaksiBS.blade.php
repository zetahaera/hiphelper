@extends('master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content black-text">
						<div class="card-title black-text">
							List Transaksi
						</div>
						<div id="row" style="padding-left: 14px; padding-right: 14px;">
					       	<div class="col s2 hide-on-med-and-down"><b>Id Pemesanan</b></div>
					    	<div class="col s4 hide-on-med-and-down"><b>Waktu</b></div>
					       	<div class="col s2 hide-on-med-and-down"><b>Lokasi</b></div>
					       	<div class="col s4 hide-on-med-and-down"><span class="wrap-text"><b>Status Pemesanan</b></span></div> 
					       	<div class="col s5 hide-on-large-only"><b>Id Pemesanan</b></div>
					       	<div class="col s7 hide-on-large-only"><b>Status Pemesanan</b></div>      
					 	</div>
					 	<br>
					 	<br>
						<ul class="collapsible" data-collapsible="accordion">
							<li>
									<div class="collapsible-header red lighten-4" style="display: block;overflow: auto;" >
										<div class="col s2 hide-on-med-and-down"><span class="wrap-text">NFK0001</span></div>
			        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Jumat, 18 Nov 2016 ; 06:00 - 18:00</span></div>
			        					<div class="col s2 hide-on-med-and-down"><span class="wrap-text">Depok</span></div>
			        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Menunggu Konfirmasi</span></div>
			        					<div class="col s5 hide-on-large-only"><span class="wrap-text">NFK0001</span></div>
			        					<div class="col s7 hide-on-large-only"><span class="wrap-text">Menunggu Konfirmasi</span></div>		
									</div>
									<div class="collapsible-body">
										
										<div class="row">
											<div class="col s12">
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Booking
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Nama Parent :</b>
																<br>
																<span class="wrap-text">
																	Zahra Zuluthfa
																</span>
															</div>
															<div class="col s5">
																<b>Jumlah Anak :</b>
																<br>
																<span class="wrap-text">
																	2 Anak
																</span>
															</div>
														</div>
														<div class="row black-text">
															<div class="col s10 ">
																<b>Waktu :</b>
																<br>
																<span class="wrap-text">
																	Jumat, 18 Nov 2016 ; 06:00 - 18:00
																</span>
															</div>


														</div>
														<div class="row black-text">
															<div class="col s5">
																<b>Lokasi : </b>
																<br>
																<span class="wrap-text">
																	Depok
																</span>
															</div>
															<div class="col s5">
																<b>Tarif :</b>
																<br>
																<span class="wrap-text">
																	Rp. 250.000
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>		
										<div class="row">
											<div class="col s12">
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Anak
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Jenis Kelamin :</b>
																<br>
																<span class="wrap-text">
																	Perempuan
																</span>
															</div>
															<div class="col s5">
																<b>Umur :</b>
																<br>
																<span class="wrap-text">
																	3 tahun
																</span>
															</div>
														</div>
														<div class="row black-text">
															<div class="col s10">
																<b>Catatan :</b>
																<br>
																<span class="wrap-text">
																	Alergi udang, obt terkahir yang dikomsunsi antibiotik
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
										<div class="row hide-on-med-and-down">
											<div class=" col s6">
												<a class=" modal-trigger waves-effect waves-light btn green" href="#konfirmasiTerima" style="width: 100%;"><i class="material-icons right">done</i>Terima Booking</a>
												
											</div>
											<div class="col s6">
												<a class="modal-trigger waves-effect waves-light btn red" href="#konfirmasiTolak" style="width: 100%;"><i class="material-icons right" >clear</i>Tolak Pesanan</a>
											</div>
										</div>
										<div class="row hide-on-large-only">
											<div class=" col s6">
												<a class="waves-effect waves-light btn green" href="#konfirmasiTerima" style="width: 100%;">Terima</a>
												
											</div>
											<div class="col s6">
												<a class="waves-effect waves-light btn red" href="#konfirmasiTolak" style="width: 100%;">Tolak</a>
											</div>
										</div>

									
									</div>
							</li>
							<li>
											
											<div class="collapsible-header yellow lighten-2" style="display: block; overflow: auto;">
												<div class="col s2 hide-on-med-and-down"><span class="wrap-text"> NFK0001</span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text"> Jumat, 18 Nov 2016 ; 07:00 - 17:00</span></div>
					        					<div class="col s2 hide-on-med-and-down"><span class="wrap-text"> Depok </span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text"> Menunggu Pembayaran</span></div>
					        					<div class="col s5 hide-on-large-only"><span class="wrap-text"> NFK0001</span></div>
					        					<div class="col s7 hide-on-large-only"><span class="wrap-text">Menunggu Pembayaran</span></div>
											</div>
											<div class="collapsible-body">
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Waktu :</b>
																		<br>
																		<span class="wrap-text">
																			Jumat, 18 Nov 2016 ; 07:00 - 17:00
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Lokasi :</b>
																		<br>
																		<span class="wrap-text">
																			Depok
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Tarif :</b>
																		<br>
																		<span class="wrap-text">
																			Tarif : 250.000
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>		
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- <div class="row">
													<div class=" col s7 push-s5">
														<a class="waves-effect waves-light btn green" href="#konfirmasiTerima"><i class="material-icons right">done</i>Terima Booking</a>
														<a class="waves-effect waves-light btn red" href="#konfirmasiTolak"><i class="material-icons right">clear</i>Tolak Pesanan</a>
													</div>
												</div> -->	
												<!-- <div id="konfirmasiTerima" class="modal modal-fixed-footer">
													<div class="modal-content">
														<h5>Apakah Anda Yakin Menerima Pesanan?</h5>
														<div class="row">
															<div class="col s10">
																<b>Id Pemesanan : </b>
																<br>
																<span class="wrap-text">
																	NFK0001
																</span>
															</div>
														</div>
														<div class="row">
															<div class="col s5">
																<b>Waktu Pemesanan :</b>
																<br>
																<span class="wrap-text">
																	Jumat, 18 Nov 2016 ; 07:00 - 17:00
																</span>
															</div>
															<div class="col s5">
																<b>Lokasi :</b>
																<br>
																<span class="wrap-text">
																	Depok
																</span>
															</div>
															
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<div class="col s7-push-s5">
															<a class="waves-effect waves-teal btn-flat"><i class="material icons-right">cancel</i>Cancel</a>
															<a class="waves-effect waves-light btn green" href="#"><i class="material-icons right">done</i>Terima Booking</a>
														</div>
													</div>
												</div> -->
											</div>
							</li>
							<li>
											<div class="collapsible-header  green accent-2" style="display: block; overflow: auto;">
												<div class="col s2 hide-on-med-and-down"><span class="wrap-text">NFK0002</span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Jumat, 2 Des 2016 ; 07:00 - 17:00</span></div>
					        					<div class="col s2 hide-on-med-and-down"><span class="wrap-text"> Kelapa Dua</span></div>
					        					<div class="col s4 hide-on-med-and-down"><span class="wrap-text">Menunggu Waktu Babysitting<span></div>		
					        					<div class="col s5 hide-on-large-only"><span class="wrap-text">NFK0002</span></div>
					        					<div class="col s7 hide-on-large-only"><span class="wrap-text">Menunggu Waktu Babysitting</span></div>
											</div>
											<div class="collapsible-body">
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Alamat :</b>
																		<br>
																		<span class="wrap-text">
																			Jl. Kedondong No. 1 Perumahan Depok Indah, Depok
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b> Waktu :</b>
																		<br>
																		<span class="wrap-text">
																			Jumat, 2 Des 2016 ; 07:00 - 17:00
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nomor HP Parent :</b>
																		<br>
																		<span class="wrap-text">
																			08782346823
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Tarif :</b>
																		<br>
																		<span class="wrap-text">
																			Rp. 250.000
																		</span>
																	</div>
																</div>
															
															</div>
														</div>
													</div>
												</div>		
												<div class="row">
													<div class="col s12">
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col s12">
														<ul class="collection with-header">
															<li class="collection-header"><h4>SoP for Babysitter</h4></li>
															<li class="collection-item">
																Menjalin komunikasi yang lancar dan nyaman dengan orang tua, termasuk berupa menggunakan etika dan bahasa yang sopan, jujur, dan terbuka.</li>
															</li>
															<li class="collection-item">
																Menggunakan kemeja/kaos berlengan dengan outer dan celana atau rok bahan ketika bekerja. Dilarang menggunakan hanya kaos oblong dan celana jeans.
															</li>
															<li class="collection-item">
																Memberikan form parental consent pada orang tua saat bertemu sebelum menjalankan tugas. Pastikan isi profil dan semua kebutuhan anak benar dan lengkap tertulis.
															</li>
															<li class="collection-item"> 
																Wajib menyiapkan SAK (Satuan Aktivitas Kegiatan) minimal 1 kegiatan dan jadwal harian untuk anak dan diperlihatkan kepada orang tua saat bertemu di awal.
															</li>
															<li class="collection-item">
																Wajib memberi kabar kepada orang tua minimal 2 jam sekali berupa telpon, video call, pengiriman foto anak melalui chat, dll. sesuai kesepakatan dengan orang tua.
															</li>
															<li class="collection-item">
																Tidak mengajak anak pergi ke lokasi lain, memberi asupan, dll tanpa seizin orang tua telah melakukan kontak terlebih dahulu untuk meminta izin/dalam kasus darurat.
															</li>
															<li class="collection-item">
																Setelah bertugas, Babysitter wajib memberikan laporan harian berisi nama anak, usia anak, kegiatan yang dilakukan anak hari itu beserta jamnya, makanan & minuman yang dikonsumsi anak hari itu, serta pencapaian & hambatan yang terjadi pada anak hari itu.
															</li>
															
														</ul> 
													</div>
													
												</div>
												<!-- <div class="row">
													<div class=" col s7 push-s5">
														<a class="waves-effect waves-light btn green" href="#konfirmasiTerima"><i class="material-icons right">done</i>Terima Booking</a>
														<a class="waves-effect waves-light btn red" href="#konfirmasiTolak"><i class="material-icons right">clear</i>Tolak Pesanan</a>
													</div>
												</div>	 -->
												<!-- <div id="konfirmasiTerima" class="modal modal-fixed-footer">
													<div class="modal-content">
														<h5>Apakah Anda Yakin Menerima Pesanan?</h5>
														<div class="row">
															<div class="col s10">
																<b>Id Pemesanan : </b>
																<br>
																<span class="wrap-text">
																	NFK0001
																</span>
															</div>
														</div>
														<div class="row">
															<div class="col s5">
																<b>Waktu Pemesanan :</b>
																<br>
																<span class="wrap-text">
																	Jumat, 18 Nov 2016 ; 07:00 - 17:00
																</span>
															</div>
															<div class="col s5">
																<b>Lokasi :</b>
																<br>
																<span class="wrap-text">
																	Depok
																</span>
															</div>
															
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Booking
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Nama Parent :</b>
																		<br>
																		<span class="wrap-text">
																			Zahra Zuluthfa
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Jumlah Anak :</b>
																		<br>
																		<span class="wrap-text">
																			2 Anak
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="card">
															<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<div class="col s7-push-s5">
															<a class="waves-effect waves-teal btn-flat"><i class="material icons-right">cancel</i>Cancel</a>
															<a class="waves-effect waves-light btn green" href="#"><i class="material-icons right">done</i>Terima Booking</a>
														</div>
													</div>
												</div> -->
											</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="konfirmasiTerima" class="modal modal-fixed-footer">
											<div class="modal-content">
												<h5>Apakah Anda Yakin Menerima Pesanan?</h5>
												<div class="row">
													<div class="col s10">
														<b>Id Pemesanan : </b>
														<br>
														<span class="wrap-text">
															NFK0001
														</span>
													</div>
												</div>
												<div class="row">
													<div class="col s5">
														<b>Waktu Pemesanan :</b>
														<br>
														<span class="wrap-text">
															Jumat, 18 Nov 2016 ; 07:00 - 17:00
														</span>
													</div>
													<div class="col s5">
														<b>Lokasi :</b>
														<br>
														<span class="wrap-text">
															Depok
														</span>
													</div>
													
												</div>
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Booking
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Nama Parent :</b>
																<br>
																<span class="wrap-text">
																	Zahra Zuluthfa
																</span>
															</div>
															<div class="col s5">
																<b>Jumlah Anak :</b>
																<br>
																<span class="wrap-text">
																	2 Anak
																</span>
															</div>
														</div>
													</div>
												</div>
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Anak
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Jenis Kelamin :</b>
																<br>
																<span class="wrap-text">
																	Perempuan
																</span>
															</div>
															<div class="col s5">
																<b>Umur :</b>
																<br>
																<span class="wrap-text">
																	3 tahun
																</span>
															</div>
														</div>
														<div class="row black-text">
															<div class="col s10">
																<b>Catatan :</b>
																<br>
																<span class="wrap-text">
																	Alergi udang, obt terkahir yang dikomsunsi antibiotik
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<div class="col s7-push-s5">
													<a class="waves-effect waves-teal btn-flat"><i class="material-icons right">close</i>Cancel</a>
													<a class="waves-effect waves-light btn green" href="#"><i class="material-icons right">done</i>Terima Booking</a>
												</div>
											</div>
					</div>
					<div id="konfirmasiTolak" class="modal modal-fixed-footer">
						<div class="modal-content"> 
							<h5>Apakah Anda Yakin Menolak Pesanan?</h5>
												<div class="row">
													<div class="col s10">
														<b>Id Pemesanan : </b>
														<br>
														<span class="wrap-text">
															NFK0001
														</span>
													</div>
												</div>
												<div class="row">
													<div class="col s5">
														<b>Waktu Pemesanan :</b>
														<br>
														<span class="wrap-text">
															Jumat, 18 Nov 2016 ; 07:00 - 17:00
														</span>
													</div>
													<div class="col s5">
														<b>Lokasi :</b>
														<br>
														<span class="wrap-text">
															Depok
														</span>
													</div>
													
												</div>
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Booking
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Nama Parent :</b>
																<br>
																<span class="wrap-text">
																	Zahra Zuluthfa
																</span>
															</div>
															<div class="col s5">
																<b>Jumlah Anak :</b>
																<br>
																<span class="wrap-text">
																	2 Anak
																</span>
															</div>
														</div>
													</div>
												</div>
												<div class="card">
													<div class="card-content">
														<span class="card-title black-text" style="color: black;">
															Detil Anak
														</span>
														<div class="row black-text">
															<div class="col s5">
																<b>Jenis Kelamin :</b>
																<br>
																<span class="wrap-text">
																	Perempuan
																</span>
															</div>
															<div class="col s5">
																<b>Umur :</b>
																<br>
																<span class="wrap-text">
																	3 tahun
																</span>
															</div>
														</div>
														<div class="row black-text">
															<div class="col s10">
																<b>Catatan :</b>
																<br>
																<span class="wrap-text">
																	Alergi udang, obt terkahir yang dikomsunsi antibiotik
																</span>
															</div>
														</div>
													</div>
												</div>
						</div>
						<div class="modal-footer">
												<div class="col s7-push-s5">
													<a class="waves-effect waves-teal btn-flat"><i class="material-icons right">close</i>Cancel</a>
													<a class="waves-effect waves-light btn red" href="#"><i class="material-icons right">close</i>Tolak Booking</a>
												</div>
						</div>
					</div>
	</div>
@stop