@extends('master')

@section('content')
	<div class="container">
		<div class="card">
			<div class="card-content black-text">
				<div class="card-title black-text">
					List Transaksi
					<hr>
				</div>
				<ul class="collapsible" data-collapsible="accordion">
					<li>
						<div class="collapsible-header maroon" style="max-height: 50px;"><span class="new badge" style="border-right-width: 10px; margin-right: 25px; margin-top: 2px;">2</span>
								<div class="row">
									<!-- <div class="col s1">
										<img src="{{ url('images/confirm-schedule.png') }}" style="height: 32px;width:32px; margin-top: 7px;margin-bottom: 7px;">
									</div> -->
									<div class="col s10">
										Menunggu Konfirmasi Babysitter
									</div>
								</div>
						</div>
						<div class="collapsible-body grey lighten-5">
								<div class="card">
									<div class="card-content black-text">
										<div class="row" style="margin-bottom: 0px;">
											<div class="col  l2 m12 s12">
												<img class="avatar" src="{{ url('images/babysitter.jpg') }}">
											</div>
											<div class="col  s12 m10 l10">
												<div class="card-title black-text">
													<span class="wrap-text">
														<a href="{{url('/detil/babysitter')}}">Dyah Nabila</a>	
													</span>
													
													
												</div>
													<div class="row">
														<div class="col m12 s12 l5">
															<b>Id Pemesanan :</b>
															<br>
															<span class="wrap-text">
																DN00001
															</span>
														</div>
														<div class="col s12 m12 l5">
															<b>Waktu :</b>
															<br>
															<span class="wrap-text">
																Jumat, 18 Nov 2016 ; 06:00 - 18:00
															</span>
														</div>
													
													</div>
													<div class="row">
														<div class="col s12 m12 l5">
															<b>Alamat : </b>
															<br>
															<span class="wrap-text">
																Jl. Kedondong No. 1 Perumahan Depok Indah, Depok
															</span>
														</div>
														<div class="col s12 m12 l5">
															<b>Tarif : </b>
															<br>
															<span class="wrap-text">
																Rp. 250.000,-
															</span>

															
														</div>
														<div class="col s12">
															<ul class="collapsible" data-collapsible="accordion">
															<li>
																<div class="collapsible-header active">
																	Detil Pemesanan<i class="material-icons">keyboard_arrow_down</i>
																</div>
																<div class="collapsible-body">
																	<div class="card">
																		<div class="card-content">
																<span class="card-title black-text" style="color: black;">
																	Detil Anak
																</span>
																<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																</div>
																<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</li>
														</ul>
														</div>
												</div>
													
											</div>
										</div>	
										
									</div>
									
								</div>
						</div>
					</li>
					<li>
						<div class="collapsible-header maroon" style="max-height: 50px;"><span class="new badge" style="border-right-width: 10px; margin-right: 25px; margin-top: 2px;">2</span>
								<div class="row">
									<!-- <div class="col s1">
										<img src="{{ url('images/credit-card.png') }}" style="height: 32px;width:32px; margin-top: 7px;margin-bottom: 7px;">
									</div> -->
									<div class="col s10">
										Menunggu Pembayaran
									</div>
								</div>
						</div>
						<div class="collapsible-body grey lighten-5">
								<div class="card">
									<div class="card-content black-text">
										<div class="row" style="margin-bottom: 0px;">
											<div class="col l2 m12 s12">
												<img class="avatar" src="{{ url('images/babysitter.jpg') }}">
											</div>
											<div class="col s12 m12 l10">
												<div class="card-title black-text">
													<span class="wrap-text">
														<a href="{{url('/detil/babysitter')}}">
															@section('babysitter')
																Dyah Nabila
															@show
														</a>
													</span>
														
													
												</div>
													<div class="row">
														<div class="col s12 m12 l5">
															<b>Id Pemesanan :</b>
															<br>
															<span class="wrap-text">
																DN00001
															</span>
														</div>
														<div class="col s12 m12 l5">
															<b>Waktu :</b>
															<br>
															<span class="wrap-text">
																Jumat, 18 Nov 2016 ; 06:00 - 18:00
															</span>
														</div>
													
													</div>
													<div class="row">
														<div class="col s12 m12 l5">
															<b>Alamat : </b>
															<br>
															<span class="wrap-text">
																Jl. Kedondong No. 1 Perumahan Depok Indah, Depok
															</span>
														</div>
														<div class="col s12 m12 l5">
															<b>Tarif : </b>
															<br>
															<span class="wrap-text">
															
																@section('totalbiaya')
																	Rp. 250.000
																@show
															
															</span>

															
														</div>
														<div class="col s12">
															<ul class="collapsible" data-collapsible="accordion">
																<li>
																<div class="collapsible-header">
																	Detil Pemesanan<i class="material-icons">keyboard_arrow_down</i>
																</div>
																<div class="collapsible-body">
																	<div class="card">
																		<div class="card-content">
																			<span class="card-title black-text" style="color: black;">
																				Detil Anak
																			</span>
																			<div class="row black-text">
																	<div class="col s5">
																		<b>Jenis Kelamin :</b>
																		<br>
																		<span class="wrap-text">
																			Perempuan
																		</span>
																	</div>
																	<div class="col s5">
																		<b>Umur :</b>
																		<br>
																		<span class="wrap-text">
																			3 tahun
																		</span>
																	</div>
																			</div>
																			<div class="row black-text">
																	<div class="col s10">
																		<b>Catatan :</b>
																		<br>
																		<span class="wrap-text">
																			Alergi udang, obt terkahir yang dikomsunsi antibiotik
																		</span>
																				</div>
																			</div>
																		</div>
																	</div>
																			</div>
																</li>
																<li>
																	<div class="collapsible-header active">
																		
																			Detil Pembayaran<i class="material-icons">keyboard_arrow_down</i>
																		
																	</div>
																	<div class="collapsible-body">
																		<div class="card">
																<div class="card-content black-text">
																	<div class="card-title black-text">
																		Prosedur Pembayaran
																	</div>
																	<p class="maroon white-text"
																	style="text-align:center;padding-bottom: 0px;">
																		Parent mohon melakukan pembayaran sebesar:
																		<br>
																		<div class="hide-on-med-and-down">
																			<h2 class="maroon white-text"style="text-align:center; margin-top: 0px;padding-bottom: 30px; margin-bottom: 0px;">@yield('totalbiaya')</h2>
																		</div>
																		<div class="maroon hide-on-large-only">
																			<h3 class="maroon white-text" style="text-align:center; margin-top: 0px;padding-bottom: 30px; margin-bottom: 0px;">
																				@yield('totalbiaya')
																			</h3>
																		</div>
																	</p>
																	<br>
																	<h6 style="text-align: center; margin: 0px;">
																			dengan melakukan transfer ke :
																	</h6>
																	<h5 style="text-align:center; margin-top: 0px;padding-bottom: 0px;margin-bottom: 0px;">
																			Rekening BNI
																	</h5>
																	<h3 style="text-align:center; margin-top: 0px;padding-bottom: 0px; margin-bottom: 0px;">
																		000 000 0000
																	</h3>
																		
																	<h5 style="text-align:center; margin-top: 0px;padding-bottom: 30px;">
																			a.n. Jihaniar Mahiranisa
																	</h5>

																	<p style="text-align: center;">
																		Setelah melakukan transfer, harap lakukan konfirmasi pembayaran minimal H-1 sebelum waktu babysitting.
																		<br>
																		<a class=" waves-effect waves-light btn modal-trigger" href="#konfirm-bayar">Konfirmasi Pembayaran</a>
																		<br>
																		<br>
																		Setelah pembayaran sudah terkonfirmasi, sistem akan otomatis memunculkan kontak babysitter yang telah Anda booking.
																	</p>

																	<div id="konfirm-bayar" class="modal">
																		<div class="modal-content">
																			<h4>Konfirmasi Pembayaran</h4>
																			<div class="row">
																				<div class="input-field col s12">
																					<input id="NamaAkun" type="text" class="validate">
          																			<label for="NamaAkun">Nama Akun</label>
																				</div>
																				<div class="input-field col s12">
																					<input id="NoRek" type="text" class="validate">
          																			<label for="NoRek">Nomor Rekening</label>
																				</div>
																				<div class="input-field col s12">
																					<input id="NamaBank" type="text" class="validate">
          																			<label for="NamaBank">Nama Bank</label>
																				</div>
																				<div class="input-field col s12">
																					<input id="Cabang" type="text" class="validate">
          																			<label for="Cabang">Cabang</label>
																				</div>
																				<div class="input-field col s6">
																					<input id="JamTrans" type="text" class="validate">
																					<label for="JamTrans">Jam Transfer</label>
																				</div>
																				<div class="input-field col s6">
																					<input id="HariTrans" type="date" class="datepicker">
																					<!-- <label for="HariTrans">Hari Tranfer</label> -->
																				</div>
																				<div class="input-field col s12">
																					<input id="JumlahTrans" type="text">
																						
																					<label for="JumlahTrans">Jumlah Transfer </label>
																				</div>

																			</div>
																		</div>
																		<div class="modal-footer">
																			<a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Simpan</a>
																		</div>
																	</div>
																	
																</div>
																
															</div>
																	</div>
																</li>
														</ul>
														</div>
														
												</div>
													
											</div>
										</div>	
										
									</div>
									
								</div>
						</div>
					</li>
					<li>
						<div class="collapsible-header maroon" style="max-height: 50px;"><span class="new badge" style="border-right-width: 10px; margin-right: 25px; margin-top: 2px;">2</span>
								<div class="row">
									<!-- <div class="col s1">
										<img src="{{ url('images/credit-card.png') }}" style="height: 32px;width:32px; margin-top: 7px;margin-bottom: 7px;">
									</div> -->
									<div class="col s10">
										On Progress
									</div>
								</div>
						</div>
						<div class="collapsible-body grey lighten-5">
								<div class="card">
									<div class="card-content black-text">
										<div class="row" style="margin-bottom: 0px;">
											<div class="col s12 m12 l2">
												<img class="avatar" src="{{ url('images/babysitter.jpg') }}">
											</div>
											<div class="col s12 s12 l10">
												<div class="card-title black-text">
													<a href="{{url('/detil/babysitter')}}">
														@section('babysitter')
															Dyah Nabila
														@show
													</a>
													
												</div>
													<div class="row">
														<div class="col s12 m12 l5 maroon white-text">
															<b>Nomor Handphone :</b>
															
															<span class="wrap-text">
																08123456789
															</span>
														</div>
													
													</div>
													<div class="row">
														<div class="col s12 m12 l5">
															<b>Id Pemesanan :</b>
															<br>
															<span class="wrap-text">
																DN00001
															</span>
														</div>
														<div class="col s12 m12 l5">
															<b>Waktu :</b>
															<br>
															<span class="wrap-text">
																Jumat, 18 Nov 2016 ; 06:00 - 18:00
															</span>
														</div>
													
													</div>
													<div class="row">
														<div class="col s12 m12 l10">
															<b>Alamat : </b>
															<br>
															<span class="wrap-text">
																Jl. Kedondong No. 1 Perumahan Depok Indah, Depok
															</span>
														</div>
														<div class="col s12 m12 l5">
															<b>Tarif : </b>
															<br>
															<span class="wrap-text">
															
																@section('totalbiaya')
																	Rp. 250.000
																@show
															
															</span>

															
														</div>
														<div class="col s12">
															<ul class="collapsible" data-collapsible="accordion">
																<li>
																	<div class="collapsible-header">
																		Detil Pemesanan<i class="material-icons">keyboard_arrow_down</i>
																	</div>
																	<div class="collapsible-body">
																		<div class="card">
																			<div class="card-content">
																				<span class="card-title black-text" style="color: black;">
																					Detil Anak
																				</span>
																				<div class="row black-text">
																					<div class="col s5">
																						<b>Jenis Kelamin :</b>
																						<br>
																						<span class="wrap-text">
																							Perempuan
																						</span>
																					</div>
																					<div class="col s5">
																						<b>Umur :</b>
																						<br>
																						<span class="wrap-text">
																							3 tahun
																						</span>
																					</div>
																				</div>
																				<div class="row black-text">
																					<div class="col s10">
																						<b>Catatan :</b>
																						<br>
																						<span class="wrap-text">
																							Alergi udang, obt terkahir yang dikomsunsi antibiotik
																						</span>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="collapsible-header">
																		Detil Pembayaran<i class="material-icons">keyboard_arrow_down</i>
																	</div>
																	<div class="collapsible-body">
																		<div class="card">

																			<div class="card-content black-text">
																				<div class="card-title black-text">
																					Terima Kasih Sudah Melakukan Pembayaran
																				</div>
																				
																				<div class="row">
																					<div class="col s12 m12 l5">
																						<b>Nama Akun :</b>
																						<br>
																						<span class="wrap-text">
																							Jayden Smith
																						</span>
																					</div>
																					<div class="col s12 m12 l5">
																						<b>Jumlah Transfer :</b>
																						<br>
																						<span class="wrap-text">
																							250.000
																						</span>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col s12 m12 l5">
																						<b>No. Rekening :</b>
																						<br>
																						<span class="wrap-text">
																							0423523432
																						</span>
																					</div>
																					<div class="col s12 m12 l5">
																						<b>Bank :</b>
																						<br>
																						<span class="wrap-text">
																							BNI, Kantor Cabang UI Depok
																						</span>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col s12 m12 l10">
																						<b>Waktu Transfer :</b>
																						<br>
																						<span class="wrap-text">
																							Kamis, 17 November 2016,08.42

																						</span>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="collapsible-header active">
																		Syarat dan Ketentuan<i class="material-icons">keyboard_arrow_down</i>
																	</div>
																	<div class="collapsible-body">
																		<div class="card">
																			<ul class="collection with-header">
																					<li class="collection-header"><h5>Syarat dan Ketentuan Pemesanan Babysitter</h5></li>
																					<li class="collection-item">	
																						Orangtua wajib menghubungi Babysitter yang bersangkutan <b> minimal H-1</b> untuk konfirmasi jam dan lokasi bertemu.
																					</li>
																					<li class="collection-item">
																						Orangtua <b>wajib</b> menyediakan semua keperluan anak termasuk makanan dan minuman selama diasuh oleh Babysitter dan <b>dilarang</b> meminta Babysitter untuk mengeluarkan biaya untuk anak di luar kesepakatan di awal kecuali saat kondisi darurat.
																					</li>
																					
																					<li class="collection-item">
																						Orang tua akan <b>menandatangi form parental consent</b> yang akan diberikan oleh Babysitter pada saat hari H. Form Parental Consent dapat dilihat di <a href="#">sini</a> 
																					</li >
																					<li class="collection-item">
																						Babysitter akan menyiapkan SAK (Satuan Aktivitas Kegiatan) minimal 1 kegiatan dan jadwal harian untuk anak dan diperlihatkan kepada orang tua saat bertemu di awal.
																					</li>
																					<li class="collection-item">
																						Babysitter akan memberi kabar kepada orang tua minimal 2 jam sekali berupa telpon, video call, pengiriman foto anak melalui chat, dll. sesuai kesepakatan dengan orang tua.
																					</li>
																					<li class="collection-item">
																						Babysitter tidak akan mengajak anak pergi ke lokasi lain, memberi asupan, dll tanpa seizin orang tua telah melakukan kontak terlebih dahulu untuk meminta izin/dalam kasus darurat.
																					</li>
																					<li class="collection-item">
																						Babysitter akan memberikan laporan harian berisi nama anak, usia anak, kegiatan yang dilakukan anak hari itu beserta jamnya, makanan dan yang dikonsumsi anak hari itu, serta pencapaian dan yang terjadi pada anak hari itu.

																					</li>
																			</ul>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														
												</div>
													
											</div>
										</div>	
										
									</div>
									
								</div>
						</div>
					</li>
					<li>
						<div class="collapsible-header maroon" style="max-height: 50px;"><span class="new badge" style="border-right-width: 10px; margin-right: 25px; margin-top: 2px;">2</span>
								<div class="row">
									<!-- <div class="col s1">
										<img src="{{ url('images/credit-card.png') }}" style="height: 32px;width:32px; margin-top: 7px;margin-bottom: 7px;">
									</div> -->
									<div class="col s10">
										Menunggu Review
									</div>
								</div>
						</div>
						<div class="collapsible-body grey lighten-5">
								<div class="card">
									<div class="card-content black-text">
										<div class="row" style="margin-bottom: 0px;">
											<div class="col s12 m12 l2">
												<img class="avatar" src="{{ url('images/babysitter.jpg') }}">
											</div>
											<div class="col s12 m12 l10">
												<div class="card-title black-text">
													<a href="{{url('/detil/babysitter')}}">Dyah Nabila</a>
													
												</div>
													<div class="row">
														<div class="col s12 m12 l5">
															<b>Id Pemesanan :</b>
															<br>
															<span class="wrap-text">
																DN00001
															</span>
														</div>
														<div class="col s12 m12 l5">
															<b>Waktu :</b>
															<br>
															<span class="wrap-text">
																Jumat, 18 Nov 2016 ; 07:00 - 17:00
															</span>
														</div>
													
													</div>
													<div class="row">
														<div class="col s12 m12 l10">
															<b>Alamat : </b>
															<br>
															<span class="wrap-text">
																Jl. Kedondong No. 1 Perumahan Depok Indah, Depok
															</span>
														</div>
														<div class="col s12">
															<ul class="collapsible" data-collapsible="accordion">
																<li>
																	<div class="collapsible-header">
																		Detil Pemesanan<i class="material-icons">keyboard_arrow_down</i>
																	</div>
																	<div class="collapsible-body">
																		<div class="card">
																			<div class="card-content">
																				<span class="card-title black-text" style="color: black;">
																					Detil Anak
																				</span>
																				<div class="row black-text">
																					<div class="col s5">
																						<b>Jenis Kelamin :</b>
																						<br>
																						<span class="wrap-text">
																							Perempuan
																						</span>
																					</div>
																					<div class="col s5">
																						<b>Umur :</b>
																						<br>
																						<span class="wrap-text">
																							3 tahun
																						</span>
																					</div>
																				</div>
																				<div class="row black-text">
																					<div class="col s10">
																						<b>Catatan :</b>
																						<br>
																						<span class="wrap-text">
																							Alergi udang, obt terkahir yang dikomsunsi antibiotik
																						</span>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="collapsible-header">
																		Detil Pembayaran<i class="material-icons">keyboard_arrow_down</i>
																	</div>
																	<div class="collapsible-body">
																		<div class="card">

																			<div class="card-content black-text">
																				<div class="card-title black-text">
																					Terima Kasih Sudah Melakukan Pembayaran
																				</div>
																				
																				<div class="row">
																					<div class="col s12 m12 l5">
																						<b>Nama Akun :</b>
																						<br>
																						<span class="wrap-text">
																							Jayden Smith
																						</span>
																					</div>
																					<div class="col s12 m12 l5">
																						<b>Jumlah Transfer :</b>
																						<br>
																						<span class="wrap-text">
																							250.000
																						</span>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col s12 m12 l5">
																						<b>No. Rekening :</b>
																						<br>
																						<span class="wrap-text">
																							0423523432
																						</span>
																					</div>
																					<div class="col s12 m12 l5">
																						<b>Bank :</b>
																						<br>
																						<span class="wrap-text">
																							BNI, Kantor Cabang UI Depok
																						</span>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col s12 m12 l10">
																						<b>Waktu Transfer :</b>
																						<br>
																						<span class="wrap-text">
																							Kamis, 17 November 2016,08.42

																						</span>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="collapsible-header">
																		Syarat dan Ketentuan<i class="material-icons">keyboard_arrow_down</i>
																	</div>
																	<div class="collapsible-body">
																		<div class="card">
																			<ul class="collection with-header">
																					<li class="collection-header"><h5>Syarat dan Ketentuan Pemesanan Babysitter</h5></li>
																					<li class="collection-item">	
																						Orangtua wajib menghubungi Babysitter yang bersangkutan <b> minimal H-1</b> untuk konfirmasi jam dan lokasi bertemu.
																					</li>
																					<li class="collection-item">
																						Orangtua <b>wajib</b> menyediakan semua keperluan anak termasuk makanan dan minuman selama diasuh oleh Babysitter dan <b>dilarang</b> meminta Babysitter untuk mengeluarkan biaya untuk anak di luar kesepakatan di awal kecuali saat kondisi darurat.
																					</li>
																					
																					<li class="collection-item">
																						Orang tua akan <b>menandatangi form parental consent</b> yang akan diberikan oleh Babysitter pada saat hari H. Form Parental Consent dapat dilihat di <a href="#">sini</a> 
																					</li >
																					<li class="collection-item">
																						Babysitter akan menyiapkan SAK (Satuan Aktivitas Kegiatan) minimal 1 kegiatan dan jadwal harian untuk anak dan diperlihatkan kepada orang tua saat bertemu di awal.
																					</li>
																					<li class="collection-item">
																						Babysitter akan memberi kabar kepada orang tua minimal 2 jam sekali berupa telpon, video call, pengiriman foto anak melalui chat, dll. sesuai kesepakatan dengan orang tua.
																					</li>
																					<li class="collection-item">
																						Babysitter tidak akan mengajak anak pergi ke lokasi lain, memberi asupan, dll tanpa seizin orang tua telah melakukan kontak terlebih dahulu untuk meminta izin/dalam kasus darurat.
																					</li>
																					<li class="collection-item">
																						Babysitter akan memberikan laporan harian berisi nama anak, usia anak, kegiatan yang dilakukan anak hari itu beserta jamnya, makanan dan yang dikonsumsi anak hari itu, serta pencapaian dan yang terjadi pada anak hari itu.

																					</li>
																			</ul>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="collapsible-header active">
																		Review<i class="material-icons">keyboard_arrow_down</i>
																	</div>	
																	<div class="collapsible-body">
																		

																		<div class="row">
																			<div class="col s12" style="text-align: center;">
																				<h5>Terima Kasih Telah Mempercayakan Buah Hati Anda kepada HipHelper :)</h5>
																			</div>
																			<form class="col s12">
																				<div class="row">
																					<div class="input-field col s12 m12 l6">
																						<select>
																							<option value="" disabled selected>Rating </option>
																							<option value="1">1</option>
																							<option value="2">2</option>
																							<option value="3">3</option>
																							<option value="4">4</option>
																							<option value="5">5</option>
																						</select>
																					</div>
																					<div class="input-field col s12">
																						<textarea id="review" class="materialize-textarea" length="120"></textarea>
																						<label for="review">Tulis review anda terhadap @yield('babysitter')</label>
																					</div>

																					
																				</div>
																				<div class="row">
																					<div class="col s5 push-s4">
																						<a class="waves-effect waves-light btn " style="width:100%;">Kirim</a>
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														
													</div>
													
											</div>
										</div>	
										
									</div>
									
								</div>
						</div>
					</li>
					<li>
						<div class="collapsible-header maroon" style="max-height: 50px;"><span class="new badge" style="border-right-width: 10px; margin-right: 25px; margin-top: 2px;">2</span>
								<div class="row">
									<!-- <div class="col s1">
										<img src="{{ url('images/credit-card.png') }}" style="height: 32px;width:32px; margin-top: 7px;margin-bottom: 7px;">
									</div> -->
									<div class="col s10">
										Transaksi Selesai
									</div>
								</div>
						</div>
						<div class="collapsible-body grey lighten-5">
								<div class="card">
									<div class="card-content black-text">
										<div class="row" style="margin-bottom: 0px;">
											<div class="col s12 m12 l2">
												<img class="avatar" src="{{ url('images/babysitter.jpg') }}">
											</div>
											<div class="col s12 m12 l10">
												<div class="card-title black-text">
													<a href="{{url('/detil/babysitter')}}">Dyah Nabila</a>
													
												</div>
													<div class="row">
														<div class="col s12 m12 l5">
															<b>Id Pemesanan :</b>
															<br>
															<span class="wrap-text">
																DN00001
															</span>
														</div>
														<div class="col s12 m12 l5">
															<b>Waktu :</b>
															<br>
															<span class="wrap-text">
																Jumat, 18 Nov 2016 ; 07:00 - 17:00
															</span>
														</div>
													
													</div>
													<div class="row">
														<div class="col s12">
															<b>Alamat : </b>
															<br>
															<span class="wrap-text">
																Jl. Kedondong No. 1 Perumahan Depok Indah, Depok
															</span>
														</div>
														<div class="col s12">
															<ul class="collapsible" data-collapsible="accordion">
																<li>
																	<div class="collapsible-header">
																		Detil Pemesanan<i class="material-icons">keyboard_arrow_down</i>
																	</div>
																	<div class="collapsible-body">
																		<div class="card">
																			<div class="card-content">
																				<span class="card-title black-text" style="color: black;">
																					Detil Anak
																				</span>
																				<div class="row black-text">
																					<div class="col s5">
																						<b>Jenis Kelamin :</b>
																						<br>
																						<span class="wrap-text">
																							Perempuan
																						</span>
																					</div>
																					<div class="col s5">
																						<b>Umur :</b>
																						<br>
																						<span class="wrap-text">
																							3 tahun
																						</span>
																					</div>
																				</div>
																				<div class="row black-text">
																					<div class="col s10">
																						<b>Catatan :</b>
																						<br>
																						<span class="wrap-text">
																							Alergi udang, obt terkahir yang dikomsunsi antibiotik
																						</span>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="collapsible-header">
																		Detil Pembayaran<i class="material-icons">keyboard_arrow_down</i>
																	</div>
																	<div class="collapsible-body">
																		<div class="card">

																			<div class="card-content black-text">
																				<div class="card-title black-text">
																					Terima Kasih Sudah Melakukan Pembayaran
																				</div>
																				
																				<div class="row">
																					<div class="col s5">
																						<b>Nama Akun :</b>
																						<br>
																						<span class="wrap-text">
																							Jayden Smith
																						</span>
																					</div>
																					<div class="col s5">
																						<b>Jumlah Transfer :</b>
																						<br>
																						<span class="wrap-text">
																							250.000
																						</span>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col s5">
																						<b>No. Rekening :</b>
																						<br>
																						<span class="wrap-text">
																							0423523432
																						</span>
																					</div>
																					<div class="col s5">
																						<b>Bank :</b>
																						<br>
																						<span class="wrap-text">
																							BNI, Kantor Cabang UI Depok
																						</span>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col s5">
																						<b>Waktu Transfer :</b>
																						<br>
																						<span class="wrap-text">
																							Kamis, 17 November 2016,08.42

																						</span>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="collapsible-header">
																		Syarat dan Ketentuan<i class="material-icons">keyboard_arrow_down</i>
																	</div>
																	<div class="collapsible-body">
																		<div class="card">
																			<ul class="collection with-header">
																					<li class="collection-header"><h5>Syarat dan Ketentuan Pemesanan Babysitter</h5></li>
																					<li class="collection-item">	
																						Orangtua wajib menghubungi Babysitter yang bersangkutan <b> minimal H-1</b> untuk konfirmasi jam dan lokasi bertemu.
																					</li>
																					<li class="collection-item">
																						Orangtua <b>wajib</b> menyediakan semua keperluan anak termasuk makanan dan minuman selama diasuh oleh Babysitter dan <b>dilarang</b> meminta Babysitter untuk mengeluarkan biaya untuk anak di luar kesepakatan di awal kecuali saat kondisi darurat.
																					</li>
																					
																					<li class="collection-item">
																						Orang tua akan <b>menandatangi form parental consent</b> yang akan diberikan oleh Babysitter pada saat hari H. Form Parental Consent dapat dilihat di <a href="#">sini</a> 
																					</li >
																					<li class="collection-item">
																						Babysitter akan menyiapkan SAK (Satuan Aktivitas Kegiatan) minimal 1 kegiatan dan jadwal harian untuk anak dan diperlihatkan kepada orang tua saat bertemu di awal.
																					</li>
																					<li class="collection-item">
																						Babysitter akan memberi kabar kepada orang tua minimal 2 jam sekali berupa telpon, video call, pengiriman foto anak melalui chat, dll. sesuai kesepakatan dengan orang tua.
																					</li>
																					<li class="collection-item">
																						Babysitter tidak akan mengajak anak pergi ke lokasi lain, memberi asupan, dll tanpa seizin orang tua telah melakukan kontak terlebih dahulu untuk meminta izin/dalam kasus darurat.
																					</li>
																					<li class="collection-item">
																						Babysitter akan memberikan laporan harian berisi nama anak, usia anak, kegiatan yang dilakukan anak hari itu beserta jamnya, makanan dan yang dikonsumsi anak hari itu, serta pencapaian dan yang terjadi pada anak hari itu.

																					</li>
																			</ul>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="collapsible-header active">
																		Review<i class="material-icons">keyboard_arrow_down</i>
																	</div>	
																	<div class="collapsible-body">
																		

																		<div class="row">
																			<div class="col s12" style="text-align: center;">
																				<h5>Terima Kasih Telah Mempercayakan Buah Hati Anda kepada HipHelper :)</h5>
																			</div>
																			<form class="col s12">
																				<div class="row">
																					<div class="col s12">
																						<i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i>                                    <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i>                                    <i class="tiny material-icons bintang">star</i> 
																					</div>
																					<div class="col s12">
																						<blockquote>
																							Terima kasih untuk para nanny yang sudah cepat berinteraksi dengan anak dan dapat mengatasi konflik dengan baik.
																						</blockquote>
																					</div>
																					

																					
																				</div>
																				
																			</form>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														
													</div>
													
											</div>
										</div>	
										
									</div>
									
								</div>
						</div>
					</li>
				</ul>
			</div>
			
		</div>
	</div>
@stop