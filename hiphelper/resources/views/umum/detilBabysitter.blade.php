<!DOCTYPE html>
<!-- File homepage semua page -->
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>HipHelper</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="{{ url('css/materialize.min.css') }}" />
    <link rel="stylesheet" href="{{ url('css/master-css.css') }}" media="screen,projection" />
    <link rel="stylesheet" href="{{ url('lib/fullcalendar-3.0.1/fullcalendar.min.css') }}" />
    <link rel="stylesheet" href="{{ url('lib/fullcalendar-3.0.1/fullcalendar.print.css') }}" media='print' />
    <link rel="stylesheet" href="{{ url('lib/css-progress-wizard-master/css-progress-wizard-master/css/progress-wizard.min.css') }}"
    />
    <!--
    <link rel="stylesheet" href="{{ url('lib/multiDatesPicker/css/jquery-ui.css') }}"  media='print'  />
    <link rel="stylesheet" href="{{ url('lib/multiDatesPicker/css/jquery-ui.structure.css') }}"  media='print'  />
    <link rel="stylesheet" href="{{ url('lib/multiDatesPicker/css/jquery-ui.theme.css') }}"  media='print'  />
-->
    <link rel="stylesheet" href="{{ url('lib/multiDatesPicker/css/mdp.css') }}" media='print' />
    <link rel="stylesheet" href="{{ url('lib/multiDatesPicker/css/pepper-ginder-custom.css') }}" media='print' />
    <!--<link rel="stylesheet" href="{{ url('lib/multi-step-form/css/main.css') }}"  />-->
    <link rel="stylesheet" href="{{ url('lib/multi-step-form/css/jquery.steps.css') }}" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ url('favicon.ico') }}" />
    <!-- implement csrf token for AJAX calling -->
    <!-- <meta name="_token" content="{!! csrf_token() !!}"/> -->
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="{{ url('lib/fullcalendar-3.0.1/lib/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('lib/multiDatesPicker/js/jquery-ui-1.11.1.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/materialize.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/master-js.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/elevatezoom/jquery.elevateZoom-3.0.8.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('lib/fullcalendar-3.0.1/fullcalendar.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('lib/multiDatesPicker/jquery-ui.multidatespicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('lib/multi-step-form/lib/jquery.cookie-1.3.1.js') }}"></script>
    <script type="text/javascript" src="{{ url('lib/multi-step-form/build/jquery.steps.js') }}"></script>
    <style>
        .tabs .indicator {
            position: absolute;
            bottom: 0;
            height: 2px;
            background-color: #b7363b;
            will-change: left, right;
        }
    </style>
    <style>
        #calendar {
            max-width: 900px;
            margin: 0 auto;
        }
    </style>
</head>

<body style="background-color:#f9f9f9">
    <div class="navbar-fixed">
        <nav>
            <div class="nav-wrapper">
                <div class="container"> <a href="#" id="logo" class="brand-logo">HipHelper</a>
                    <ul id="menu-navbar" class="right hide-on-med-and-down">
                        <li>
                            <!-- Login call modal --><a class="modal-trigger" href="#login">Masuk</a> </li>
                        <li> <a id="btn-daftar" class="waves-effect waves-light btn" href="{{  url('/registration/parent') }}">Daftar</a>                            </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--modal-->
    <div id="login" class="modal">
        <div class="modal-content" style="color: rgb(128, 128, 128); padding:24px 24px 0px 24px;">
            <div class="center">
                <h5 style=" font-family:Segoe UI;font-weight: 500; color:grey;">Login</h5>
            </div>
            <form>
                <div class="row">
                    <div class="input-field col s12">
                        <input style="background-color: rgba(167, 159, 159, 0.17);; border-bottom: none; margin:0px;" value="" placeholder=" Email"
                            id="email" type="text" class="validate"> </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input style="background-color:rgba(167, 159, 159, 0.17); border-bottom: none; margin:0px;" value="" placeholder=" Password"
                            id="password" type="password" class="validate"> </div>
                </div>
            </form>
        </div>
        <div class="modal-footer" style="padding: 0px 34px 0px 34px;"> <a style="width:30%;" href="#!" class="waves-effect waves-light btn green modal-action modal-close">Masuk</a>
            <a
                href="{{  url('/registration/parent') }}" class="btn btn-flat  click-to-toggle" style="color:green;background-color:transparent;box-shadow: 0px 0px 0px transparent;">Lupa Kata Sandi?</a>
        </div>
    </div>
    <div class="container" style="margin-top:30px;">
        <div class="row">
            <div class="col s3">
                <form style="padding:10px; background-color:#f5f5f5;">
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Cari Babysitter" id="keyword" type="text">
                            <label for="keyword">Keyword</label>
                        </div>
                        <div class="input-field col s12">
                            <input placeholder="Cari Lokasi" type="text" id="autocomplete-input" class="autocomplete">
                            <label for="autocomplete-input">Lokasi</label>
                        </div>
                        <div class="col s12">
                            <label>Pendidikan Terakhir</label>
                            <select class="browser-default">
                            <option value="" disabled selected="">Pendidikan Terakhir</option>
                            <option value="SMP/MTS">SMP/MTS</option>
                            <option value="SMA/SMK/MA">SMA/SMK/MA</option>
                            <option value="D3">D3</option>
                            <option value="S1">S1</option>
                            <option value="S2">S2</option>
                            <option value="S3">S3</option>
                        </select>
                        </div>
                        <div class="col s12">
                            <label>Status</label>
                            <select class="browser-default">
                            <option value="" disabled selected="">Status</option>
                            <option value="semua">Semua</option>
                            <option value="lajang">Lajang</option>
                            <option value="menikah">Menikah</option>
                            <option value="janda">Janda</option>
                        </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Minimal Umur" id="min-umur" type="number" class="validate">
                            <label for="min-umur">Minimal Umur</label>
                        </div>
                        <div class="input-field col s12">
                            <input placeholder="Maksimal Umur" id="max-umur" type="number" class="validate">
                            <label for="max-umur">Maksimal Umur</label>
                        </div>
                        <div class="col s12">
                            <label>Agama</label>
                            <select class="browser-default">
                            <option value="" disabled selected="">Agama</option>
                            <option value="semua">Semua</option>
                            <option value="Islam">Islam</option>
                            <option value="Kristen">Kristen</option>
                            <option value="Katolik">Katolik</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Buddha">Buddha</option>
                            <option value="lainnya">lainnya</option>
                        </select>
                        </div>
                        <div class="col s12">
                            <label>Suku</label>
                            <select class="browser-default">
                            <option value="" disabled selected="">Suku</option>
                            <option value="semua">Sunda</option>
                            <option value="Islam">Jawa</option>
                            <option value="Kristen">Minang</option>
                            <option value="Katolik">Batak</option>
                            <option value="Hindu">Bugis</option>
                            <option value="Buddha">Lampung</option>
                            <option value="lainnya">Palembang</option>
                            <option value="lainnya">lainnya</option>
                        </select>
                        </div>
                    </div>
                    <div class="center-align"> <a class="waves-effect waves-light btn dark-maroon">Cari</a> </div>
                </form>
            </div>
            <div class="col s9" style="background-color:#fCfcfc;">
                <div style="padding:20px;">
                    <div class="row">
                        <div class="col l5 s12">
                            <img id="img_01" src="{{ url('images/babysitter.jpg') }}" data-zoom-image="{{ url('images/babysitter.jpg') }}" style="width:100%;"
                            />
                        </div>
                        <div class="col l7 s12">
                            <div class="col s12">
                                <h4 style="margin-bottom: 4px; margin-top:0px"> Dyah Nabila </h4>
                                <div class="left-align"> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i>                                    <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i>                                    <i class="tiny material-icons bintang">star</i> </div> <a href="#">6 Review</a> </div>
                            <div class="col s12 no-padding">
                                <br>
                                <div class="col s6"> <span><i class="tiny material-icons">room</i>  UNJ</span> </div>
                                <div class="col s6"> <span><i class="tiny material-icons">picture_in_picture</i>  Islam</span> </div>
                                <div class="col s6">
                                    <br> <span><i class="tiny material-icons">store</i>  Jawa </span> </div>
                                <div class="col s6">
                                    <br> <span><i class="tiny material-icons">schedule</i>  20 years old</span> </div>
                                <div class="col s6">
                                    <br> <span><i class="tiny material-icons">mode_edit</i>  SMA</span> </div>
                                <div class="col s6">
                                    <br> <span><i class="tiny material-icons">loyalty</i>  Belum Menikah</span> </div>
                                <div class="col s12">
                                    <br> <span><i class="tiny material-icons">assignment_ind</i>  Memiliki keahlian memasak, mencuci, mengasuh bayi</span>                                    </div>
                            </div>
                            <div class="col s12">
                                <br>
                                <a style="width: 100%;" class="waves-effect waves-light btn green" href="{{  url('/pemesanan/babysitter') }}">Pesan</a></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <div class="row">
                                <div class="col s12">
                                    <ul class="tabs" style="background-color: #f5f5f5;">
                                        <li class="tab col s3"><a class="active" href="#tentang">Deskripsi Diri</a></li>
                                        <li class="tab col s3"><a href="#pengalaman">Ulasan</a></li>
                                        <li class="tab col s3"><a href="#pendidikan">Jadwal</a></li>
                                    </ul>
                                </div>
                                <div id="tentang" class="col s12">
                                    <div class="col s12">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sin tantum modo ad indicia
                                            veteris memoriae cognoscenda, curiosorum. Ille incendat? Illud dico, ea, quae
                                            dicat, praeclare inter se cohaerere. Hanc quoque iucunditatem, si vis, transfer
                                            in animum; Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere?
                                            </p>
                                        <p>Duo Reges: constructio interrete. Oratio me istius philosophi non offendit; An hoc
                                            usque quaque, aliter in vita? Zenonis est, inquam, hoc Stoici. Ex quo intellegitur
                                            officium medium quiddam esse, quod neque in bonis ponatur neque in contrariis.
                                            Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus; Aeque enim
                                            contingit omnibus fidibus, ut incontentae sint. Nummus in Croesi divitiis obscuratur,
                                            pars est tamen divitiarum.</p>
                                        <p>Non enim ipsa genuit hominem, sed accepit a natura inchoatum. Non igitur de improbo,
                                            sed de callido improbo quaerimus, qualis Q. Sin tantum modo ad indicia veteris
                                            memoriae cognoscenda, curiosorum. Restatis igitur vos; Quonam modo? Quid enim
                                            est a Chrysippo praetermissum in Stoicis? Illud quaero, quid ei, qui in voluptate
                                            summum bonum ponat, consentaneum sit dicere. Dolor ergo, id est summum malum,
                                            metuetur semper, etiamsi non aderit; Estne, quaeso, inquam, sitienti in bibendo
                                            voluptas? Torquatus, is qui consul cum Cn.</p>
                                    </div>
                                    <!--<div class="col s12 center-align">
                                    <iframe width="500" height="300" src="https://www.youtube.com/embed/uvF8_fefHE0" frameborder="0" allowfullscreen></iframe>
                                    <h5> Video Tentang Saya </h5> </div> -->
                                </div>
                                <div id="pengalaman" class="col s12">
                                    <table>
                                        <colgroup width="75">
                                            <colgroup>
                                                <tr>
                                                    <td style="vertical-align: top;"> <img id="avatar-circle" src="{{ url('images/boy.png') }}" alt="" class="circle responsive-img"
                                                            style="width:50px;margin: 0 auto;vertical-align: middle;"> </td>
                                                    <td> <span>Zahra Zulutfa </span>
                                                        <div class="left-align"> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i>                                                            <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i>                                                            <i class="tiny material-icons bintang">star</i> </div> <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum. Ille incendat? Illud dico, ea, quae dicat, praeclare inter se cohaerere. Hanc quoque iucunditatem, si vis, transfer in animum; Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere? </span>                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;"> <img id="avatar-circle" src="{{ url('images/boy.png') }}" alt="" class="circle responsive-img"
                                                            style="width:50px;margin: 0 auto;vertical-align: middle;"> </td>
                                                    <td> <span>Zahra Zulutfa </span>
                                                        <div class="left-align"> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i>                                                            <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i>                                                            <i class="tiny material-icons bintang">star</i> </div> <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum. Ille incendat? Illud dico, ea, quae dicat, praeclare inter se cohaerere. Hanc quoque iucunditatem, si vis, transfer in animum; Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere? </span>                                                        </td>
                                                </tr>
                                    </table>
                                </div>
                                <div id="pendidikan" class="col s12">
                                    <br>
                                    <div class="col s12">
                                        <div id='calendar'></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--footer-->
    <footer class="page-footer" style="background-color:rgba(206, 198, 198, 0.47);">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 style="color:#6b6b6b;">Footer Content</h5>
                    <p style="color:#6b6b6b;">Hiphelper providing nannies whenever and wherever you need </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 style="color:#6b6b6b;">Links</h5>
                    <ul>
                        <li><a style="color:#6b6b6b;" href="#!">Tentang Hiphelper</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Tips Babysitter</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Hubungi Kami</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Cerita Inspiratif</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright" style="background-color:#a6263e;">
            <div class="container" style="color: white!important;"> © 2014 Copyright Text <a class="right" style="color:white;" href="#!">More Links</a> </div>
        </div>
    </footer>
    <script type="text/javascript">
        $(document).ready(function () {
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal-trigger').leanModal();
        });
    </script>
    <script>
    $(document).ready(function () {
        $('.materialboxed').materialbox();
    });
    //initiate the plugin and pass the id of the div containing gallery images
    $("#zoom_03").elevateZoom({
        gallery: 'gallery_01'
        , cursor: 'pointer'
        , galleryActiveClass: 'active'
        , imageCrossfade: true
        , loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'
    });
    //pass the images to Fancybox
    $("#zoom_03").bind("click", function (e) {
        var ez = $('#zoom_03').data('elevateZoom');
        $.fancybox(ez.getGalleryList());
        return false;
    });
</script>
    <script>
    $(document).ready(function () {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today'
                , center: 'title'
                , right: 'month,basicWeek,basicDay'
            }
            , defaultDate: '2016-09-12'
            , navLinks: true, // can click day/week names to navigate views
            editable: true
            , eventLimit: true, // allow "more" link when too many events
            events: [
                {
                    title: 'All Day Event'
                    , start: '2016-09-01'
    				}
                , {
                    title: 'Long Event'
                    , start: '2016-09-07'
                    , end: '2016-09-10'
    				}
                , {
                    id: 999
                    , title: 'Repeating Event'
                    , start: '2016-09-09T16:00:00'
    				}
                , {
                    id: 999
                    , title: 'Repeating Event'
                    , start: '2016-09-16T16:00:00'
    				}
                , {
                    title: 'Conference'
                    , start: '2016-09-11'
                    , end: '2016-09-13'
    				}
                , {
                    title: 'Meeting'
                    , start: '2016-09-12T10:30:00'
                    , end: '2016-09-12T12:30:00'
    				}
                , {
                    title: 'Lunch'
                    , start: '2016-09-12T12:00:00'
    				}
                , {
                    title: 'Meeting'
                    , start: '2016-09-12T14:30:00'
    				}
                , {
                    title: 'Happy Hour'
                    , start: '2016-09-12T17:30:00'
    				}
                , {
                    title: 'Dinner'
                    , start: '2016-09-12T20:00:00'
    				}
                , {
                    title: 'Birthday Party'
                    , start: '2016-09-13T07:00:00'
    				}
                , {
                    title: 'Click for Google'
                    , url: 'http://google.com/'
                    , start: '2016-09-28'
    				}
    			]
        });
           
    });
    
    
</script>
</body>

</html>