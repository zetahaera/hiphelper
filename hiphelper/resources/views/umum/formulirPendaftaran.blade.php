<!DOCTYPE html>
<!-- File master semua page -->
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>HipHelper</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="{{ url('css/materialize.min.css') }}" />
    <link rel="stylesheet" href="{{ url('css/master-css.css') }}" media="screen,projection" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ url('favicon.ico') }}" />
    <!-- implement csrf token for AJAX calling -->
    <!-- <meta name="_token" content="{!! csrf_token() !!}"/> -->
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/materialize.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/master-js.js') }}"></script>
</head>

<body style="background-color:rgba(167, 159, 159, 0.05);">
    <div class="container">
        <div class="row">
            <div class="col s6">
                <!--<div class="row">
                    <h6 class="left" style="margin:10px 0px 0px 10px; font-family:Segoe UI;font-weight: 30;">Hubungi kami
                        <img id="img_01" style="width: 8%;margin-left: 20px;" src="{{ url('images/facebook2.png') }}" data-zoom-image="{{ url('images/facebook2.png') }}"
                        />
                        <img id="img_01" style="width: 8%;margin-left: 10px;" src="{{ url('images/instagram.png') }}" data-zoom-image="{{ url('images/instagram.png') }}"
                        />
                        <img id="img_01" style="width: 8%;margin-left: 13px;" src="{{ url('images/email.png') }}" data-zoom-image="{{ url('images/email.png') }}"
                        />
                    </h6>
                </div>-->
                <div class="row" style="margin:250px 0px 0px 10px;">
                    <h4>
                        <a style="width: 100%; font-family:Segoe UI;font-weight: 700; color:maroon;text-shadow: 1px 1px 1px grey;" href="{{  url('/home') }}">Hiphelper</a>
                    </h4>
                    <p style="margin:20px 20px 30px 0px; font-family:Segoe UI;color:grey">Platform yang dapat membantu Anda untuk mengasuh Anak Anda</p>
                    <a style="width: 25%;" class="waves-effect waves-light btn maroon modal-trigger" href="#login">Masuk</a>
                </div>
            </div>
            
  <!-- Modal login -->
<div id="login" class="modal">
        <div class="modal-content" style="color: rgb(128, 128, 128); padding:24px 24px 0px 24px;">
             <div class="center"><h5 style=" font-family:Segoe UI;font-weight: 500; color:grey;">Login</h5>
           
                    </div>
             <form>
                        <div class="row">
                            <div class="input-field col s12">
                               <input style="background-color: rgba(167, 159, 159, 0.17);; border-bottom: none; margin:0px;" value="" placeholder=" Email"
                                id="email" type="text" class="validate">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input style="background-color:rgba(167, 159, 159, 0.17); border-bottom: none; margin:0px;" value="" placeholder=" Password"
                                id="password" type="password" class="validate">
                            </div>
                        </div>

                    </form>
        </div>
        <div class="modal-footer" style="padding: 0px 34px 0px 34px;">   
             <a style="width:30%;"href="#!" class="waves-effect waves-light btn green modal-action modal-close">Masuk</a> 
            <a href="{{  url('/registration/parent') }}" class="btn btn-flat  click-to-toggle" style="color:green;background-color:transparent;box-shadow: 0px 0px 0px transparent;">Lupa Kata Sandi?</a></div>
         
    </div>

            <!--konten-->
            <div class="col s6">
                <div class="row" style="margin:60px 0px 10px 0px;">
                    <h5 style="font-family:Segoe UI;font-weight: 700;color:grey">Pendaftaran User Baru <i class="material-icons">mode_edit</i></h5>
                    <h6 style="margin:10px 0px 10px 0px; font-family:Segoe UI;color:grey">Isi formulir di bawah untuk mendapatkan Akun Anda</h6>
                    <hr>
                </div>
                <div class="row" style="margin:0px 0px 0px 50px;">
                </div>
                <form>
                    <div class=row>
                        <div class="col s12"> <input style="background-color: #ffffff; border-bottom: none; margin:0px 0px 5px 0px;" value="" placeholder=" Nama Depan"
                                id="name" type="text" class="validate"></div>
                    </div>
                    <div class=row>
                        <div class="col s12">
                            <input style="background-color: #ffffff;border-bottom: none;margin:0px 0px 5px 0px; color:#ccc6c6" type="date" placeholder=" Tanggal Lahir"
                                class="datepicker">
                        </div>
                    </div>
                    <div class=row>
                        <div class="col s12">
                            <select class="browser-default" style="margin: 0 0 5px 0;">
                                    <option style="color:rgba(21, 19, 19, 0.04);" value="" disabled selected="" id="jenis_kelamin">Jenis Kelamin</option>
                                    <option value="laki">Laki-laki</option>
                                    <option value="perempuan">Perempuan</option>
                                </select></div>
                    </div>
                    <div class=row>
                        <div class="col s12"> <input style="background-color: #ffffff;border-bottom: none;margin:0px 0px 5px 0px;" value="" placeholder=" Email"
                                id="email" type="text" class="validate"></div>
                    </div>
                    <div class=row>
                        <div class="col s12"> <input style="background-color: #ffffff;border-bottom: none;margin:0px 0px 5px 0px;" value="" placeholder=" Password"
                                id="password" type="password" class="validate"></div>
                    </div>
                    <div class=row>
                        <div class="col s12">
                            <input style="background-color: #ffffff;border-bottom: none;margin:0px 0px 5px 0px;" value="" placeholder=" Konfirmasi Password"
                                id="confirmation_password" type="password" class="validate"> </div>
                    </div>
                    <div class=row>
                        <div class="col s12" style="margin:0px 0px 3px 0px;">
                            <a style="width: 100%;" class="waves-effect waves-light btn green" href="{{  url('/home/parent') }}">Daftar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <!--footer-->
    <!--
  <footer class="page-footer" style="background-color:rgba(206, 198, 198, 0.47);">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 style="color:#6b6b6b;">Footer Content</h5>
                    <p style="color:#6b6b6b;">You can use rows and columns here to organize your footer content.</p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 style="color:#6b6b6b;">Links</h5>
                    <ul>
                        <li><a style="color:#6b6b6b;" href="#!">Link 1</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Link 2</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Link 3</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Link 4</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container"style="color:#6b6b6b !important;"> © 2014 Copyright Text <a class="right" style="color:#6b6b6b;" href="#!">More Links</a> </div>
        </div>
    </footer>
-->
    <!--modal trigger javascript-->
    <script type="text/javascript">
        $(document).ready(function () {
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal-trigger').leanModal();
        });
    </script>
</body>

</html>