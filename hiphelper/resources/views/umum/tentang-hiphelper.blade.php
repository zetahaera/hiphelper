@extends('master')

@section('content')
<div class="about-us-cover">
    <div class="about-us-info">
        <h1>Hiphelper</h1>
        <h4>Hiphelper providing nannies whenever and wherever you need</h4>
    </div>
    <div class="overlay overlay-color"></div>
</div>


<div class="container paragraph">
    <div class="row">
        <div class="col s12 ">
            <p>HipHelper adalah platform penyedia layanan babysitter freelance yang bisa dipesan jasanya secara online, dimana saja dan kapan saja. Dengan tenaga pengasuh yang sabar dan terpelajar, anak Anda akan berada di tangan yang aman ketika Anda membutuhkan waktu-waktu tertentu untuk beraktivitas lain. Untuk saat ini, layanan hanya tersedia di wilayah Jakarta, Depok, Tangerang, & Bekasi.</p>
            <br>

            <div style="text-align:center;">
               <iframe width="854" height="480" src="https://www.youtube.com/embed/71Wn0P3iic4" frameborder="0" allowfullscreen></iframe>
            </div>

            <br>
         

        </div>
    </div>

<div class=about-us-link>
    <div class="row">
        <div class="col s12 m4 l4">
            <h5>Bergabung Bersama Kami</h5>
            <p>Apakah Anda tertarik untuk mendafar di sini? Silahkan klik <a style="color:#a6263e" href="{{url ('/registration/parent')}}">daftar</a> untuk bergabung bersama kami</p>
        </div>
        <div class="col s12 m4 l4">
            <h5>Lebih Dekat Dengan Kami</h5>
            <p>Untuk mengetahui informasi terkini mengenai Hiphelper silahkan pantau linimasa kami di <a style="color:#a6263e" href="{{url ('https://www.instagram.com/hiphelper/')}}">instagram</a> kami</p>
        </div>
        <div class="col s12 m4 l4">
             <h5>Cara Penggunaan website</h5>
            <p>Untuk mengetahui  cara penggunaan website silahkan klik <a style="color:#a6263e" href="{{url ('')}}">cara penggunaan</a> </p>
        </div>

    </div>
</div>
</div>
@stop