<!DOCTYPE html>
<!-- File homepage semua page -->
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>HipHelper</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="{{ url('css/materialize.min.css') }}" />
    <link rel="stylesheet" href="{{ url('css/master-css.css') }}" media="screen,projection" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ url('favicon.ico') }}" />
    <!-- implement csrf token for AJAX calling -->
    <!-- <meta name="_token" content="{!! csrf_token() !!}"/> -->
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/materialize.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/master-js.js') }}"></script>

<style>
.tabs .indicator {
    position: absolute;
    bottom: 0;
    height: 2px;
    background-color: #b7363b;
    will-change: left, right;
}
</style>
    
</head>

<body style="background-color:#f9f9f9">
    <div class="navbar-fixed">
        <nav>
            <div class="nav-wrapper">
                <div class="container"> <a href="#" id="logo" class="brand-logo">HipHelper</a>
                    <ul id="menu-navbar" class="right hide-on-med-and-down">
                        <li>
                            <!-- Login call modal --><a class="modal-trigger" href="#login">Masuk</a> </li>
                        <li> <a id="btn-daftar" class="waves-effect waves-light btn" href="{{  url('/registration/parent') }}">Daftar</a> </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <!--modal-->
    <div id="login" class="modal">
        <div class="modal-content" style="color: rgb(128, 128, 128); padding:24px 24px 0px 24px;">
            <div class="center">
                <h5 style=" font-family:Segoe UI;font-weight: 500; color:grey;">Login</h5> </div>
            <form>
                <div class="row">
                    <div class="input-field col s12">
                        <input style="background-color: rgba(167, 159, 159, 0.17);; border-bottom: none; margin:0px;" value="" placeholder=" Email" id="email" type="text" class="validate"> </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input style="background-color:rgba(167, 159, 159, 0.17); border-bottom: none; margin:0px;" value="" placeholder=" Password" id="password" type="password" class="validate"> </div>
                </div>
            </form>
        </div>
        <div class="modal-footer" style="padding: 0px 34px 0px 34px;"> <a style="width:30%;" href="#!" class="waves-effect waves-light btn green modal-action modal-close">Masuk</a> <a href="{{  url('/registration/parent') }}" class="btn btn-flat  click-to-toggle" style="color:green;background-color:transparent;box-shadow: 0px 0px 0px transparent;">Lupa Kata Sandi?</a></div>
    </div>

    
    <div class="slider">
        <ul class="slides">
            <li> <img style="border:none !important" src="{{ url('images/slider1.jpg') }}">
                <div class="caption center-align">
                    <div class="big-rectangle" style="background-color:rgba(41, 35, 36, 0.59);">
                        <h2 style="margin-bottom:10px;">Selamat Datang di HipHelper
							</h2>
                        <p style="font-size:20px; margin-top:10px;" class="light grey-text text-lighten-3"> "Providing you daily nannies whenever and wherever you need" </p>
                    </div>
                </div>
            </li>
            <li> <img style="border:none !important" src="{{ url('images/slider3.jpg') }}">
                <div class="caption center-align">
                    <div class="row">
                        <div class="col s4">
                            <div class="slider-card" style="background-color:rgba(41, 35, 36, 0.65);">
                                <h5 style="font-size:23px;" class="light grey-text text-lighten-3">
									Sangat membantu sekali untuk orang tua yang punya kesibukan di luar rumah untuk mengajarkan anak-anak
									</h5> <img src="{{ url('images/girl.png') }}" alt="" id="user-icon" class="circle responsive-img">
                                <h5 style="font-weight: bold;">
										Heidi
									</h5>
                                <h5 style="font-style: italic;">
										26 tahun - Pns
									</h5> </div>
                        </div>
                        <div class="col s4">
                            <div class="slider-card" style="background-color:rgba(41, 35, 36, 0.65);">
                                <h5 style="font-size:23px;" class="light grey-text text-lighten-3">
										Sangat membantu sekali untuk orang tua yang punya kesibukan di luar rumah untuk mengajarkan anak-anak
									</h5> <img src="{{ url('images/boy.png') }}" alt="" id="user-icon" class="circle responsive-img">
                                <h5 style="font-weight: bold;">
										Ayunda
									</h5>
                                <h5 style="font-style: italic;">
										22 tahun - PNS
									</h5> </div>
                        </div>
                        <div class="col s4">
                            <div class="slider-card" style="background-color:rgba(41, 35, 36, 0.65);">
                                <h5 style="font-size:23px;" class="light grey-text text-lighten-3">
										Terima kasih untuk para nany yang sudah cepat berinteraksi dengan anak dan dapat mengatasi konflik dengan baik.
									</h5> <img src="{{ url('images/girl.png') }}" alt="" id="user-icon" class="circle responsive-img">
                                <h5 style="font-weight: bold;">
										Nia
									</h5>
                                <h5 style="font-style: italic;">
										30 tahun - PNS
									</h5> </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="container" style="padding: 0 0.75rem;">
        <!--<div id="search-bar" class="z-depth-2">-->
        <ul class="collapsible" data-collapsible="accordion" style="border-top:none" ; />
        <li>
            <div class="collapsible-header">
                <form>
                    <div class="input-field"> <i class="material-icons prefix">search</i>
                        <input placeholder="Cari Babysitter atau Agen Penyalur" id="icon_prefix" type="text"> </div>
                </form>
            </div>
        </li>
        <li>
            <div style="padding-top: 7px;" class="collapsible-header green">
                <h6 class="center-align" style="color:white;">Pencarian Lebih Lanjut</h6> </div>
            <div class="collapsible-body" style="background-color:white;">
                <form>
                    <div class="row">
                        <div class="input-field col s4">
                            <input placeholder="Lokasi tempat tinggal Anda" type="text" id="autocomplete-input" class="autocomplete">
                            <label for="autocomplete-input">Lokasi</label>
                        </div>
                        <div class="col s12 m4">
                            <label>Pendidikan Terakhir</label>
                            <select class="browser-default">
                                <option value="" disabled selected="">Pendidikan Terakhir</option>
                                <option value="SMP/MTS">SMP/MTS</option>
                                <option value="SMA/SMK/MA">SMA/SMK/MA</option>
                                <option value="D3">D3</option>
                                <option value="S1">S1</option>
                                <option value="S2">S2</option>
                                <option value="S3">S3</option>
                            </select>
                        </div>
                        <div class="col s12 m4">
                            <label>Status</label>
                            <select class="browser-default">
                                <option value="" disabled selected="">Status</option>
                                <option value="semua">Semua</option>
                                <option value="lajang">Lajang</option>
                                <option value="menikah">Menikah</option>
                                <option value="janda">Janda</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s3">
                            <input placeholder="Minimal Umur" id="min-umur" type="number" class="validate">
                            <label for="min-umur">Minimal Umur</label>
                        </div>
                        <div class="input-field col s3">
                            <input placeholder="Maksimal Umur" id="max-umur" type="number" class="validate">
                            <label for="max-umur">Maksimal Umur</label>
                        </div>
                        <div class="col s3">
                            <label>Agama</label>
                            <select class="browser-default">
                                <option value="" disabled selected="">Agama</option>
                                <option value="semua">Semua</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Buddha">Buddha</option>
                                <option value="lainnya">lainnya</option>
                            </select>
                        </div>
                        <div class="col s3">
                            <label>Suku</label>
                            <select class="browser-default">
                                <option value="" disabled selected="">Suku</option>
                                <option value="semua">Sunda</option>
                                <option value="Islam">Jawa</option>
                                <option value="Kristen">Minang</option>
                                <option value="Katolik">Batak</option>
                                <option value="Hindu">Bugis</option>
                                <option value="Buddha">Lampung</option>
                                <option value="lainnya">Palembang</option>
                                <option value="lainnya">lainnya</option>
                            </select>
                        </div>
                    </div>
                    <div class="center-align"> <a class="waves-effect waves-light btn dark-maroon">Cari</a> </div>
                </form>
            </div>
        </li>
        </ul>
    </div>
    </div>
    <div id="card-babysitter" style="background-color:#f9f9f9">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <ul class="tabs" style="background-color:rgba(206, 198, 198, 0.47);">
                        <li class="tab col s3"><a style="color:maroon;" class="active" href="#pendatang-baru">Pendatang Baru</a></li>
                        <li class="tab col s3"><a style="color:maroon;"href="#tersedia">Tersedia</a></li>
                        <li class="tab col s3"><a style="color:maroon;" href="#segera-tersedia">Segera Tersedia</a></li>
                    </ul>
                </div>
            </div>
            <div id="pendatang-baru">
                <div class="row"> @for ($i = 0 ; $i < 9; $i++) <div class="col s12 m6 l4">
                        <div class="card white">
                            <div style="padding-bottom: 0px;padding-top: 0px;" class="card-content">
                                <div class="row">
                                    <div style="height: 25px;" class="col s12">
                                        <span class="card-title" style="color: #6b6767; font-family:Segoe UI; font-weight:500;">
                                                Dyah Nabila
                                            <span class="location grey-text text-darken-3">
                                                - Depok
                                            </span> 
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s6 grey-text text-darken-3"> <img class="avatar" src="{{ url('images/babysitter.jpg') }}">
                                        <div class="center-align"> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">query_builder</i></span> <span>6 tahun</span> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">perm_identity</i></span> <span>29 tahun</span> </div>
                                    </div>
                                    <div class="col s6 grey-text text-darken-3">
                                         <p>Selain mendalami ilmu pentitikan anak usia dini, saya juga aktif di komunitas sosial pecinta anak di waktu luang</p>
                                    </div>
                                    <div style="height: 10px;" class="col s12 center-align"> <a href="#">6 Review</a> </div>
                                </div>
                            </div>
                            <div class="card-action"> <a style="width: 100%;" class="waves-effect waves-light btn maroon" href="{{  url('/detil/babysitter') }}">Detil</a> </div>
                        </div>
                </div> @endfor </div></div>


          <div id="tersedia">
                <div class="row"> @for ($i = 0 ; $i < 6; $i++) <div class="col s12 m6 l4">
                        <div class="card white">
                            <div style="padding-bottom: 0px;padding-top: 0px;" class="card-content">
                                <div class="row">
                                    <div style="height: 25px;" class="col s12">
                                        <span class="card-title" style="color: #6b6767; font-family:Segoe UI; font-weight:500;">
                                                Dyah Nabila
                                            <span class="location grey-text text-darken-3">
                                                - Depok
                                            </span> 
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s6 grey-text text-darken-3"> <img class="avatar" src="{{ url('images/babysitter1.jpg') }}">
                                        <div class="center-align"> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">query_builder</i></span> <span>6 tahun</span> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">perm_identity</i></span> <span>29 tahun</span> </div>
                                    </div>
                                    <div class="col s6 grey-text text-darken-3">
                                         <p>Selain mendalami ilmu pentitikan anak usia dini, saya juga aktif di komunitas sosial pecinta anak di waktu luang</p>
                                    </div>
                                    <div style="height: 10px;" class="col s12 center-align"> <a href="#">6 Review</a> </div>
                                </div>
                            </div>
                            <div class="card-action"> <a style="width: 100%;" class="waves-effect waves-light btn maroon" href="{{  url('/detil/babysitter/login') }}">Detil</a> </div>
                        </div>
                </div> @endfor </div></div>


 <div id="segera-tersedia">
                <div class="row"> @for ($i = 0 ; $i < 3; $i++) <div class="col s12 m6 l4">
                        <div class="card white">
                            <div style="padding-bottom: 0px;padding-top: 0px;" class="card-content">
                                <div class="row">
                                    <div style="height: 25px;" class="col s12">
                                        <span class="card-title" style="color: #6b6767; font-family:Segoe UI; font-weight:500;">
                                               Dyah Nabila
                                            <span class="location grey-text text-darken-3">
                                                - Depok
                                            </span> 
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s6 grey-text text-darken-3"> <img class="avatar" src="{{ url('images/babysitter.jpg') }}">
                                        <div class="center-align"> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">query_builder</i></span> <span>2 tahun</span> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">perm_identity</i></span> <span>20 tahun</span> </div>
                                    </div>
                                    <div class="col s6 grey-text text-darken-3">
                                        <p>Selain mendalami ilmu pentitikan anak usia dini, saya juga aktif di komunitas sosial pecinta anak di waktu luang</p>
                                    </div>
                                    <div style="height: 10px;" class="col s12 center-align"> <a href="#">6 Review</a> </div>
                                </div>
                            </div>
                            <div class="card-action"> <a style="width: 100%;" class="waves-effect waves-light btn maroon" href="{{  url('/profile') }}">Detil</a> </div>
                        </div>
                </div> @endfor </div></div>


            
            <div class="right-align">
                <ul class="pagination">
                    <li class="disabled"><i class="material-icons">chevron_left</i></li>
                    <li class="active">1</li>
                    <li class="waves-effect">2</li>
                    <li class="waves-effect">3</li>
                    <li class="waves-effect">4</li>
                    <li class="waves-effect">5</li>
                    <li class="waves-effect"><i class="material-icons">chevron_right</i></li>
                </ul>
            </div>
        </div>
    </div>
    </div>
    <!--footer-->
    <footer class="page-footer" style="background-color:rgba(206, 198, 198, 0.47);">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 style="color:#6b6b6b;">Footer Content</h5>
                    <p style="color:#6b6b6b;">Hiphelper providing nannies whenever and wherever you need </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 style="color:#6b6b6b;">Links</h5>
                    <ul>
                        <li><a style="color:#6b6b6b;" href="{{ url('/about-us') }}">Tentang Hiphelper</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Tips Babysitter</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Hubungi Kami</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Cerita Inspiratif</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright" style="background-color:#a6263e;">
            <div class="container" style="color: white!important;"> © 2014 Copyright Text <a class="right" style="color:white;" href="#!">More Links</a> </div>
        </div>
    </footer>
    <script type="text/javascript">
        $(document).ready(function () {
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal-trigger').leanModal();
        });
    </script>

	</body>
</html>
