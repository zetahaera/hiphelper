
@extends('master') @section('content')
<style>
.tabs .indicator {
    position: absolute;
    bottom: 0;
    height: 2px;
    background-color: #b7363b;
    will-change: left, right;
}
</style>


 <div class="container" style="padding: 0 0.75rem;">
        <!--<div id="search-bar" class="z-depth-2">-->
        <ul class="collapsible" data-collapsible="accordion" style="border-top:none" ; />
        <li>
            <div class="collapsible-header">
                <form>
                    <div class="input-field"> <i class="material-icons prefix">search</i>
                        <input placeholder="Cari Babysitter atau Agen Penyalur" id="icon_prefix" type="text"> </div>
                </form>
            </div>
        </li>
        <li>
            <div style="padding-top: 7px;" class="collapsible-header green">
                <h6 class="center-align" style="color:white;">Pencarian Lebih Lanjut</h6> </div>
            <div class="collapsible-body" style="background-color:white;">
                <form>
                    <div class="row">
                        <div class="input-field col s4">
                            <input placeholder="Lokasi tempat tinggal Anda" type="text" id="autocomplete-input" class="autocomplete">
                            <label for="autocomplete-input">Lokasi</label>
                        </div>
                        <div class="col s12 m4">
                            <label>Pendidikan Terakhir</label>
                            <select class="browser-default">
                                <option value="" disabled selected="">Pendidikan Terakhir</option>
                                <option value="SMP/MTS">SMP/MTS</option>
                                <option value="SMA/SMK/MA">SMA/SMK/MA</option>
                                <option value="D3">D3</option>
                                <option value="S1">S1</option>
                                <option value="S2">S2</option>
                                <option value="S3">S3</option>
                            </select>
                        </div>
                        <div class="col s12 m4">
                            <label>Status</label>
                            <select class="browser-default">
                                <option value="" disabled selected="">Status</option>
                                <option value="semua">Semua</option>
                                <option value="lajang">Lajang</option>
                                <option value="menikah">Menikah</option>
                                <option value="janda">Janda</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s3">
                            <input placeholder="Minimal Umur" id="min-umur" type="number" class="validate">
                            <label for="min-umur">Minimal Umur</label>
                        </div>
                        <div class="input-field col s3">
                            <input placeholder="Maksimal Umur" id="max-umur" type="number" class="validate">
                            <label for="max-umur">Maksimal Umur</label>
                        </div>
                        <div class="col s3">
                            <label>Agama</label>
                            <select class="browser-default">
                                <option value="" disabled selected="">Agama</option>
                                <option value="semua">Semua</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Buddha">Buddha</option>
                                <option value="lainnya">lainnya</option>
                            </select>
                        </div>
                        <div class="col s3">
                            <label>Suku</label>
                            <select class="browser-default">
                                <option value="" disabled selected="">Suku</option>
                                <option value="semua">Sunda</option>
                                <option value="Islam">Jawa</option>
                                <option value="Kristen">Minang</option>
                                <option value="Katolik">Batak</option>
                                <option value="Hindu">Bugis</option>
                                <option value="Buddha">Lampung</option>
                                <option value="lainnya">Palembang</option>
                                <option value="lainnya">lainnya</option>
                            </select>
                        </div>
                    </div>
                    <div class="center-align"> <a class="waves-effect waves-light btn dark-maroon">Cari</a> </div>
                </form>
            </div>
        </li>
        </ul>
    </div>
    </div>
   
    <div id="card-babysitter" style="background-color:#f9f9f9">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <ul class="tabs" style="background-color:rgba(206, 198, 198, 0.47);">
                        <li class="tab col s3"><a style="color:maroon;" class="active" href="#pendatang-baru">Pendatang Baru</a></li>
                        <li class="tab col s3"><a style="color:maroon;"href="#tersedia">Tersedia</a></li>
                        <li class="tab col s3"><a style="color:maroon;" href="#segera-tersedia">Segera Tersedia</a></li>
                    </ul>
                </div>
            </div>
            <div id="pendatang-baru">
                <div class="row"> @for ($i = 0 ; $i < 9; $i++) <div class="col s12 m6 l4">
                        <div class="card white">
                            <div style="padding-bottom: 0px;padding-top: 0px;" class="card-content">
                                <div class="row">
                                    <div style="height: 25px;" class="col s12">
                                        <span class="card-title" style="color: #6b6767; font-family:Segoe UI; font-weight:500;">
                                                Dyah Nabila
                                            <span class="location grey-text text-darken-3">
                                                - Depok
                                            </span> 
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s6 grey-text text-darken-3"> <img class="avatar" src="{{ url('images/babysitter.jpg') }}">
                                        <div class="center-align"> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">query_builder</i></span> <span>6 tahun</span> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">perm_identity</i></span> <span>29 tahun</span> </div>
                                    </div>
                                    <div class="col s6 grey-text text-darken-3">
                                         <p>Selain mendalami ilmu pentitikan anak usia dini, saya juga aktif di komunitas sosial pecinta anak di waktu luang</p>
                                    </div>
                                    <div style="height: 10px;" class="col s12 center-align"> <a href="#">6 Review</a> </div>
                                </div>
                            </div>
                            <div class="card-action"> <a style="width: 100%;" class="waves-effect waves-light btn maroon" href="{{  url('/detil/babysitter') }}">Detil</a> </div>
                        </div>
                </div> @endfor </div></div>


          <div id="tersedia">
                <div class="row"> @for ($i = 0 ; $i < 6; $i++) <div class="col s12 m6 l4">
                        <div class="card white">
                            <div style="padding-bottom: 0px;padding-top: 0px;" class="card-content">
                                <div class="row">
                                    <div style="height: 25px;" class="col s12">
                                        <span class="card-title" style="color: #6b6767; font-family:Segoe UI; font-weight:500;">
                                                Dyah Nabila
                                            <span class="location grey-text text-darken-3">
                                                - Depok
                                            </span> 
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s6 grey-text text-darken-3"> <img class="avatar" src="{{ url('images/babysitter1.jpg') }}">
                                        <div class="center-align"> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">query_builder</i></span> <span>6 tahun</span> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">perm_identity</i></span> <span>29 tahun</span> </div>
                                    </div>
                                    <div class="col s6 grey-text text-darken-3">
                                         <p>Selain mendalami ilmu pentitikan anak usia dini, saya juga aktif di komunitas sosial pecinta anak di waktu luang</p>
                                    </div>
                                    <div style="height: 10px;" class="col s12 center-align"> <a href="#">6 Review</a> </div>
                                </div>
                            </div>
                            <div class="card-action"> <a style="width: 100%;" class="waves-effect waves-light btn maroon" href="{{  url('/detil/babysitter/login') }}">Detil</a> </div>
                        </div>
                </div> @endfor </div></div>


 <div id="segera-tersedia">
                <div class="row"> @for ($i = 0 ; $i < 3; $i++) <div class="col s12 m6 l4">
                        <div class="card white">
                            <div style="padding-bottom: 0px;padding-top: 0px;" class="card-content">
                                <div class="row">
                                    <div style="height: 25px;" class="col s12">
                                        <span class="card-title" style="color: #6b6767; font-family:Segoe UI; font-weight:500;">
                                               Dyah Nabila
                                            <span class="location grey-text text-darken-3">
                                                - Depok
                                            </span> 
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s6 grey-text text-darken-3"> <img class="avatar" src="{{ url('images/babysitter.jpg') }}">
                                        <div class="center-align"> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">query_builder</i></span> <span>2 tahun</span> </div>
                                        <div class="col s12"> <span><i class="tiny material-icons prefix">perm_identity</i></span> <span>20 tahun</span> </div>
                                    </div>
                                    <div class="col s6 grey-text text-darken-3">
                                        <p>Selain mendalami ilmu pentitikan anak usia dini, saya juga aktif di komunitas sosial pecinta anak di waktu luang</p>
                                    </div>
                                    <div style="height: 10px;" class="col s12 center-align"> <a href="#">6 Review</a> </div>
                                </div>
                            </div>
                            <div class="card-action"> <a style="width: 100%;" class="waves-effect waves-light btn maroon" href="{{  url('/profile') }}">Detil</a> </div>
                        </div>
                </div> @endfor </div></div>


            
            <div class="right-align">
                <ul class="pagination" style="margin-bottom:0px;">
                    <li class="disabled"><i class="material-icons">chevron_left</i></li>
                    <li class="active">1</li>
                    <li class="waves-effect">2</li>
                    <li class="waves-effect">3</li>
                    <li class="waves-effect">4</li>
                    <li class="waves-effect">5</li>
                    <li class="waves-effect"><i class="material-icons">chevron_right</i></li>
                </ul>
            </div>
        </div>
    </div>
    </div>
 
@stop
