
@extends('master') @section('content')
<div class="container">
    <div class="row" style="margin-bottom:0px;">
            <div class="col s12 m3 l3">
			<div class="card">
				<div class="card-content">
			<img class="avatar" src="{{ url('images/avatar.jpg') }}">
	
			</div></div>
		</div>

        <div class="col s12 m9 l9">
            <div style=" padding:20px;">
                <div class="row">
                    <div class="col s9">
                    <h4 style="margin-top:0px;"> Profile Zahra Zulutfa</h4></div>
                    <div class="col s3">
                <a style="width: 100%;" class="waves-effect waves-light btn orange" href="{{url('/profile/parent/edit') }}"><i class="material-icons right">mode_edit</i>Edit</a></div>
                </div>
                <div class="row">
                
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active"><i class="material-icons">perm_identity</i>Profile Anda</div>
                        <div class="collapsible-body" style="background-color:white;">
                            <table>
                                <colgroup width="100px"></colgroup>
                                <colgroup></colgroup>
                                <tbody>
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>
                                            Zahra Zulutfa
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>:</td>
                                        <td>
                                            Perempuan
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>
                                            Jalan Mangga No 20, Jakarta Barat 164257
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Umur</td>
                                        <td>:</td>
                                        <td>
                                            30 Tahun
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor HP</td>
                                        <td>:</td>
                                        <td>
                                            086823672519276
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td>
                                            zahra@gmail.com
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </li>
                </ul>
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">loyalty</i>Profile Anak</div>
                        <div class="collapsible-body" style="background-color:white;">
                            <table>
                                <colgroup width="100px"></colgroup>
                                <colgroup></colgroup>
                                <tbody>
                                    <tr>
                                        <td>Nama Anak</td>
                                        <td>:</td>
                                        <td>
                                            Aisyah Nurmala
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>:</td>
                                        <td>
                                        Perempuan
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Keterangan Tambahan</td>
                                        <td>:</td>
                                            <td>
                                            Aisyah tidak suka makan sayur dan makanan yang terlalu manis
                                            </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </li>
                </ul>
                 
            </div>
        </div>
      </div>
    </div>
</div>

 @stop