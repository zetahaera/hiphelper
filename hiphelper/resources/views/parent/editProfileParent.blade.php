
@extends('master') @section('content')
<div class="container">
    <div class="row" style="margin-bottom:0px;">
        
        <div class="col s12 l3 m3">
			<div class="card">
				<div class="card-content">
			<img class="avatar" src="{{ url('images/avatar.jpg') }}">
			<br>
			<br>
			<a class="waves-effect waves-light btn" style="width:100%;"><i class="material-icons left">publish</i>Upload Foto</a>		
			</div></div>
		</div>
        
        <div class="col s12 l9 m9">
            <div style="padding:20px;">
                <h4 style="margin-top:0px;"> Profile Zahra Zulutfa</h4>
            
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active" style="background-color:white;"><i class="material-icons">mode_edit</i>Edit Profile Anda</div>
                        <div class="collapsible-body" style="background-color:white;">
                            <table>
                                <colgroup width="100px"></colgroup>
                                <colgroup></colgroup>
                                <tbody>
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>
                                            <input placeholder="Nama Lengkap" id="nama_lengkap" type="text" class="validate">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>:</td>
                                        <td>
                                            <select class="browser-default">
                                                <option value="" disabled selected="" id="jenis_kelamin">Jenis Kelamin</option>
                                                <option value="laki">Laki-laki</option>
                                                <option value="perempuan">Perempuan</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>
                                            <input placeholder="Alamat" id="alamat" type="text" class="validate">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>
                                           <input style="background-color: #ffffff;border-bottom: none;margin:0px 0px 5px 0px; color:#ccc6c6" type="date" placeholder=" Tanggal Lahir"
                                            class="datepicker">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor HP</td>
                                        <td>:</td>
                                        <td>
                                            <input placeholder="Nomor Handphone" id="no_hp" type="text" class="validate">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </li>
                </ul>
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">mode_edit</i>Edit Akun</div>
                        <div class="collapsible-body" style="background-color:white;">
                            <table>
                                <colgroup width="100px"></colgroup>
                                <colgroup></colgroup>
                                <tbody>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td>
                                            <input placeholder="Email" id="email" type="text" class="validate">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Password</td>
                                        <td>:</td>
                                        <td>
                                            <input placeholder="Password" id="alamat" type="password" class="validate">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Konfirmasi Password</td>
                                        <td>:</td>
                                        <td>
                                            <input placeholder="Konfirmasi Password" id="alamat" type="password" class="validate">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </li>
                </ul>
                 
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">mode_edit</i>Edit Profile Anak</div>
                        <div class="collapsible-body" style="background-color:white;">
                        <div class="child_input">
                            <table id="child">
                                <colgroup width="100px"></colgroup>
                                <colgroup></colgroup>
                                <tbody>
                                    <tr>
                                        <td>Nama Anak</td>
                                        <td>:</td>
                                        <td>
                                            <input placeholder="Nama Lengkap" id="nama_lengkap" type="text" class="validate">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>:</td>
                                        <td>
                                            <select class="browser-default">
                                                <option value="" disabled selected="" id="jenis_kelamin">Jenis Kelamin</option>
                                                <option value="laki">Laki-laki</option>
                                                <option value="perempuan">Perempuan</option>
                                            </select>
                                        </td>
                                   
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>
                                           <input style="background-color: #ffffff;border-bottom: none;margin:0px 0px 5px 0px; color:#ccc6c6" type="date" placeholder=" Tanggal Lahir"
                                            class="datepicker">
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>Alergi</td>
                                        <td>:</td>
                                            <td>
                                                <input placeholder="Alergi Makanan/Obat" id="alergi" type="text" class="validate">
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                           <div style="float:left;"><a href="#" class="add_child waves-effect waves-light btn"><i class="material-icons left">playlist_add</i>Tambah Anak </a></div>    
                                           
                                        </td>
                                    </tr>
                                </tbody>
                    
                                
                            </table>
                            <table id="child-template" hidden>
                                <colgroup width="100px"></colgroup>
                                <colgroup></colgroup>
                                <tbody>
                                    <tr>
                                        <td>Nama Anak</td>
                                        <td>:</td>
                                        <td>
                                            <input placeholder="Nama Lengkap" id="nama_lengkap" type="text" class="validate">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>:</td>
                                        <td>
                                            <select class="browser-default">
                                                <option value="" disabled selected="" id="jenis_kelamin">Jenis Kelamin</option>
                                                <option value="laki">Laki-laki</option>
                                                <option value="perempuan">Perempuan</option>
                                            </select>
                                        </td>
                                   
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>
                                           <input style="background-color: #ffffff;border-bottom: none;margin:0px 0px 5px 0px; color:#ccc6c6" type="date" placeholder=" Tanggal Lahir"
                                            class="datepicker">
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>Alergi</td>
                                        <td>:</td>
                                            <td>
                                                <input placeholder="Alergi Makanan/Obat" id="alergi" type="text" class="validate">
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <div style="float:left;"><a href="#" class="add_child waves-effect waves-light btn"><i class="material-icons left">playlist_add</i>Tambah Anak </a></div>    
                                              <div style="float:right;"><i style="color:red;" class="material-icons left remove_field">delete</i> </a></div>    
                                        </td>
                                    </tr>
                                </tbody>
                    
                                
                            </table>
                        </div>
                        </div>
                        
                    </li>
                </ul>
                <div class="right-align">  
                <a class="waves-effect waves-light btn green" href="{{  url('/profile/parent') }}">Simpan</a></div>
            </div>
        </div>
    </div>
</div>


<script>
    
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".child_input"); //Fields wrapper
    var add_button      = $(".add_child"); //Add button ID
    
    var x = 1; //initlal text box count
    $(document).on('click', '.add_child',function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $('#child-template').show();
            $template = $('#child-template').clone();
            $('#child-template').hide();
            $(wrapper).append($template); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent().parent().parent().parent().parent().remove(); x--;
    })
});

</script>

 @stop