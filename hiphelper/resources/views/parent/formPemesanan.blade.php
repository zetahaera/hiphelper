@extends('master') @section('content')
<style>
    #calendar {
        max-width: 900px;
        margin: 0 auto;
    }
    
    .wizard>.content>.body {
        float: left;
        position: relative;
        width: 95%;
        height: 95%;
        padding: 2.5%;
    }

    .wizard > .content {
        background: #fafafa;
        display: block;
        margin: 0.5em;
        min-height: 35em;
        overflow: hidden;
        position: relative;
        width: auto;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        border: 1px solid maroon;
    }
    .wizard>.content>.body {
        float: left;
        position: relative;
        width: 95%;
        height: 95%;
        padding: 0;
        
    }

    
    .wizard > .actions a, .wizard > .actions a:hover, .wizard > .actions a:active {
        background: green;
        color: #fff;
        display: block;
        padding: 0 2rem;
        padding-top: 0px;
        padding-right: 2rem;
        padding-bottom: 0px;
        padding-left: 2rem;
        text-decoration: none;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }
    .wizard > .actions .disabled a, .wizard > .actions .disabled a:hover, .wizard > .actions .disabled a:active {
    background: #eee !important;
    color: #aaa;
    }

.wizard > .steps .current a,
.wizard > .steps .current a:hover,
.wizard > .steps .current a:active
{
    background: green;
    color: #fff;
    cursor: default;
}

.wizard > .steps .done a,
.wizard > .steps .done a:hover,
.wizard > .steps .done a:active
{
    background:rgba(76, 175, 80, 0.61);
    color: #fff;
}


</style>
<div class="container">
    <!--for short bio-->
    <div class="row">
        <div style="border: 1px solid maroon;margin: 5px;border-radius: 6px;">
            <h5 style="box-sizing: border-box;background-color: maroon; width: 30%;padding: 12px 20px; color: white;"> Babysitter yang Dipesan</h5>
            <div style="padding:20px;">
                <div class="row">
                    <div class="col s3"> <img width=100% id="img_01" src="{{ url('images/girl.png') }}" data-zoom-image="{{ url('images/avatar.jpg') }}"
                        /> </div>
                    <div class="col s9">
                        <div class="col s12">
                            <h4 style="margin-bottom: 4px; margin-top:0px"> Niea Kurnia Fajar </h4>
                            <div class="left-align"> <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i>                                <i class="tiny material-icons bintang">star</i> <i class="tiny material-icons bintang">star</i>                                <i class="tiny material-icons bintang">star</i> </div> <a href="#">6 Review</a> </div>
                        <div class="col s12 no-padding">
                            <br>
                            <div class="col s6"> <span><i class="tiny material-icons">room</i>  Kampus UI Depok</span> </div>
                            <div class="col s6"> <span><i class="tiny material-icons">picture_in_picture</i>  Islam</span> </div>
                            <div class="col s6">
                                <br> <span><i class="tiny material-icons">store</i>  Sunda </span> </div>
                            <div class="col s6">
                                <br> <span><i class="tiny material-icons">schedule</i>  22 years old</span> </div>
                            <div class="col s6">
                                <br> <span><i class="tiny material-icons">mode_edit</i>  SMA</span> </div>
                            <div class="col s6">
                                <br> <span><i class="tiny material-icons">loyalty</i>  Belum Menikah</span> </div>
                            <div class="col s12">
                                <br> <span><i class="tiny material-icons">assignment_ind</i>  Memiliki keahlian memasak, mencuci, mengasuh bayi</span>                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-11 col-centered">
            <script>
                $(function ()
                {
                    $("#wizard").steps({
                        headerTag: "h2",
                        bodyTag: "section",
                        transitionEffect: "slideLeft"
                    });
                });
            </script>
            <div id="wizard" style="">
                <h2>Jadwal</h2>
                <section>
                    <h5 style="box-sizing: border-box;background-color: maroon; width: 30%;padding: 12px 20px; color: white;"> Pilih Jadwal Babysitter</h5>
                    <div id='calendar' style="margin:30px 100px 100px 100px;">
                </section>
                <h2>Formulir</h2>
                <section>
                      <h5 style="box-sizing: border-box;background-color: maroon; width: 30%;padding: 12px 20px; color: white;"> Formulir Pemesanan</h5>
                    <form style="margin:30px 100px 100px 100px;">
                        <div class="row">
                            <div class="input-field col s6">
                                <input disabled value="Rabu, 30 November 2016 " id="disabled" type="text" class="validate">
                                <label for="disabled">Waktu Pemesanan</label>
                            </div>
                            <div class="input-field col s6">
                                <input disabled value="Pukul 09.00-17.00 WIB " id="disabled" type="text" class="validate">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="first_name" type="text" class="validate" value="Jayden Smith">
                                <label for="first_name">Nama Lengkap</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="nomor_telpon" type="text" class="validate" value="080989999">
                                <label for="nomor_telpon">Nomor Telepon</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="email" type="email" class="validate" value="jayden@email.com">
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <label>Jumlah Anak</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <select id="numberofchild" name="numberofchild" class="browser-default">
                            <option value="" disabled selected="" id="jumlah_anak">Jumlah Anak</option>
                            <option value="1">1 Anak</option>
                            <option value="2">2 Anak</option>
                        </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s6" id="child1" hidden>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <h6>Data Anak Pertama</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="first_child" type="text" class="validate" value="">
                                        <label for="first_name">Nama Anak 1</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                        <select class="browser-default">
                                    <option value="" disabled selected="" id="sex">Jenis kelamin</option>
                                    <option value="boy">Laki-laki</option>
                                    <option value="girl">Perempuan</option>
                                </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="first_notes" type="text" class="materialize-textarea validate"></textarea>
                                        <label for="first_name">Catatan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col s6" id="child2" hidden>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <h6>Data Anak Kedua</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="second_child" type="text" class="validate" value="">
                                        <label for="second_name">Nama Anak 2</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                        <select class="browser-default">
                                    <option value="" disabled selected="" id="sex">Jenis kelamin</option>
                                    <option value="boy">Laki-laki</option>
                                    <option value="girl">Perempuan</option>
                                </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="second_notes" type="text" class="materialize-textarea validate"></textarea>
                                        <label for="first_name">Catatan</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
                <h2>Konfirmasi</h2>
                <section>
                      <h5 style="box-sizing: border-box;background-color: maroon; width: 30%;padding: 12px 20px; color: white;"> Konfirmasi Pemesanan</h5>
                    <form style="margin:30px 100px 100px 100px;">
                        <div class="row">
                            <div class="input-field col s6">
                                <input disabled value="Rabu, 30 November 2016 " id="disabled" type="text" class="validate">
                                <label for="disabled">Waktu Pemesanan</label>
                            </div>
                            <div class="input-field col s6">
                                <input disabled value="Pukul 09.00-17.00 WIB " id="disabled" type="text" class="validate">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="first_name" type="text" class="validate" value="Jayden Smith">
                                <label for="first_name">Nama Lengkap</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="nomor_telpon" type="text" class="validate" value="080989999">
                                <label for="nomor_telpon">Nomor Telepon</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="email" type="email" class="validate" value="jayden@email.com">
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="email" type="email" class="validate" value="2 Anak">
                                <label for="email">Jumlah Anak</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s6" id="child1">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <h6>Data Anak Pertama</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="first_child" type="text" class="validate" value="Nilamsari">
                                        <label for="first_name">Nama Anak 1</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="sex" type="text" class="validate" value="Perempuan">
                                        <label for="sex">Jenis Kelamin</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="first_notes" type="text" class="materialize-textarea validate">Alergi udang, obat terakhir yang dikonsumsi antibiotik</textarea>
                                        <label for="first_notes">Catatan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col s6" id="child2">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <h6>Data Anak Kedua</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="second_child" type="text" class="validate" value="Feaza">
                                        <label for="second_name">Nama Anak 2</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="sex" type="text" class="validate" value="Laki-laki">
                                        <label for="sex">Jenis Kelamin</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="second_notes" type="text" class="materialize-textarea validate">Suka es krim, tidur harus dengan guling, tidak suka makan sayur</textarea>
                                        <label for="first_name">Catatan</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
                </div>
            </div>
        </div>
    </div>
</div>

     <!-- Modal login -->
    <div id="finish" class="modal">
        <div class="modal-content" style="color: rgb(128, 128, 128);">
            Apakah Anda sudah yakin?
        </div>
        <div class="modal-footer"> <a href="{{url('parent/transaksi')}}" class="modal-action modal-close waves-effect waves-green btn-flat">Ya</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Kembali</a> </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $( "a[href=#finish]" ).addClass( "modal-trigger" );
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal-trigger').leanModal();
        });
    </script>

<script>
    $(document).ready(function () {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today'
                , center: 'title'
                , right: 'month,basicWeek,basicDay'
            }
            , defaultDate: '2016-09-12'
            , navLinks: true, // can click day/week names to navigate views
            editable: true
            , eventLimit: true, // allow "more" link when too many events
            events: [
                {
                    title: 'All Day Event'
                    , start: '2016-09-01'
    				}
                , {
                    title: 'Long Event'
                    , start: '2016-09-07'
                    , end: '2016-09-10'
    				}
                , {
                    id: 999
                    , title: 'Repeating Event'
                    , start: '2016-09-09T16:00:00'
    				}
                , {
                    id: 999
                    , title: 'Repeating Event'
                    , start: '2016-09-16T16:00:00'
    				}
                , {
                    title: 'Conference'
                    , start: '2016-09-11'
                    , end: '2016-09-13'
    				}
                , {
                    title: 'Meeting'
                    , start: '2016-09-12T10:30:00'
                    , end: '2016-09-12T12:30:00'
    				}
                , {
                    title: 'Lunch'
                    , start: '2016-09-12T12:00:00'
    				}
                , {
                    title: 'Meeting'
                    , start: '2016-09-12T14:30:00'
    				}
                , {
                    title: 'Happy Hour'
                    , start: '2016-09-12T17:30:00'
    				}
                , {
                    title: 'Dinner'
                    , start: '2016-09-12T20:00:00'
    				}
                , {
                    title: 'Birthday Party'
                    , start: '2016-09-13T07:00:00'
    				}
                , {
                    title: 'Click for Google'
                    , url: 'http://google.com/'
                    , start: '2016-09-28'
    				}
    			]
        });
           
    });

    
</script>
<script>
    $( document ).ready(function() {
        $("select[name=numberofchild]").on('change',function() {
            var child = $(this).val();
            if(child == 1){
                $("div#child1").show();
                $("div#child2").hide();
            }else if(child == 2){
                $("div#child1").show();
                $("div#child2").show();
            }else{
                $("div#child1").hide();
                $("div#child2").hide();
            }
        });
    });

</script> 

@stop