<!DOCTYPE html>
<!-- File master semua page -->
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>HipHelper</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="{{ url('css/materialize.min.css') }}" />
    <link rel="stylesheet" href="{{ url('css/master-css.css') }}" media="screen,projection" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ url('favicon.ico') }}" />
    <!-- implement csrf token for AJAX calling -->
    <!-- <meta name="_token" content="{!! csrf_token() !!}"/> -->
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/materialize.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/master-js.js') }}"></script>
</head>

<body id=background>
    <nav>
        <div class="nav-wrapper">
            <div class="container"> <a href="#" id="logo" class="brand-logo">HipHelper</a>
                <ul id="menu-navbar" class="right hide-on-med-and-down">
                    <li>
                        <!-- Login call modal --><a class="modal-trigger" href="#login">Masuk</a> </li>
                    <li> <a id="btn-daftar" class="waves-effect waves-light btn" href="{{  url('/register') }}">Daftar</a> </li>
                </ul>
            </div>
        </div>
    </nav>
    
    <!-- Modal login -->
    <div id="login" class="modal">
        <div class="modal-content" style="color: rgb(128, 128, 128);">
            <form>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="" id="email-login" type="text" class="validate">
                        <label class="active" for="email-login">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="" id="password-login" type="password" class="validate">
                        <label class="active" for="password-login">Password</label>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer"> <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Masuk</a> </div>
    </div>
    
    <div class="row" id="gap-space">
        <div class="container">
            <div class="row">
                <div id="pd-form" class="col s2">
                    <div class="row"></div>
                </div>
                <div id="main-form" class="z-depth-baru col s8">
                    <!-- <div class="z-depth-baru"> -->
                    <!-- <mulai ngedit dari sini> -->
                   {!!  Form::open(array('action' => 'babysitterController@getListBS')) !!}
        <div class="container">
    <div class="col-md-8">
        <div class="table-responsive">
            <table id="searchTable" class="table" style="margin-left:25%; margin-right:15%;">   
                <thead> 
                    <tr>
                       
                        <th>Name</th>
                        <th>Position</th>
                        <th>Company</th>
                        <!-- <th>Choose</th> -->
                    </tr>
                </thead>
                <tbody>
                  
                    @foreach ($babysitters as $babysitter)
                 
                    <tr>
                        
                        <td>
                            
                             {{ $babysitter-> nama }}
                                
                        </td>
                        <td> 
                            {{ $babysitter-> Email }}
                        </td>
                        <td>
                            {{ $babysitter-> Alamat}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{!! Form::close() !!}
 					<a id="btn-daftar" class="waves-effect waves-light btn" href="{{  url('/booking') }}">Booking</a> 


                    <!-- <sampe sini> -->
                </div>
                <div id="pd-form" class="col s2">
                    <div class="row"></div>
                </div>
            </div>
        </div>
    </div>
    <footer class="page-footer">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Footer Content</h5>
                    <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container"> © 2014 Copyright Text <a class="grey-text text-lighten-4 right" href="#!">More Links</a> </div>
        </div>
    </footer>
    <script type="text/javascript">
        $(document).ready(function () {
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal-trigger').leanModal();
        });
    </script>
</body>

</html>