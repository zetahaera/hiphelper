<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>HipHelper</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="{{ url('css/materialize.min.css') }}" />
    <link rel="stylesheet" href="{{ url('css/master-css.css') }}" media="screen,projection" />
    <link rel="stylesheet" href="{{ url('lib/fullcalendar-3.0.1/fullcalendar.min.css') }}" />
    <link rel="stylesheet" href="{{ url('lib/fullcalendar-3.0.1/fullcalendar.print.css') }}"  media='print'  />
     <link rel="stylesheet" href="{{ url('lib/css-progress-wizard-master/css-progress-wizard-master/css/progress-wizard.min.css') }}" />
<!--
    <link rel="stylesheet" href="{{ url('lib/multiDatesPicker/css/jquery-ui.css') }}"  media='print'  />
    <link rel="stylesheet" href="{{ url('lib/multiDatesPicker/css/jquery-ui.structure.css') }}"  media='print'  />
    <link rel="stylesheet" href="{{ url('lib/multiDatesPicker/css/jquery-ui.theme.css') }}"  media='print'  />
-->
    <link rel="stylesheet" href="{{ url('lib/multiDatesPicker/css/mdp.css') }}"  media='print'  />
    <link rel="stylesheet" href="{{ url('lib/multiDatesPicker/css/pepper-ginder-custom.css') }}"  media='print'  />
    <!--<link rel="stylesheet" href="{{ url('lib/multi-step-form/css/main.css') }}"  />-->
    <link rel="stylesheet" href="{{ url('lib/multi-step-form/css/jquery.steps.css') }}"  />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ url('favicon.ico') }}" />
    <!-- implement csrf token for AJAX calling -->
    <!-- <meta name="_token" content="{!! csrf_token() !!}"/> -->
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="{{ url('lib/fullcalendar-3.0.1/lib/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('lib/multiDatesPicker/js/jquery-ui-1.11.1.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/materialize.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/master-js.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/elevatezoom/jquery.elevateZoom-3.0.8.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ url('lib/fullcalendar-3.0.1/fullcalendar.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('lib/multiDatesPicker/jquery-ui.multidatespicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('lib/multi-step-form/lib/jquery.cookie-1.3.1.js') }}"></script>
    <script type="text/javascript" src="{{ url('lib/multi-step-form/build/jquery.steps.js') }}"></script>
</head>

<body style= "background-color: #f9f9f9;">
    <div class="navbar-fixed">
        <nav>
            <div class="nav-wrapper">
                <div class="container"> <a href="#" id="logo" class="brand-logo">HipHelper</a>
                    <ul id="menu-navbar" class="right hide-on-med-and-down">
                        <li style="width:20%;">
                            <a style="padding-left: 1px;padding-right: 1px;height: 64px;"><img id="avatar-circle" src="{{ url('images/girl.png') }}" alt="" class="circle responsive-img" style="width: 38px;margin: 0 auto;vertical-align: middle; "></a>
                        </li>
                        <ul id="dropdown-account" class="dropdown-content">
                            <li><a href="#!">Logout</a></li>
                            <li><a href="{{url('profile/parent/edit')}}">Edit Profile</a></li>
                        </ul>
                        <li> <a class="dropdown-button" href="#" data-activates="dropdown-account">Jayden Smith<i class="material-icons right">arrow_drop_down</i></a> </li>
                    </ul>
                </div>
            </div>
            <div style="background-color: #efeaea;" class="row z-depth-1">
                <div class="container">
                     <a href="#" data-activates="mobile-demo" class="button-collapse"><i style="color:#a6263e;" class="material-icons">menu</i></a>
                        <ul id="nav-mobile" class="right hide-on-med-and-down">
                            <li style="height: 36px;"> <a class="btn-flat" style="color: #790000;font-weight: bold;line-height: 1px;" href="">Transaksi</a> </li>
                            <li style="height: 36px;"> <a class="btn-flat" style="color: #790000;font-weight: bold;line-height: 1px;" href="">Cara Penggunaan</a> </li>
                            <li style="height: 36px;"> <a class="btn-flat" style="color: #790000;font-weight: bold;line-height: 1px;" href="{{ url('/about-us') }}">Tentang HipHelper</a> </li>
                        </ul>
                        <ul class="side-nav" id="mobile-demo">
                            <li style="width:100%;">
                                <a style="float:left;"><img id="avatar-circle" src="{{ url('images/girl.png') }}" alt="" class="circle responsive-img" style="width: 38px;margin: 0 auto;vertical-align: middle; "></a>
                                <a class="dropdown-button" href="#" data-activates="dropdown-account2">Jayden Smith<i style="margin-top: 18px;" class="material-icons right">arrow_drop_down</i></a> 
                            </li>
                            <ul id="dropdown-account2" class="dropdown-content">
                                <li><a href="#!">Logout</a></li>
                                <li><a href="{{url('profile/parent/edit')}}">Edit Profile</a></li>
                            </ul>
                           
                            <li> <a class="btn-flat" style="color: #790000;font-weight: bold;" href="">Transaksi</a> </li>
                            <li> <a class="btn-flat" style="color: #790000;font-weight: bold;" href="">Cara Penggunaan</a> </li>
                            <li> <a class="btn-flat" style="color: #790000;font-weight: bold;" href="{{ url('/about-us') }}">Tentang HipHelper</a> </li>

                        </ul>
                </div>
            </div>
        </nav>
    </div>
    <div style="margin-top:75px;"> @yield('content') </div>
     <!--footer-->
    <footer class="page-footer" style="background-color:rgba(206, 198, 198, 0.47); margin-top:0px;">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 style="color:#6b6b6b;">Footer Content</h5>
                    <p style="color:#6b6b6b;">Hiphelper providing nannies whenever and wherever you need </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 style="color:#6b6b6b;">Links</h5>
                    <ul>
                        <li><a style="color:#6b6b6b;" href="{{ url('/about-us') }}">Tentang Hiphelper</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Tips Babysitter</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Hubungi Kami</a></li>
                        <li><a style="color:#6b6b6b;" href="#!">Cerita Inspiratif</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright" style="background-color:#a6263e;">
            <div class="container" style="color: white!important;"> © 2014 Copyright Text <a class="right" style="color:white;" href="#!">More Links</a> </div>
        </div>
    </footer>
</body>
<script>
$(".button-collapse").sideNav();
</script>
</html>
