<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/home', function () {
    return view('home');
});

Route::get('/beranda', function () {
    return view('beranda');
});

Route::get('/login',function(){
	return view('master');
});

Route::get('/booking',function(){ //di profil baby sitter ada button booking, run ke sini
	return view('booking');
});
Route::post('/booking', 'babysitterController@booking');

Route::post('/register', 'babysitterController@register'); //register babysitter


//=========================FITUR UMUM=========================================//

//tampilan halaman registrasi//
    Route::get('/registration/parent',function(){
	return view('umum/formulirPendaftaran');
});

//tampilan halaman public/list babysitter sebelum login//
Route::get('/',function(){
	return view('umum/homeNonLogin');
});

//tampilan halaman about us//
Route::get('/about-us',function(){
	return view('umum/tentang-hiphelper');
});

//tampilan halaman detail babysitter sebelum login//
Route::get('/detil/babysitter',function(){
	return view('umum/detilBabysitter');
});

//=============================================================================//

//=========================FITUR PARENT=========================================//

//tampilan halaman parent setelah login aplikasi//
Route::get('/parent',function(){
	return view('parent/homeAfterLogin');
});

//tampilan halaman detail babysitter-parent//
Route::get('/detil/babysitter/login',function(){
	return view('parent/detilBabysitterAfterLogin');
});

//tampilan halaman profile parent//
Route::get('/profile/parent',function(){
	return view('parent/readProfileParent');
});

//tampilan halaman edit profile parent//
Route::get('/profile/parent/edit',function(){
	return view('parent/editProfileParent');
});

//tampilan halaman form pemesanan babysitter//
Route::get('/pemesanan/babysitter',function(){
	return view('parent/formPemesanan');
});

//==================================================================================//


//=========================FITUR BABYSITTER=========================================//
Route::get('/babysitter',function(){
	return view ('homeBS');
});

Route::get('/babysitter/edit', function(){
	return view ('editProfileBabysitter');
});

Route::get('/babysitter/transaksi', function(){
	return view ('transaksiBS');
});

Route::get('/parent/transaksi',function(){
	return view('transaksiPAR');
});


//=============================================================================//


//=========================FITUR ADMIN=========================================//

//tampilan halaman public admin//
Route::get('/dashboard/kelola-babysitter',function(){
	return view('admin/kelola-babysitter');
});

//tampilan halaman detail babysitter//
Route::get('/dashboard/kelola-babysitter/detail',function(){
	return view('admin/detail-babysitter');
});

//tampilan halaman create babysitter//
Route::get('/dashboard/kelola-babysitter/tambah',function(){
	return view('admin/tambah-babysitter');
});

//tampilan halaman kelola transaksi//
Route::get('/dashboard/kelola-transaksi',function(){
	return view('admin/kelola-transaksi');
});

//tampilan halaman detail transaksi//
Route::get('/dashboard/kelola-transaksi/detail',function(){
	return view('admin/detail-transaksi');
});

//==============================================================================//

Route::auth();

Route::get('/home', 'HomeController@index');

// Authentication routes
Route::group(['prefix' => 'auth'], function(){
	Route::group(['prefix' => 'admin'], function(){
		// Authentication Routes...
		Route::get('login', 'Auth\AuthController@showLoginForm');
		Route::post('login', 'Auth\AuthController@login');
		Route::get('logout', 'Auth\AuthController@logout');
		
		// Registration Routes...
		Route::get('register', 'Auth\AuthController@showRegistrationForm');
		Route::post('register', 'Auth\AuthController@register');
		
		// Password Reset Routes...
		Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
		Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
		Route::post('password/reset', 'Auth\PasswordController@reset');
	});

	Route::group(['prefix' => 'parent'], function(){
		// Authentication Routes...
		Route::get('login', 'Auth\Parents\AuthController@showLoginForm');
		Route::post('login', 'Auth\Parents\AuthController@login');
		Route::get('logout', 'Auth\Parents\AuthController@logout');
		
		// Registration Routes...
		Route::get('register', 'Auth\Parents\AuthController@showRegistrationForm');
		Route::post('register', 'Auth\Parents\AuthController@register');
		
		// Password Reset Routes...
		Route::get('password/reset/{token?}', 'Auth\Parents\PasswordController@showResetForm');
		Route::post('password/email', 'Auth\Parents\PasswordController@sendResetLinkEmail');
		Route::post('password/reset', 'Auth\Parents\PasswordController@reset');
	});

	Route::group(['prefix' => 'babysitter'], function(){
		// Authentication Routes...
		Route::get('login', 'Auth\Babysitter\AuthController@showLoginForm');
		Route::post('login', 'Auth\Babysitter\AuthController@login');
		Route::get('logout', 'Auth\Babysitter\AuthController@logout');		
		
		// Password Reset Routes...
		Route::get('password/reset/{token?}', 'Auth\Babysitter\PasswordController@showResetForm');
		Route::post('password/email', 'Auth\Babysitter\PasswordController@sendResetLinkEmail');
		Route::post('password/reset', 'Auth\Babysitter\PasswordController@reset');
	});
});	



// Route::get('/register',function(){
// 	return view('register');
// });

// Route::get('/profile',function(){
// 	return view('profile');
// });

// Route::get('/profileParent',function(){
// 	return view('profileParent');
// });

// Route::get('/edit',function(){
// 	return view('editProfileParent');
// });

// Route::get('/home/parent',function(){
// 	return view('homeParent');
// });

// Route::get('/pesan',function(){
// 	return view('pemesanan/pemesananParent');
// });

// Route::get('/pesan/form',function(){
// 	return view('pemesanan/pemesananParentform');
// });

// Route::get('/pesan/konfirmasi',function(){
// 	return view('pemesanan/pemesananParentKonfirmasi');
// });

// Route::get('/pesan/jadwal',function(){
// 	return view('pemesanan/pemesananParentJadwal');
// });

// Route::get('/pesan/formulir',function(){
// 	return view('pemesanan/formulirPemesanan');
// });

// Route::get('/profile/parent/edit',function(){
// 	return view('ProfileParent2');
// });


// Route::get('/profilBS','babysitterController@getDetailBS'); //lihat detail profil baby sitter
// Route::get('/profilBS','babysitterController@getListBS'); //lihat detail profil baby sitter