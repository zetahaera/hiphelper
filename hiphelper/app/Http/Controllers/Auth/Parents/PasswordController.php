<?php

namespace App\Http\Controllers\Auth\Parents;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * The reset view.
     *
     * @var string
     */
    protected $resetView = 'auth.parent.passwords.reset';

    /**
     * The email request.
     *
     * @var string
     */
    protected $linkRequestView = 'auth.parent.passwords.email';

    /**
     * The broker to use.
     *
     * @var string
     */
    protected $broker = 'parents';

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware());
    }
}
